function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function callTracking() {
    email = document.getElementById('edit-email').value;
    if (email != undefined) {
        window.dmPt('track'); // Simply specify email address
        window.dmPt('identify', email);
        setCookie('trackEmail', email);
        console.log("email = " + email);
    }
}
var dm_insight_id = 'DM-5627734275-01';
(function (w, d, u, t, o, c)
{
    w['dmtrackingobjectname'] = o;
    c = d.createElement(t);
    c.async = 1;
    c.src = u;
    t = d.getElementsByTagName
            (t)[0];
    t.parentNode.insertBefore(c, t);
    w[o] = w[o] || function () {
        (w[o].q = w[o].q || []).push(arguments);
    };
    w[o]('track');
})
        (window, document, '//static.trackedweb.net/js/_dmptv4.js', 'script', 'dmPt');

cEmail = getCookie('trackEmail');
if (cEmail === "") {
    formId = document.querySelector('.webform-submission-form').id;
    document.getElementById(formId).addEventListener("submit", callTracking
            );
} else {
    window.dmPt('track'); // Simply specify email address
    window.dmPt('identify', cEmail);
}
