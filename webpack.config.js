var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/pages/welcome.jsx')
    .addStyleEntry('css/bulma', './node_modules/bulma/bulma.sass')
    .addStyleEntry('css/core', ['./assets/css/core.scss', './node_modules/react-select/dist/react-select.css'])
    .addStyleEntry('css/base-auth', ['./assets/css/base-auth.css', './node_modules/react-notifications/lib/notifications.css'])
    .addStyleEntry('css/base', './assets/css/base.css')
    .addStyleEntry('css/privacy', './assets/css/privacy.css')
    .addStyleEntry('css/exceptions', './assets/css/exceptions.scss')

    .createSharedEntry('vendor', './node_modules/@babel/polyfill/dist/polyfill.js')
    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

    .enableReactPreset()
    // https:/  /github.com/symfony/webpack-encore/issues/193
    // https://github.com/symfony/webpack-encore/issues/266
    .configureBabel(function (babelConfig) {
        babelConfig.plugins = ["@babel/plugin-proposal-object-rest-spread", "@babel/plugin-proposal-class-properties"]
    });

module.exports = Encore.getWebpackConfig();

