<?php

namespace App\Service;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Entity\Hub;
use App\Entity\Tag;
use App\Entity\Tile;
use App\Entity\HubUser;
use App\Entity\User;
use App\Entity\HubTracking;
use Twig\Environment;

/**
 * Class HubManager
 *
 * @package App\Service
 */
class HubManager
{
    protected $settingManager;
    protected $tagManager;
    protected $tileManager;
    protected $route;
    protected $emailSend;
    protected $twigEnvironment;

    /**
     * HubManager constructor.
     * @param SettingManager $settingManager
     * @param TagManager $tagManager
     * @param TileManager $tileManager
     * @param RouterInterface $route
     * @param EmailSend $emailSend
     * @param Environment $twigEnvironment
     */
    public function __construct(
        SettingManager $settingManager,
        TagManager $tagManager,
        TileManager $tileManager,
        RouterInterface $route,
        EmailSend $emailSend,
        Environment $twigEnvironment
    ) {
        $this->settingManager = $settingManager;
        $this->tagManager = $tagManager;
        $this->tileManager = $tileManager;
        $this->route = $route;
        $this->emailSend = $emailSend;
        $this->twigEnvironment = $twigEnvironment;
    }

    /**
     * @param $payload
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function hubClone($payload, $id)
    {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();

        $hub = $em->getRepository(Hub::class)->find($id);
        if (!$hub) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Hub.'];
        }

        $name = $payload["name"];
        if ($name == "") {
            return ['result' => 'error', 'error' => 'Please enter a Hub name.'];
        } else {
            $existsHub = $em->getRepository(Hub::class)->findOneByName($user->getId(), $name);
            if ($existsHub) {
                return [
                    'result' => 'error',
                    'error' => 'It looks like that name already exists.'
                ];
            }
        }

        $newHub = new Hub();
        $newHub->setUser($user)
            ->setName($name)
            ->setIsShared($hub->getIsShared())
            ->setHubType($hub->getHubType())
            ->setHash(bin2hex(random_bytes(16)))
            ->setCreated(new \DateTime())
            ->setUpdated(new \DateTime());

        $em->persist($newHub);
        $em->flush();

        if ($hub->getTile()) {
            foreach ($hub->getTile() as $tile) {
                $newHub->addTile($tile);
            }
            $em->flush();
        }

        if ($hub->getTags()) {
            foreach ($hub->getTags() as $tag) {
                if ($tag->getUser()->getId() == $user->getId()) {
                    $newHub->addTag($tag);
                }
            }
            $em->flush();
        }

        $response['result'] = 'success';
        $response['data'] = [
            "id" => $newHub->getId(),
            "hash" => $newHub->getHash(),
            "name" => $newHub->getName(),
            "hubType" => $newHub->getHubType(),
            "isDelete" => ($newHub->getUser()->getId() == $user->getId()) ? true : false,
            "type" => $this->getHubType($newHub)
        ];

        return $response;
    }

    /**
     * @param $hubId
     * @return array
     */
    public function getSelectedTagIds($hubId)
    {
        $em = $this->settingManager->getEntityManager();
        $hub = $em->getRepository(Hub::class)->find($hubId);
        if (!$hub) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Hub.'];
        }
        $response['result'] = 'success';
        if ($hub->getTags()) {
            foreach ($hub->getTags() as $tag) {
                $response['data'][] = $tag->getId();
            }
        }

        return $response;
    }

    /**
     * @param $payload
     * @return array
     */
    public function addTagsOnHub($payload)
    {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $hubId = $payload["hubId"];
        $hub = $em->getRepository(Hub::class)->find($hubId);

        if (!$hub) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Hub.'];
        }

        $linkedTagIds = [];
        if ($hub->getTags()) {
            foreach ($hub->getTags() as $tag) {
                $linkedTagIds[] = $tag->getId();
            }
        }
        $newTags = false;
        if (array_key_exists('tags', $payload)) {
            if (array_key_exists('hubs', $payload['tags'])) {
                if (is_array($payload['tags']['hubs'])) {
                    $this->syncToRemoveHubTags($hub, $linkedTagIds, $payload['tags']['hubs']);
                    foreach ($payload['tags']['hubs'] as $tagVal) {
                        if (isset($tagVal['id']) && $tagVal['id'] != "") {
                            $tag = $em->getRepository(Tag::class)
                                ->findOneByTagAndUser($user->getId(), $tagVal['id']);

                            if ($tag) {
                                $hub->addTag($tag);
                            }
                        } else {
                            $tag = new Tag();
                            $tag->setUser($user)
                                ->setName($tagVal['name'])
                                ->setCreated(new \DateTime())
                                ->setUpdated(new \DateTime());
                            $em->persist($tag);
                            $em->flush();

                            $hub->addTag($tag);
                            $newTags = true;
                        }
                    }
                    $em->flush();
                }
            }
        }
        if (!$newTags) {
            $this->tagManager->removeUnusedTag();
        }

        $response = ['result' => 'success', 'message' => 'Your Tag has been added.'];

        return $response;
    }

    /**
     * @param $payload
     * @param $hub
     * @return array
     */
    public function updateHub($payload, $hub)
    {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $name = $payload["name"];
        $hubType = $payload["hubType"];

        if ($name == "") {
            return ['result' => 'error', 'error' => 'Please enter a Hub name.'];
        } else {
            $existsHub = $em->getRepository(Hub::class)
                ->findOneByName($user->getId(), $name, $hub->getId());
            if ($existsHub) {
                return ['result' => 'error', 'error' => 'It looks like that Hub already exists.'];
            }
        }

        $hub->setName($name)
            ->setHubType($hubType);
        $em->flush($hub);

//        if($hubType === 'private'){
//           $hubUser = $em->getRepository(HubUser::class)->removeSharedHubUser($user->getId(),$hub->getId());
//        }

        if($hub->getId() > 0) {
            $hubTracking = new HubTracking();
            $hubTracking->setHub($hub->getId())
            ->setTile('')
            ->setActionType('Update')
            ->setCreated(new \DateTime());
            $em->persist($hubTracking);
            $em->flush();
        }

        $response['result'] = 'success';
        $response['message'] = 'Your Hub has been updated.';
        $response['data'] = [
            "id" => $hub->getId(),
            "hash" => $hub->getHash(),
            "name" => $hub->getName(),
            "hubType" => $hub->getHubType(),
            "isDelete" => ($hub->getUser()->getId() == $user->getId()) ? true : false,
            "type" => $this->getHubType($hub)
        ];

        return $response;
    }

    /**
     * @param $payload
     * @return array
     * @throws \Exception
     */
    public function addHub($payload)
    {
        $MAX_HUB_FREE_USER = getenv('MAX_HUB_FREE_USER');
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $name = $payload["name"];

        if ($name === "") {
            return ['result' => 'error', 'error' => 'Please enter a Hub name.'];
        } else {
            $existsHub = $em->getRepository(Hub::class)->findOneByName($user->getId(), $name);

            if ($existsHub) {
                return ['result' => 'error', 'error' => 'It looks like that Hub already exists.'];
            }

            if ($user->getAccountType() === "FREE") {
                $hubCount = $em->getRepository(Hub::class)->getHubCount($user->getId());

                if ($MAX_HUB_FREE_USER < ($hubCount + 1)) {
                    return [
                        'result' => 'error',
                        'error' => "You have reached the maximum number of Hubs allowed for a free account.
                        Upgrade your account for more."
                    ];
                }
            }
        }
        if ($payload["hubType"] === "") {
            return ['result' => 'error', 'error' => 'Please select a Hub type.'];
        }

        $hubType = $payload["hubType"];

        $hub = new Hub();
        $hub->setUser($user)
            ->setName($name)
            ->setIsShared(false)
            ->setHubType($hubType)
            ->setHash(bin2hex(random_bytes(16)))
            ->setCreated(new \DateTime())
            ->setUpdated(new \DateTime());

        $em->persist($hub);
        $em->flush();

        $response['result'] = 'success';
        $response['message'] = 'Your Hub has been created.';
        $response['data'] = [
            "id" => $hub->getId(),
            "hash" => $hub->getHash(),
            "name" => $hub->getName(),
            "hubType" => $hub->getHubType(),
            "isDelete" => ($hub->getUser()->getId() == $user->getId()) ? true : false,
            "type" => $this->getHubType($hub)
        ];

        return $response;
    }

    /**
     * @param $userId
     * @param $hash
     * @param $request
     * @return array
     */
    public function getHubInfo($userId, $hash, $request)
    {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        /*private, public and  shared hub details */
        $hub = $em->getRepository(Hub::class)->findOneByHubDetails($userId, $hash);

        if (!$hub) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Hub.'];
        }

        $tags = [];
        foreach ($hub->getTags() as $tag) {
            $tags[] = $tag->getName();
        }
        $hubUserId = $hub->getUser()->getId();
        $addTile = true;

        if ($hub->getHubType() === 'shared' && $hubUserId !== $userId) {
            $addTile = false;
        }

        $tilesCount = $em->getRepository(Tile::class)->findByAllTiles($userId, $request, $hub->getId(), null, true);

        $isMinus = $userId === $hubUserId;

        $response = [
            "id" => $hub->getId(),
            "hash" => $hub->getHash(),
            "email" => $hub->getUser()->getEmail(),
            "created" => $hub->getCreated()->format("H:i:s d-m-Y"),
            "type" => $this->getHubType($hub),
            "hubType" => $hub->getHubType(),
            "isDelete" => ($hubUserId == $userId) ? true : false,
            "nonRegister" => false,
            "addTile" => $addTile,
            "name" => $hub->getName(),
            "userId" => $userId,
            "total" => $tilesCount,
            "tags" => $tags,
            "data" => $this->tileManager->getTilesByOptions($userId, $request, $hub->getId()),
            "isMinus" => $isMinus,
            "result" => "success"
        ];

        return $response;
    }

    /**
     * @param $hub
     * @param $userId
     * @return string
     */
    public function getHubType($hub)
    {
        if ($hub->getHubType() == 'shared') {
            $type = 'sharedIcon';
        } elseif ($hub->getHubType() == 'open') {
            $type = 'publicIcon';
        } else {
            $type = 'privateIcon';
        }
        return $type;
    }

    /**
     * @param $userId
     * @param $hub
     * @param $emails
     * @return array
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function createShareHub($userId, $hub, $emails)
    {
        $hubId = $hub->getId();
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $registerEmails = $nonRegisterEmails = [];
        $notShareEmails = '';
        $shareWithCurrentUser = '';

        foreach ($emails as $key => $email) {
            $email = trim($email);

            if($user->getEmail() == $email) {
                $shareWithCurrentUser = "You can not share hub to your email.";
                continue;
            }

            $sharedUser = $em->getRepository(User::class)->findOneByEmail($email);
            if ($sharedUser) {
                $sharedHub = $em->getRepository(HubUser::class)
                    ->getOneBySharedUser(
                        $user->getId(),
                        $hubId,
                        $sharedUser->getId(),
                        $email
                    );

                if (!$sharedHub) {
                    $hubUser = new HubUser();
                    $hubUser->setOwner($user)
                        ->setShared($sharedUser)
                        ->setHub($hub)
                        ->setCreated(new \DateTime())
                        ->setUpdated(new \DateTime());
                    $em->persist($hubUser);
                    $registerEmails[] = $email;
                }
                else
                {
                    $notShareEmails .= $email.', ';
                }

            } else {

                $noRegSharedHub = $em->getRepository(HubUser::class)
                    ->getOneBySharedUser(
                        $user->getId(),
                        $hubId,
                        null,
                        $email
                    );
                if(!$noRegSharedHub){
                    $hubUser = new HubUser();
                    $hubUser->setOwner($user)
                        ->setEmail($email)
                        ->setHub($hub)
                        ->setCreated(new \DateTime())
                        ->setUpdated(new \DateTime());
                    $em->persist($hubUser);
                    $nonRegisterEmails[] = $email;
                }
                else
                {
                    $notShareEmails .= $email.', ';
                }
            }
        }
        $em->flush();

        $subject = 'Gruup – you invite to join';
        if ($registerEmails) {
            foreach ($registerEmails as $registerEmail) {
                $sharedLink = $this->route->generate(
                    "show_shared_hub",
                    [
                        "hash" => $hub->getHash(),
                        "email" => $registerEmail
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $registerBody = $this->twigEnvironment->render(
                    'emails/share.html.twig',
                    [
                        'user' => $user,
                        'isRegister' => true,
                        'sharedLink' => $sharedLink
                    ]
                );

                $this->emailSend->sendEmail($registerEmail, $subject, $registerBody);
            }
        }

        if ($nonRegisterEmails) {
            foreach ($nonRegisterEmails as $nonRegisterEmail) {
                $sharedLink = $this->route->generate(
                    "show_shared_hub",
                    [
                        "hash" => $hub->getHash(),
                        "email" => $nonRegisterEmail
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $nonRegisterBody = $this->twigEnvironment->render(
                    'emails/share.html.twig',
                    [
                        'user' => $user,
                        'isRegister' => false,
                        'sharedLink' => $sharedLink
                    ]
                );
                $this->emailSend->sendEmail($nonRegisterEmail, $subject, $nonRegisterBody);
            }
        }

        $message = '';
        $warningMessage = ' '.$shareWithCurrentUser ;
        if($registerEmails || $nonRegisterEmails)
        {
            $message = 'We\'ve shared your Hub.';
        }
        if(!empty($notShareEmails))
        {
            $warningMessage = 'Already shared your Hub with this email(s) - '.$notShareEmails;
        }

        $response = ['result' => 'success', 'message' => $message, 'warningMessage' => trim($warningMessage)];

        return $response;
    }

     /**
     * @param $sendEmailId
     * @param $hubName
     * @return boolean
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailUnshareHub($sendEmailId, $hubName)
    {
        $subject = 'Hub update notification';
        $user = $this->settingManager->getUser();
        $mailBody = $this->twigEnvironment->render(
            'emails/unshare.html.twig',
            [
                'user' => $user,
                'hubName' => $hubName
            ]
        );

        try {
            $this->emailSend->sendEmail($sendEmailId, $subject, $mailBody);
            return true;
        } catch(\Exception $e) {
            return false;
        }
    }

    /**
     * @param $hub
     * @param $linkedTagIds
     * @param $hubsTags
     */
    private function syncToRemoveHubTags($hub, $linkedTagIds, $hubsTags)
    {
        $getTagIds = [];
        foreach ($hubsTags as $tagId) {
            if (isset($tagId['id']) && $tagId['id'] != "") {
                $getTagIds[] = $tagId['id'];
            }
        }
        $diffTagIds = array_diff($linkedTagIds, $getTagIds);
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        if (is_array($diffTagIds) && count($diffTagIds) > 0) {
            foreach ($diffTagIds as $tagId) {
                $tag = $em->getRepository(Tag::class)->findOneByTagAndUser($user->getId(), $tagId);
                if ($tag) {
                    $hub->removeTag($tag);
                }
            }
            $em->flush();
            $em->refresh($hub);
        }
    }

    /**
     * @param $payload
     * @return array
     */
    public function removeHubs($payload)
    {
        $em = $this->settingManager->getEntityManager();
        $tileIdArr = $hubIdArr = $tagsArr = $hubs = $tags = $message = "";

        if (!isset($payload["tileIds"]) && empty($payload["tileIds"])) {
            return ['result' => 'error', 'message' => 'You must select a Tile first.'];
        }

        if (!isset($payload["hubId"]) && empty($payload["hubId"])) {
            return ['result' => 'error', 'message' => 'You must select a Hub first.'];
        }

        if (isset($payload["tileIds"]) && is_array($payload['tileIds']) && !empty($payload["tileIds"])) {
            $tileIdArr = $payload["tileIds"];
        }

        $hub = $em->getRepository(Hub::class)->find($payload["hubId"]);
        if (!$hub) {
            return ['result' => 'error', 'message' => 'We couldn\'t find that Hub.'];
        }
        if (!empty($tileIdArr)) {
            $message = (count($tileIdArr) > 1) ? "Tiles removed to Hub." : "Tile removed to Hub.";

            $tileIds = implode(',', $tileIdArr);
            $tiles = $em->getRepository(Tile::class)->getMultipleTileByIds($tileIds);
            if (!$tiles) {
                return ['result' => 'error', 'message' => 'We couldn\'t find that Tile.'];
            }
            foreach ($tiles as $tile) {
                $tile->removeHub($hub);
            }
            $em->flush();
            $em->refresh($tile);
        }

        return ['result' => 'success', 'message' => $message];
    }

     /**
     * @param $emails
     * @param $hubName
     * @return boolean
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailRemoveHub($emails, $hubName)
    {
        $subject = 'Hub update notification.';
        $user = $this->settingManager->getUser();
        foreach($emails as $sendEmailId) {
            $mailBody = $this->twigEnvironment->render(
                'emails/remove.hub.html.twig',
                [
                    'user' => $user,
                    'hubName' => $hubName
                ]
            );

            try {
                $this->emailSend->sendEmail($sendEmailId, $subject, $mailBody);
                return true;
            } catch(\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param $emails
     * @return boolean
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailUpdateHub($emails)
    {
        $em = $this->settingManager->getEntityManager();
        $subject = 'Hub update notification.';
        foreach($emails as $sendEmailInfo) {

            $hubInfo = $em->getRepository(Hub::class)->find($sendEmailInfo['hubId']);
            if($hubInfo->getId() > 0)
            {
                $sendMailId = $sendEmailInfo['emailId'];
                $hubName = $sendEmailInfo['hubName'];
                $hubId = $sendEmailInfo['hubId'];
                $hubLink = $this->route->generate(
                    "view_hub_details",
                    [
                        "hash" => $hubInfo->getHash()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $mailBody = $this->twigEnvironment->render(
                    'emails/update.hub.html.twig',
                    [
                        'hubName' => $hubName,
                        'hubLink' => $hubLink
                    ]
                );
                try {
                    $this->emailSend->sendEmail($sendMailId, $subject, $mailBody);
                } catch(\Exception $e) {
                    return false;
                }
            }
        }
    }
}
