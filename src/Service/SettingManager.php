<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Dotenv\Dotenv;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use App\Classes\FileType;
use App\Classes\MimeType;
use App\Classes\Extension;

/**
 * Class SettingManager
 *
 * @package App\Service
 */
class SettingManager
{
    protected $em;
    protected $securityToken;
    protected $logger;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $securityToken,
        LoggerInterface $logger
    ) {
        $this->em = $em;
        $this->securityToken = $securityToken;
        $this->logger = $logger;
    }

    /**
     * Return Entity manager
     *
     * @return Object EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * return current login user object
     *
     * @return Object
     */
    public function getUser()
    {
        if ($this->securityToken && is_object($this->securityToken->getToken())) {
            if (is_object($this->securityToken->getToken()->getUser())) {
                return $this->securityToken->getToken()->getUser();
            }
        } else {
            return null;
        }
    }

    public function getExceptionError($msg, $e)
    {
        $user = $this->getUser();
        $userMsg = "";
        if ($user) {
            $userMsg = " userId:".$user->getId()." email:".$user->getEmail();
        }
        $this->logger->err($msg.$userMsg." -> Exception:". $e->getMessage());
        $error = getenv('TECH_ISSUE_ERR');

        return $error;
    }

    /**
     * @param $mimeType
     * @return mixed|null|string
     */
    public function getExtension($mimeType, $fileName)
    {
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        if ($ext == "") {
            $mimeTypeExtensionGuesser = new MimeTypeExtensionGuesser();
            $ext = $mimeTypeExtensionGuesser->guess($mimeType);
        }
        if ($ext == "") {
            $extension = new Extension();
            $ext = $extension->getFileExtension($mimeType);
        }

        return $ext;
    }

    /**
     * @param $fileName
     * @return mixed|null|string
     */
    public function getFileName($fileName)
    {
        $name = preg_replace('/\\.[^.\\s]{2,6}$/', '', $fileName);
        return $name;
    }

    /**
     * @param $mimeType
     * @return mixed|null|string
     */
    public function getFiletype($mimeType)
    {
        $fileType = new FileType();
        return $fileType->getFileType($mimeType);
    }

    /**
     * @param $mimeType
     * @return mixed|null|string
     */
    public function getMimetype($mimeType)
    {
        $mmType = new MimeType();
        return $mmType->getMimeType($mimeType);
    }
}
