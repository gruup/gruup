<?php

namespace App\Service;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\SettingManager;
use App\Service\TileManager;
use App\Entity\Hub;
use App\Entity\Tag;
use App\Entity\Tile;

/**
 * Class SearchManager
 *
 * @package App\Service
 */
class SearchManager
{
    protected $settingManager;
    protected $route;


    public function __construct(
        SettingManager $settingManager,
        TileManager $tileManager,
        RouterInterface $route
    ) {
        $this->settingManager = $settingManager;
        $this->tileManager = $tileManager;
        $this->route = $route;
    }

    public function getSearchResult($userId, $searchText)
    {
        $response = [ 'tileData' => [], 'hubData' => [], 'tagData' => [] ];
        $em = $this->settingManager->getEntityManager();
        $user = $this->settingManager->getUser();
        $tagRepo = $em->getRepository(Tag::class);

        /* Search Tiles */
        $tiles = $em->getRepository(Tile::class)->getSearchTiles($userId, $searchText);
        if ($tiles) {
            foreach ($tiles as $key => $tile) {
                if ($tile['source'] !== "URL") {
                    $url = $this->route->generate(
                        'download_drive_file',
                        array(
                         'fileId'=> $tile['tileId']
                        ),
                        UrlGeneratorInterface::ABSOLUTE_URL
                    );
                    $tiles[$key]['filePath'] = $url; //$url;
                }
                $tiles[$key]['created'] = $tile['created']->format("d/m/Y");
                if ($user) {
                    $tiles[$key]['isDelete'] = ($tile['userId'] == $user->getId()) ? true : false;
                    $tiles[$key]['tags'] = $tagRepo->getUserTagsByTile($tile['tileId'], $user);
                } else {
                    $tiles[$key]['isDelete'] = false;
                    $tiles[$key]['tags'] = $tagRepo->getUserTagsByTile($tile['tileId']);
                }
                $tiles[$key]['hubs'] = $this->tileManager->getSelectedHubIds($userId, $tile['tileId'], true);
            }
            $response['tileData'] = $tiles;
        }


        /* Search Hubs */
        $hubs = $em->getRepository(Hub::class)->getSearchHubs($userId, $searchText);
        if ($hubs) {
            $response['hubData'] = $hubs;
        }

        /* Search TagList */
        $tags = $em->getRepository(Tag::class)->getSearchTags($userId, $searchText);
        if ($tags) {
            $response['tagData'] = $tags;
        }

        return $response;
    }
}
