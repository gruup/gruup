<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use App\Entity\LinkedUser;
use App\Service\SettingManager;
use Google_Client as GoogleClient;
use App\Classes\GoogleUserInfo;

/**
 * Class GoogleOauthManager
 *
 * @package App\Service
 */

class GoogleOauthManager
{
    protected $requestStack;
    protected $googleClient;
    protected $settingManager;
    protected $logger;

    public function __construct(
        RequestStack $requestStack,
        GoogleClient $googleClient,
        SettingManager $settingManager,
        $logger
    ) {
        $this->requestStack = $requestStack;
        $this->googleClient = $googleClient;
        $this->settingManager = $settingManager;
        $this->logger = $logger;
    }

    public function checkOauth()
    {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $request = $this->requestStack->getCurrentRequest();

        $client = $this->googleClient;

        try {
            $access_token = $client->getAccessToken();
            $servicePlus = new \Google_Service_Plus($client);
            $googleUser = new GoogleUserInfo($servicePlus);

            $googleId = $googleUser->getId();
            $googleEmail = $googleUser->getEmail();
            $accessToken = $access_token['access_token'];
            $refreshToken = $access_token['refresh_token'];

            $returnData = $data = [];
            $linkedUser = $em->getRepository(LinkedUser::class)
                ->findOneByGoogleUser($user->getId(), $googleEmail, $googleId);

            if ($linkedUser) {
                $linkedUser
                    ->setIsActive(true)
                    ->setAccessToken($accessToken)
                    ->setToken(json_encode($access_token))
                    ->setRefreshToken($refreshToken);
                $em->flush();
                $data['id'] = $linkedUser->getId();
                $data['email'] = $linkedUser->getEmail();
            } else {
                $linkedUser = new LinkedUser();
                $linkedUser->setUser($user)
                    ->setEmail($googleEmail)
                    ->setGoogleId($googleId)
                    ->setSource('google')
                    ->setIsActive(true)
                    ->setAccessToken($accessToken)
                    ->setRefreshToken($refreshToken)
                    ->setToken(json_encode($access_token))
                    ->setSyncStart(new \DateTime())
                    ->setSyncEnd(new \DateTime())
                    ->setCreated(new \DateTime())
                    ->setUpdated(new \DateTime());
                $em->persist($linkedUser);
                $em->flush();

                $data['id'] = $linkedUser->getId();
                $data['email'] = $linkedUser->getEmail();
            }

            $returnData['IsLoggedIn'] = true;
            $returnData['data'] = $data;

            return $returnData;
        } catch (\Exception $e) {
            $error = $this->settingManager->getExceptionError("GoogleOauthManager checkOauth ", $e);
            $returnData = ['IsLoggedIn' => false, 'error' => $error];
            return $returnData;
        }
    }
}
