<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Swift_Mailer;

/**
 * Class EmailSend
 *
 * @package App\Service
 */
class EmailSend
{
    /**
     * Swift_Message
     *
     * @var \Swift_Message $swift
     */
    private $swift = null;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * EmailSend constructor.
     * @param Swift_Mailer $mailer
     * @param LoggerInterface $logger
     */
    public function __construct(Swift_Mailer $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @param $emails
     * @param $subject
     * @param $body
     */
    public function sendEmail($emails, $subject, $body)
    {
        $mailer = $this->mailer;
        $emailFrom = getenv('emailFrom');
        $nameFrom = getenv('nameFrom');
        $this->swift = new \Swift_Message();
        $this->swift->setFrom($emailFrom, $nameFrom);
        $this->swift->setSubject($subject);
        $this->addToAddress($emails);
        $this->swift->setBody($body, 'text/html');

        //Send
        try {
            $mailer->send($this->swift);
        } catch (\Swift_TransportException $swift_TransportException) {
            $this->logger->critical($emails . ' ' . $swift_TransportException->getMessage());
        }
    }

    /**
     * Add sender email addresses to $this->swift mailer object.
     * @param $emails
     */
    public function addToAddress($emails)
    {
        if (is_array($emails)) {
            foreach ($emails as $email) {
                $this->swift->addTo(trim($email));
            }
        } else {
            $this->swift->addTo(trim($emails));
        }

        return;
    }
}
