<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Response;

use Google_Client as GoogleClient;

/**
 * Class OpenDownloadFilesManager
 *
 * @package App\Service
 */
class OpenDownloadFilesManager
{
    /**
     * @var \Google_Service_Drive
     */
    protected $service;

    /**
     * @var GoogleClient
     */
    protected $googleClient;

    /**
     * @var \App\Service\SettingManager
     */
    protected $settingManager;

    /**
     * OpenDownloadFilesManager constructor.
     * @param \Google_Service_Drive $service
     * @param GoogleClient $googleClient
     * @param \App\Service\SettingManager $settingManager
     */
    public function __construct(
        \Google_Service_Drive $service,
        GoogleClient $googleClient,
        SettingManager $settingManager
    ) {
        $this->service = $service;
        $this->googleClient = $googleClient;
        $this->settingManager = $settingManager;
    }

    /**
     * @param $fileId
     * @return mixed
     */
    public function accessFiles($fileId)
    {
        ini_set('output_buffering', 'off') ;
        $response = $this->service->files->get($fileId);
        $mimeType = $response["mimeType"];
        $getMimeType = $this->settingManager->getMimeType($mimeType);
        if ($getMimeType != "") {
            $mimeType = $getMimeType;
            $responseFile = $this->service->files->export($fileId, $getMimeType);
            $fileExt = ".".$this->settingManager->getExtension($mimeType, $response['name']);
            $fileName = $this->settingManager->getFileName($response['name']);
            $responseDownload = new Response(
                $responseFile->getBody(),
                $responseFile->getStatusCode(),
                array_merge(
                    $responseFile->getHeaders(),
                    ["Content-Disposition" => "attachment; filename=\"{$fileName}{$fileExt}\";"]
                )
            );
            $returnData['results'] = $responseDownload;
        } else {
            $responseFile = $this->service->files->get($fileId, ['alt' => 'media']);

            $fileExt = ".".$this->settingManager->getExtension($mimeType, $response['name']);
            $fileName = $this->settingManager->getFileName($response['name']);
            $responseDownload = new StreamedResponse(
                function () use ($responseFile) {
                    $stream = $responseFile->getBody();
                    $i = 0;
                    while (!$stream->eof()) {
                        echo $stream->read(100);
                        flush();
                    }
                },
                $responseFile->getStatusCode(),
                array_merge(
                    $responseFile->getHeaders(),
                    ["Content-Disposition" => "attachment; filename=\"{$fileName}{$fileExt}\";"]
                )
            );
            $responseDownload->headers->set('X-Accel-Buffering', 'no');
            $returnData['results'] = $responseDownload;
        }
        $returnData['isFolder'] = false;

        return $returnData;
    }
}
