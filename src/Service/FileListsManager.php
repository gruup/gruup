<?php

namespace App\Service;

use App\Entity\Tile;
use App\Entity\LinkedUser;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class FileListsManager
 *
 * @package App\Service
 */
class FileListsManager
{
    /**
     * @var \Google_Service_Drive
     */
    private $googleServiceDrive;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * FileListsManager constructor.
     * @param \Google_Service_Drive $googleServiceDrive
     * @param SettingManager $settingManager
     * @param Logger $logger
     */
    public function __construct(
        \Google_Service_Drive $googleServiceDrive,
        SettingManager $settingManager,
        Logger $logger
    ) {
        $this->googleServiceDrive = $googleServiceDrive;
        $this->settingManager = $settingManager;
        $this->logger = $logger;
    }

    /**
     * @param $client
     * @param $payload
     * @return array
     */
        public function getFileLists($client, $payload)
    {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();

        $linkedUser = $em->getRepository(LinkedUser::class)
            ->findOneByGoogleLoginUser($user->getId());

        if (!$linkedUser) {
            return [
                'result'   => 'error',
                'error'   => 'You need to connect a Google account first.'
            ];
        }

        $offset = 0;
        $limit = getenv('pageSize');
        if (isset($payload['limit']) && is_numeric($payload['limit']) && $payload['limit'] != "") {
            $limit = $payload['limit'];
        }
        if (isset($payload['offset']) && is_numeric($payload['offset']) && $payload['offset'] != "") {
            $offset = $payload['offset'];
        }
        $pageToken = false;
        if (isset($payload['pageToken']) && $payload['pageToken']) {
            $pageToken = $payload['pageToken'];
        }

        try {
            $service = $this->googleServiceDrive;

            $savedPageToken = $linkedUser->getStartPageToken();
            if ($savedPageToken != "") {
                $parameters = $this->getChangeListParameters($pageToken, $limit);
                $results = $service->changes->listChanges($savedPageToken, $parameters);
                $changeList = true;
            } else {
                $parameters = $this->getParameters($pageToken, $limit);
                $results = $service->files->listFiles($parameters);
                $changeList = false;
            }
            $pageToken = $results->nextPageToken;
            $this->syncToDriveFiles($results, $linkedUser, $changeList);

            $returnData = [
                'result' => 'success',
                'message' => 'Sync Successful',
                'pageToken' => $pageToken
            ];
            if (empty($pageToken)) {
                $response = $service->changes->getStartPageToken();
                if ($response) {
                    $linkedUser->setStartPageToken($response->startPageToken);
                }
                $linkedUser->setSyncEnd(new \DateTime());
                $em->flush();
                $tilesCount = $em->getRepository(Tile::class)->getTileCount($user->getId());
                $returnData['total'] = $tilesCount;
            }

            return $returnData;
        } catch (\Exception $e) {
            $this->logger->error(
                "FileListsManager getFileLists userId:" . $user->getId() . " email: " .
                $user->getEmail() . " -> Exception:" . $e->getMessage()
            );
            $error = getenv('TECH_ISSUE_ERR');
            $data['result'] = 'error';
            $data['error'] = $error;
            return $data;
        }
    }

    /**
     * @param $pageToken
     * @param $limit
     * @return array
     */
    private function getParameters($pageToken, $limit)
    {
        $file_fields = getenv('file_fields');
        $query = getenv('query');
        $pageSize = $limit;

        $optParams = array(
          'pageSize' => $pageSize,
          'orderBy' => 'modifiedTime desc',
          'q' => $query,
          'fields' => 'nextPageToken, files(' . $file_fields . ')'
        );
        if ($pageToken) {
            $optParams['pageToken'] = $pageToken;
        }
        return $optParams;
    }

    /**
     * @param $pageToken
     * @param $limit
     * @return array
     */
    private function getChangeListParameters($pageToken, $limit)
    {
        $file_fields = getenv('file_fields');
        $query = getenv('query');
        $pageSize = $limit;

        $optParams = array(
          'pageSize' => $pageSize,
          'spaces' => 'drive',
          'includeCorpusRemovals' => true,
          'fields' => 'nextPageToken,newStartPageToken, changes(file(' . $file_fields . '),removed)'
        );
        if ($pageToken) {
            $optParams['pageToken'] = $pageToken;
        }
        return $optParams;
    }

    /**
     * @param $results
     * @param $linkedUser
     * @param $changeList
     * @return array
     */
    private function syncToDriveFiles($results, $linkedUser, $changeList)
    {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $THUMB_DIR = getenv('THUMB_DIR');
        $fileIds = [];
        $thumbImageName = $thumbnailLink = "";
        $deletedIds = [];
        if ($linkedUser) {
            if ($results) {
                /*Add new file and update exist file*/
                foreach ($results as $key => $result) {
                    if ($changeList) {
                        if ($result->removed) {
                            $deletedIds[] = $result->file->id;
                            continue;
                        }
                        $result = $result->file;
                        if ($result->explicitlyTrashed) {
                            $deletedIds[] = $result->id;
                            continue;
                        }
                    }
                    if ($result->mimeType != "application/vnd.google-apps.folder") {
                        $fileIds[] = $result->id;
                        $mimeType = $result->mimeType;
                        $fileType = $this->settingManager->getFiletype($result->mimeType);
                        $getMimeType = $this->settingManager->getMimetype($mimeType);
                        if ($getMimeType != "") {
                            $mimeType = $getMimeType;
                        }
                        $fileExt = $this->settingManager->getExtension($mimeType, $result->name);
                        $filename = $this->settingManager->getFileName($result->name);

                        $tile = $em->getRepository(Tile::class)
                            ->findOneByUserFileId($user->getId(), $result->id);
                        if ($tile) {
                            if ($fileType == 'image' || $fileType == 'video') {
                                if ($result->thumbnailLink != "") {
                                    $thumbnailLink = $result->thumbnailLink;
                                } else {
                                    $thumbnailLink = $result->iconLink;
                                }
                                /*unlink thumb image*/
                                $this->unlinkThumbImage($tile->getThumbImg(), $fileType);
                            }

                            $tile->setName($filename)
                                ->setFilePath($result->webViewLink)
                                ->setThumbImg(null)
                                ->setRemoteThumbUrl($thumbnailLink)
                                ->setFileType($fileType)
                                ->setFileSize($result->size)
                                ->setFileExt($fileExt)
                                ->setMimeType($mimeType)
                                ->setUrl($result->webViewLink)
                                ->setIsDelete(false)
                                ->setModifiedTime(new \DateTime($result->modifiedTime))
                                ->setDescription($result->description);
                        } else {
                            if ($fileType == 'image' || $fileType == 'video') {
                                if ($result->thumbnailLink != "") {
                                    $thumbnailLink = $result->thumbnailLink;
                                } else {
                                    $thumbnailLink = $result->iconLink;
                                }
                            }

                            $tile = new Tile();
                            $tile->setUser($user)
                                ->setLinkedUser($linkedUser)
                                ->setRemoteFileId($result->id)
                                ->setName($filename)
                                ->setFilePath($result->webViewLink)
                                ->setRemoteThumbUrl($thumbnailLink)
                                ->setFileType($fileType)
                                ->setFileSize($result->size)
                                ->setFileExt($fileExt)
                                ->setMimeType($mimeType)
                                ->setSource('GoogleDrive')
                                ->setUrl($result->webViewLink)
                                ->setDescription($result->description)
                                ->setIsView(false)
                                ->setIsDelete(false)
                                ->setModifiedTime(new \DateTime($result->modifiedTime))
                                ->setCreated(new \DateTime())
                                ->setUpdated(new \DateTime());
                            $em->persist($tile);
                        }
                    }
                }
                $this->syncToRemoveFiles($deletedIds, $linkedUser);
                $em->flush();
            }
        }

        return $fileIds;
    }

    /**
     * @param $thumbImagePath
     * @param $fileType
     * @param $access_token
     * @return string
     */
    private function saveThumbImage($thumbImagePath, $fileType, $access_token)
    {
        $THUMB_DIR = getenv('THUMB_DIR');
        $fileName = "";
        if ($thumbImagePath != "") {
            if (strpos($thumbImagePath, "docs.google.com") === false
                && strpos($thumbImagePath, "drive.google.com") === false
            ) {
                $getFileContent = file_get_contents($thumbImagePath);
            } else {
                $getFileContent = file_get_contents($thumbImagePath."&access_token=".$access_token);
            }

            $file_info = new \finfo(FILEINFO_MIME);
            $mime_type = $file_info->buffer($getFileContent);
            $ext = 'png';
            switch ($mime_type) {
                case "image/jpeg":
                case "image/jpeg; charset=binary":
                    $ext = "jpg";
                    break;
                case "image/png":
                case "image/png; charset=binary":
                    $ext = "png";
                    break;
                case "image/gif":
                case "image/gif; charset=binary":
                    $ext = "gif";
                    break;
                default:
            }

            $fileName = md5(microtime().rand(0, 10)).".".$ext;
            file_put_contents($THUMB_DIR.$fileName, $getFileContent);

            return $fileName;
        }
        return $fileName;
    }

    private function unlinkThumbImage($image, $fileType)
    {
        $THUMB_DIR = getenv('THUMB_DIR');
        if ($image != "") {
            if ($fileType != 'zip') {
                if (file_exists($THUMB_DIR.$image)) {
                    unlink($THUMB_DIR.$image);
                }
            }
        }
    }

    private function syncToRemoveFiles($diffGoogleFileIds, $linkedUserId)
    {
        $em = $this->settingManager->getEntityManager();

        if (is_array($diffGoogleFileIds) && count($diffGoogleFileIds) > 0) {
            $em->getRepository(Tile::class)->removeFiles($diffGoogleFileIds, $linkedUserId);
        }
    }

    public function getSyncHint($client)
    {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();

        $linkedUser = $em->getRepository(LinkedUser::class)
            ->findOneByGoogleLoginUser($user->getId());

        $returnData = ['modifyCount' => 0];
        if ($linkedUser) {
            $service = $this->googleServiceDrive;
            $savedPageToken = $linkedUser->getStartPageToken();
            if ($savedPageToken != "") {
                $results = $service->changes->listChanges($savedPageToken, array(
                    'spaces' => 'drive'
                ));
                if (count($results) > 0) {
                    $returnData = [
                        'modifyCount' => count($results),
                        'message' => 'Files on your Google Drive have changed. Resync them now.'
                    ];
                }
            }
        }

        return $returnData;
    }

    public function saveThumb($tile, $client)
    {
        // $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        // if($user){
        //     $userId = $user->getId();
        // }else{
        $userId = $tile->getUser()->getId();
        // }

        $linkedUser = $em->getRepository(LinkedUser::class)
            ->findOneByGoogleLoginUser($userId);

        if (!$linkedUser) {
            return [
                'result'   => 'error',
                'message'   => 'You need to connect a Google account first.'
            ];
        }
        $access_token = $linkedUser->getAccessToken();

        $THUMB_DIR = getenv('THUMB_DIR');
        $fileName = $tile->getThumbImg();
        $thumbImagePath = $tile->getRemoteThumbUrl();

        $service = $this->googleServiceDrive;
        $fileId = $tile->getRemoteFileId();
        $params = array('fields' => 'hasThumbnail, thumbnailLink');
        $fileName = "../images/cross.png";
        // return new BinaryFileResponse($THUMB_DIR.$fileName);

        try {
            $file = $service->files->get($fileId, $params);
            $thumbImagePath = $file->thumbnailLink;

            if ($thumbImagePath != "") {
                if (strpos($thumbImagePath, "docs.google.com") === false
                    && strpos($thumbImagePath, "drive.google.com") === false
                ) {
                    $getFileContent = file_get_contents($thumbImagePath);
                } else {
                    $getFileContent = file_get_contents($thumbImagePath."&access_token=".$access_token);
                }

                $file_info = new \finfo(FILEINFO_MIME);
                $mime_type = $file_info->buffer($getFileContent);
                $ext = 'png';
                switch ($mime_type) {
                    case "image/jpeg":
                    case "image/jpeg; charset=binary":
                        $ext = "jpg";
                        break;
                    case "image/png":
                    case "image/png; charset=binary":
                        $ext = "png";
                        break;
                    case "image/gif":
                    case "image/gif; charset=binary":
                        $ext = "gif";
                        break;
                    default:
                }

                $fileName = md5(microtime().rand(0, 10)).".".$ext;
                file_put_contents($THUMB_DIR.$fileName, $getFileContent);
                $tile->setThumbImg($fileName)->setRemoteThumbUrl(null);
                $em->flush();
            }
        } catch (\Exception $e) {
            // log error
            $this->logger->err("FileListsManager saveThumb userId:".$userId." -> Exception:". $e->getMessage()." tile:". $tile->getId());
            $fileName = "../images/cross.png";
        }
        
        return new BinaryFileResponse($THUMB_DIR.$fileName);
    }
}
