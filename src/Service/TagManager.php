<?php

namespace App\Service;

use Symfony\Component\Routing\RouterInterface;
use App\Service\SettingManager;
use App\Service\TileManager;
use App\Entity\Tag;
use App\Entity\Tile;

/**
 * Class TagManager
 *
 * @package App\Service
 */
class TagManager extends TileManager
{
    protected $settingManager;
    protected $route;

    public function __construct(
        SettingManager $settingManager,
        RouterInterface $route
    ) {
        $this->settingManager = $settingManager;
        $this->route = $route;
    }

    public function removeUnusedTag()
    {
        $em = $this->settingManager->getEntityManager();
        $user = $this->settingManager->getUser();
        $tags = $em->getRepository(Tag::class)->findBy(['user' => $user->getId()]);
        if (!$tags) {
            return [];
        }
        $isRemoved = false;
        foreach ($tags as $tag) {
            if (count($tag->getTile()) == 0 && count($tag->getHubs()) == 0) {
                $em->remove($tag);
                $isRemoved = true;
            }
        }
        $em->flush();
        return ['isRemoved' => $isRemoved];
    }

    public function getAllTagLists()
    {
        $em = $this->settingManager->getEntityManager();
        $user = $this->settingManager->getUser();

        $tags = $em->getRepository(Tag::class)->findBy(['user' => $user->getId()]);
        if (!$tags) {
            return [];
        }

        $response['result'] = 'success';
        foreach ($tags as $tag) {
            $response['data'][] = [
                'id' => $tag->getId(),
                'name' => $tag->getName(),
                'email' => $tag->getUser()->getEmail(),
                'created' => $tag->getCreated()->format("d/m/Y"),
                'count' => $em->getRepository(Tile::class)->countTilesByTag($tag->getId())
            ];
        }

        return $response;
    }

    public function getTagInfo($userId, $tagId, $request)
    {
        $em = $this->settingManager->getEntityManager();
        $user = $this->settingManager->getUser();

        $tag = $em->getRepository(Tag::class)->findOneByTagDetails($userId, $tagId);

        if (!$tag) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Tag.'];
        }
        $tagId = $tag->getId();
        $tilesCount = $em->getRepository(Tile::class)->findByAllTiles($userId, $request, null, $tagId, true);
        $response['result'] = 'success';
        $response['id'] = $tagId;
        $response['tagId'] = $tagId;
        $response['name'] = '#'.$tag->getName();
        $response['created'] = $tag->getCreated()->format("d/m/Y");
        $response['total'] = $tilesCount;
        $response['data'] = [];

        if ($tilesCount > $request->get('offset')) {
            $response['data'] = $this->getTilesByOptions($userId, $request, null, $tagId);
        }

        return $response;
    }
}
