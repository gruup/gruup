<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use App\Service\SettingManager;
use App\Entity\LinkedUser;
use Symfony\Component\Dotenv\Dotenv;
use Google_Client as GoogleClient;

/**
 * Class GoogleDriveClientManager
 *
 * @package App\Service
 */

class GoogleDriveClientManager
{
    protected $requestStack;
    protected $googleClient;
    protected $googleServiceOauth2;
    protected $settingManager;
    protected $logger;


    public function __construct(
        RequestStack $requestStack,
        GoogleClient $googleClient,
        \Google_Service_Oauth2 $googleServiceOauth2,
        SettingManager $settingManager,
        $logger
    ) {
        $this->requestStack = $requestStack;
        $this->googleClient = $googleClient;
        $this->googleServiceOauth2 = $googleServiceOauth2;
        $this->settingManager = $settingManager;
        $this->logger = $logger;
    }

    public function client($userId)
    {
        $em = $this->settingManager->getEntityManager();

        try {
            $linkedUser = $em->getRepository(LinkedUser::class)
                ->findOneByUser($userId);

            if (!$linkedUser) {
                return [
                    'result'   => 'error',
                    'error'   => 'no linked google account found',
                    'IsAccess' => false,
                    'client'   => false
                ];
            }
            $client = $this->googleClient;
            $client->setAccessToken($linkedUser->getToken());

            if ($client->isAccessTokenExpired()) {
                $client->fetchAccessTokenWithRefreshToken();
                $token = $client->getAccessToken();
                $linkedUser
                    ->setAccessToken($token['access_token'])
                    ->setToken(json_encode($client->getAccessToken()))
                    ->setRefreshToken($token['refresh_token']);
                $em->flush($linkedUser);
            }

            $data['IsAccess'] = true;
            $data['client'] = $client;

            return $data;
        } catch (\Exception $e) {
            $error = $this->settingManager->getExceptionError("GoogleDriveClientManager client", $e);
            $data['result'] = 'error';
            $data['IsAccess'] = false;
            $data['error'] = $error;
            $data['client'] = false;
            return $data;
        }
    }
}
