<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Entity\LinkedUser;
use Twig\Environment;

/**
 * Class UserManager
 *
 * @package App\Service
 */
class UserManager
{
    protected $settingManager;
    protected $route;
    protected $emailSend;
    protected $twigEnvironment;
    protected $logger;

    /**
     * UserManager constructor.
     * @param SettingManager $settingManager
     * @param RouterInterface $route
     * @param EmailSend $emailSend
     * @param Environment $twigEnvironment
     * @param LoggerInterface $logger
     */
    public function __construct(
        SettingManager $settingManager,
        RouterInterface $route,
        EmailSend $emailSend,
        Environment $twigEnvironment,
        LoggerInterface $logger
    ) {
        $this->settingManager = $settingManager;
        $this->route = $route;
        $this->emailSend = $emailSend;
        $this->twigEnvironment = $twigEnvironment;
        $this->logger = $logger;
    }

    /**
     * @param $user
     * @return mixed
     */
    public function getAccountInfo($user)
    {
        $em = $this->settingManager->getEntityManager();

        $linkedUser = $em->getRepository(LinkedUser::class)
            ->findOneByGoogleLoginUser($user->getId());

        $googleLogin = false;
        $googleEmail = "";
        if ($linkedUser && $linkedUser->getAccessToken() != "") {
            $googleLogin = true;
            $googleEmail = $linkedUser->getEmail();
        }
        $response['result'] = 'success';
        $response['id'] = $user->getId();
        $response['email'] = $user->getEmail();
        $response['image'] = $user->getImage();
        $response['googleLogin'] = $googleLogin;
        $response['googleEmail'] = $googleEmail;

        return $response;
    }

    /**
     * @param $user
     * @return $this
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendRegisterInfoMail($user)
    {
        $body = $this->twigEnvironment->render(
            'emails/internal.new.user.html.twig',
            [
                'user' => $user
            ]
        );

        try {
            $this->emailSend->sendEmail(getenv('emailTo'), 'Gruup: Event: New Registration', $body);
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }

        return $this;
    }

    /**
     * @param $user
     * @param $token
     * @return $this
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendWelcomeMail($user, $token)
    {
        $body = $this->twigEnvironment->render(
            'emails/verification.html.twig',
            [
                'url' => $this->route->generate(
                    'user_active_token',
                    [
                        'token' => $token
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ]
        );

        try {
            $this->emailSend->sendEmail($user->getEmail(), 'Gruup account verification', $body);
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }

        return $this;
    }
}
