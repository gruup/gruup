<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\SettingManager;
use App\Service\TagManager;
use App\Entity\Hub;
use App\Entity\Tag;
use App\Entity\Tile;
use App\Entity\HubTracking;
use Embed\Embed;

/**
 * Class TileManager
 *
 * @package App\Service
 */
class TileManager {

    /**
     * @var \App\Service\SettingManager
     */
    protected $settingManager;

    /**
     * @var \App\Service\TagManager
     */
    protected $tagManager;

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    protected $route;

    /**
     * TileManager constructor.
     *
     * @param \App\Service\SettingManager                $settingManager
     * @param \App\Service\TagManager                $tagManager
     * @param \Symfony\Component\Routing\RouterInterface $route
     */
    public function __construct(
            SettingManager $settingManager,
            TagManager $tagManager,
            RouterInterface $route
    ) {
        $this->settingManager = $settingManager;
        $this->tagManager = $tagManager;
        $this->route = $route;
    }

    /**
     * @param                                           $userId
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param null                                      $hubId
     * @param null                                      $tagId
     *
     * @return array
     */
    public function getTilesByOptions($userId, Request $request, $hubId = null, $tagId = null) {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $tagRepo = $em->getRepository(Tag::class);
        $tiles = $em->getRepository(Tile::class)->findByAllTiles($userId, $request, $hubId, $tagId);

        if (!$tiles) {
            return [];
        }
        foreach ($tiles as $key => $tile) {
            if ($tile['source'] === "GoogleDrive") {
                $url = $this->route->generate(
                        'download_drive_file',
                        array(
                            'fileId' => $tile['tileId']
                        ),
                        UrlGeneratorInterface::ABSOLUTE_URL
                );
                $tiles[$key]['filePath'] = $url; //$url;
            }
            $tiles[$key]['created'] = $tile['created']->format("d/m/Y");
            if ($user) {
                $tiles[$key]['isDelete'] = ($tile['userId'] == $user->getId()) ? true : false;
                $tiles[$key]['tags'] = $tagRepo->getUserTagsByTile($tile['tileId'], $user);
            } else {
                $tiles[$key]['isDelete'] = false;
                $tiles[$key]['tags'] = $tagRepo->getUserTagsByTile($tile['tileId']);
            }
            $tiles[$key]['hubs'] = $this->getSelectedHubIds($userId, $tile['tileId'], true);
        }
        return $tiles;
    }

    /**
     * @param $userId
     * @param $tileId
     *
     * @return array
     */
    public function getTilesDetails($userId, $tileId) {
//$user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $tagRepo = $em->getRepository(Tag::class);
        $tiles = $em->getRepository(Tile::class)->findTileDetails($userId, $tileId);
        if (!$tiles) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Tile.'];
        }
        $tile = [];
        foreach ($tiles as $key => $tile) {
            if ($tile['source'] !== "URL") {
                $url = $this->route->generate(
                        'download_drive_file',
                        array(
                            'fileId' => $tile['tileId']
                        ),
                        UrlGeneratorInterface::ABSOLUTE_URL
                );
                $tile['filePath'] = $url; //$url;
            }
            $tile['created'] = $tile['created']->format("d/m/Y");
            $tile['isDelete'] = ($tile['userId'] == $userId) ? true : false;
            $tile['tags'] = $tagRepo->getUserTagsByTile($tile['tileId'], $userId);
            $tile['hubs'] = $this->getSelectedHubIds($userId, $tile['tileId'], true);
        }

        return $tile;
    }

    /**
     * @param $payload
     * @return mixed
     */
    public function addTile($payload) {
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        $url = $payload["url"];
        if ($url == "") {
            return ['result' => 'error', 'error' => 'Please enter a web address.'];
        } else {
            $info = parse_url($url);
            if (!isset($info['scheme'])) {
                $url = "http://" . $url;
            }
            
            if (!filter_var($url, FILTER_VALIDATE_URL)) {
                return ['result' => 'error', 'error' => 'Please enter a valid web address.'];
            }

            $httpPattern = '/^http:\/\//';
            $httpReplacePattern = '/^(http|https):\/\//';
            $withHttpW3 = preg_replace($httpReplacePattern, 'http://', $url);
            $withHttp = preg_replace($httpReplacePattern, 'http://', $url);
            $withHttpsW3 = $url;
            $withHttps = $url;
            if(preg_match($httpPattern, $url)) {
                $withHttpsW3 = preg_replace($httpPattern, 'https://', $url);
            }

            $withHttp = preg_replace($httpReplacePattern, 'http://www.', $url, 1);
            $withHttps = preg_replace($httpReplacePattern, 'https://www.', $url, 1);
            if(preg_match('/^(http|https):\/\/www./', $url)) {
                $withHttp = preg_replace('/www./', '', $withHttp, 2);
                $withHttps = preg_replace('/www./', '', $withHttps, 2);
            }
            $tile = $em->getRepository(Tile::class)->findOneByUrl($user->getId(), $withHttpW3, $withHttp, $withHttpsW3, $withHttps);
            if ($tile) {
                return ['result' => 'error', 'error' => 'It looks like that Tile already exists.'];
            }
        }

        $urlInfo = $this->embedUrl($url);
        if (isset($payload['filePath'])) {
            $filePath = $payload['filePath'];
        } else {
            $filePath = $urlInfo['image'];
        }
        if (isset($payload['source'])) {
            $source = $payload['source'];
        } else {
            $source = "URL";
        }
        if (isset($payload['thumb_img'])) {
            $thumb_img = $payload['thumb_img'];
        } else {
            $thumb_img = $urlInfo['image'];
        }
        if (isset($payload['name'])) {
            $name = $payload['name'];
        } else {
            $name = $urlInfo['title'];
        }
        if (isset($payload['fileType'])) {
            $fileType = $payload['fileType'];
        } elseif (isset($urlInfo['fileType'])) {
            $fileType = $urlInfo['fileType'];
        } else {
            $fileType = null;
        }
        if (isset($payload['favicon'])) {
            $favicon = $payload['favicon'];
        } else {
            $favicon = $urlInfo['favicon'];
        }

        $tile = new Tile();
        $tile->setUser($user)
                ->setUrl($url)
                ->setIsView(false)
                ->setIsDelete(false)
                ->setCreated(new \DateTime())
                ->setModifiedTime(new \DateTime())
                ->setUpdated(new \DateTime());

        if ($urlInfo['result'] == 'error') {
            $tile->setUrl($url)
                    ->setSource($source)
                    ->setName($url);
        } else {
            $tile->setUrl($url)
                    ->setSource($source)
                    ->setName($name)
                    ->setFilePath($filePath)
                    ->setThumbImg($thumb_img)
                    ->setFavicon($favicon)
                    ->setFileType($fileType)
                    ->setFileExt($fileType)
                    ->setMimeType($fileType)
                    ->setDescription($urlInfo['description'])
                    ->setEmbedCode($urlInfo['embedCode']);
        }

        $em->persist($tile);
        $em->flush();

        $tilesCount = $em->getRepository(Tile::class)->findByAllTilesgetTilesCount($user->getId());

        if (isset($payload["hash"]) && $payload["hash"] !== "") {
            $hash = $payload["hash"];
            $hub = $em->getRepository(Hub::class)
                    ->findOneBy(array('hash' => $hash));
            if (!$hub) {
                return ['result' => 'error', 'error' => 'We couldn\'t find that Hub.'];
            }

            $tile->addHub($hub);
            $em->flush();

            $tilesCount = count($hub->getTile());
        }

        if (isset($payload["tagId"]) && $payload["tagId"] !== "") {
            $tagId = $payload["tagId"];
            $tag = $em->getRepository(Tag::class)->find($tagId);
            if (!$tag) {
                return ['result' => 'error', 'error' => 'Tag not found'];
            }

            $tile->addTag($tag);
            $em->flush();

            $tilesCount = count($tag->getTile());
        }

        $response['result'] = 'success';
        $response['message'] = 'Your Tile has been added.';
        $response['total'] = $tilesCount;

        $response['data'] = [
            "tileId" => $tile->getId(),
            "path" => trim($tile->getUrl()),
            "created" => $tile->getCreated()->format("d/m/Y"),
            "name" => $tile->getName(),
            "thumbImg" => $tile->getThumbImg(),
            "source" => $tile->getSource(),
            "filePath" => $tile->getFilePath(),
            "isDelete" => ($tile->getUser()->getId() == $user->getId()) ? true : false,
            "fileSize" => $tile->getFileSize(),
            "fileType" => $tile->getFileType(),
            "fileExt" => $tile->getFileExt(),
            "favicon" => $tile->getFavicon(),
            "mimeType" => $tile->getMimeType(),
            "description" => $tile->getDescription(),
            "owner" => $tile->getUser()->getEmail()
        ];
        $response['data']['hubs'] = $response['data']['tags'] = [];
        if (isset($payload["hash"]) && $payload["hash"] !== "") {
            $response['data']['hubs'] = $this->getHubsFromTiles($tile, $user);
        }
        if (isset($payload["tagId"]) && $payload["tagId"] !== "") {
            $response['data']['tags'] = $em->getRepository(Tag::class)
                    ->getUserTagsByTile($tile->getId(), $user);
        }

        return $response;
    }

    /**
     * @param $tile
     * @param $linkedHubIds
     * @param $hubsTiles
     */
    public function syncToRemoveHubs($tile, $linkedHubIds, $hubsTiles) {
        $getHubIds = [];
        foreach ($hubsTiles as $hubId) {
            $getHubIds[] = $hubId['id'];
        }
        $diffHubIds = array_diff($linkedHubIds, $getHubIds);
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        if (is_array($diffHubIds) && count($diffHubIds) > 0) {
            foreach ($diffHubIds as $hubId) {
                $hub = $em->getRepository(Hub::class)->findOneByHubId($user->getId(), $hubId);
                if ($hub) {
                    $tile->removeHub($hub);
                }
            }
            $em->flush();
            $em->refresh($tile);
        }
    }

    /**
     * @param $tile
     * @param $linkedTagIds
     * @param $tagsTiles
     */
    public function syncToRemoveTags($tile, $linkedTagIds, $tagsTiles) {
        $getTagIds = [];
        foreach ($tagsTiles as $tagId) {
            if (isset($tagId['id']) && $tagId['id'] !== "") {
                $getTagIds[] = $tagId['id'];
            }
        }
        $diffTagIds = array_diff($linkedTagIds, $getTagIds);
        $user = $this->settingManager->getUser();
        $em = $this->settingManager->getEntityManager();
        if (is_array($diffTagIds) && count($diffTagIds) > 0) {
            foreach ($diffTagIds as $tagId) {
                $tag = $em->getRepository(Tag::class)->findOneByTagAndUser($user->getId(), $tagId);
                if ($tag) {
                    $tile->removeTag($tag);
                }
            }
            $em->flush();
            $em->refresh($tile);
        }
    }

    /**
     * @param $tile
     * @param $user
     *
     * @return array
     */
    public function getHubsFromTiles($tile, $user) {
        $response = [];
        if ($tile->getHubs()) {
            foreach ($tile->getHubs() as $hub) {
                if ($hub->getUser()->getId() === $user->getId()) {
                    $response[] = $hub->getId();
                }
                if ($hub->getHubUser()) {
                    foreach ($hub->getHubUser() as $hubUser) {
                        if ($hubUser->getShared() && $hubUser->getShared()->getId() === $user->getId()) {
                            $response[] = $hub->getId();
                        }
                    }
                }
            }
        }
        return $response;
    }

    /**
     * @param $userId
     * @param $payload
     *
     * @return array
     */
    public function addHubAndTagOnTiles($userId, $payload) {
        $em = $this->settingManager->getEntityManager();
        $user = $this->settingManager->getUser();
        $tileIdArr = $hubIdArr = $tagsArr = $hubs = $tags = $message = "";
        $tagIdArr = $newTagArr = $tagEntities = [];
        $newTags = false;
        $tileAddFlag = false;
        if (isset($payload["tileIds"]) && is_array($payload['tileIds']) && !empty($payload["tileIds"])) {
            $tileIdArr = $payload["tileIds"];
        }
        if (isset($payload["hubIds"]) && is_array($payload['hubIds']) && !empty($payload["hubIds"])) {
            $hubIdArr = $payload["hubIds"];
            if (count($tileIdArr) > 1) {
                $tileAddFlag = true;
                $message = "Tiles loaded to Hub.";
            } else {
                $tileAddFlag = true;
                $message = "Tile loaded to Hub.";
            }
        }
        if (isset($payload["tags"]) && is_array($payload['tags']) && !empty($payload["tags"])) {
            $tagsArr = $payload["tags"];
            $message = "Your Tag has been added.";
        }

        if (!empty($tileIdArr)) {
            $tileIds = implode(',', $tileIdArr);
            $tiles = $em->getRepository(Tile::class)->getMultipleTileByIds($tileIds);

            if (!$tiles) {
                return ['result' => 'error', 'message' => 'We couldn\'t find that Tile.'];
            }
            if ($tiles) {
                if (!empty($hubIdArr)) {
                    $hubIds = implode(',', $hubIdArr);
                    $hubs = $em->getRepository(Hub::class)->findByHubIds($userId, $hubIds);
                    if (!$hubs) {
                        return ['result' => 'error', 'message' => 'We couldn\'t add that Tile to the Hub.'];
                    }
                }
                if (!empty($tagsArr)) {
                    foreach ($tagsArr as $tagVal) {
                        if (isset($tagVal['id']) && $tagVal['id'] != "") {
                            $tagIdArr[] = $tagVal['id'];
                        } else {
                            $newTagArr[] = $tagVal['name'];
                        }
                    }
                    if (!empty($tagIdArr)) {
                        $tagIds = implode(',', $tagIdArr);
                        $tags = $em->getRepository(Tag::class)->findByTagIds($userId, $tagIds);
                    }
                }
                foreach ($newTagArr as $key => $newTag) {
                    $tag = new Tag();
                    $tag->setUser($user)
                            ->setName($newTag)
                            ->setCreated(new \DateTime())
                            ->setUpdated(new \DateTime());
                    $em->persist($tag);
                    $tagEntities[] = $tag;
//$tile->addTag($tag);
                    $newTags = true;
                }
                foreach ($tiles as $tile) {
                    if ($hubs) {
                        foreach ($hubs as $hub) {
                            $tile->addHub($hub);
                        }
                    }
                    if ($tags) {
                        foreach ($tags as $tag) {
                            $tile->addTag($tag);
                        }
                    }
                    if (!empty($tagEntities)) {
                        foreach ($tagEntities as $key => $tagEntity) {
                            $tile->addTag($tagEntity);
                        }
                    }
                }
                $em->flush();

                if (isset($payload["hubIds"])) {
                    $currentHub = $payload["hubIds"][0];

                    if ($tileAddFlag == true) {
                        foreach ($payload["tileIds"] as $tileId) {
                            $hubTracking = new HubTracking();
                            $hubTracking->setHub($currentHub)
                                    ->setTile($tileId)
                                    ->setActionType('Add')
                                    ->setCreated(new \DateTime());
                            $em->persist($hubTracking);
                        }
                        $em->flush();
                    }
                }
            }
        }
        if (!$newTags) {
            $this->tagManager->removeUnusedTag();
        }

        return ['result' => 'success', 'message' => $message];
    }

    private function embedUrl($url) {
        $response = [];
        try {
            $info = Embed::create($url, [
                        'min_image_width' => 160,
                        'min_image_height' => 160,
                        'choose_bigger_image' => true
            ]);

            $response['result'] = 'success';
            $response['title'] = $info->title;
            $response['description'] = $info->description;
            $response['image'] = $info->image;
            $response['embedCode'] = $info->code; //The code to embed the image, video, etc
            $response['favicon'] = $info->providerIcon;
            $fileType = $info->type; //The page type (link, video, image, rich)
            if ($info->type == 'link' || $info->type == 'rich') {
                $fileType = 'URL';
            } elseif ($info->type == 'photo') {
                $fileType = 'image';
            }
            $response['fileType'] = $fileType;
        } catch (\Exception $e) {
            $error = $this->settingManager->getExceptionError("TileManager embedUrl", $e);
            $response = ['result' => 'error', 'error' => $error];
        }
        return $response;
    }

    /**
     * @param $userId
     * @param $payload
     *
     * @return array
     */
    public function deleteTiles($userId, $payload) {
        $em = $this->settingManager->getEntityManager();
        $user = $this->settingManager->getUser();
        $tileIdArr = [];
        if (!isset($payload["tileIds"]) && empty($payload["tileIds"])) {
            return ['result' => 'error', 'error' => 'You must select a Tile first.'];
        }

        if (isset($payload["tileIds"]) && is_array($payload['tileIds']) && !empty($payload["tileIds"])) {
            $tileIdArr = $payload["tileIds"];
        }

        $notDeletedTiles = [];
        $isDeletedTiles = false;
        if (!empty($tileIdArr)) {
            $tileIds = implode(',', $tileIdArr);
            $tiles = $em->getRepository(Tile::class)->getMultipleTileByIds($tileIds);
            if (!$tiles) {
                return ['result' => 'error', 'error' => 'We couldn\'t find that Tile.'];
            }

            foreach ($tiles as $key => $tile) {
                if ($tile->getUser()->getId() == $userId) {
                    if ($tile->getFileType() == 'URL') {
                        $em->remove($tile);
                    } else {
                        $tile->setIsDelete(true);
                    }
                    $isDeletedTiles = true;
                } else {
                    $notDeletedTiles[$key]['id'] = $tile->getId();
                    $notDeletedTiles[$key]['name'] = $tile->getName();
                }
            }
            $em->flush();
            $this->tagManager->removeUnusedTag();
        }
        if (!empty($notDeletedTiles)) {
            $result = 'error';
            $message = '';
            if ($isDeletedTiles) {
                $message .= 'Authorised tiles deleted successfully, But ';
            }
            $message .= 'You can not able to delete unauthorised tiles.';
        } else {
            $result = 'success';
            $message = 'Your selected Tiles have been deleted.';
        }
        return ['result' => $result, 'message' => $message, 'notDeletedTiles' => $notDeletedTiles];
    }

    /**
     * @param $userId
     * @param $tileId
     *
     * @return array
     */
    public function getSelectedHubIds($userId, $tileId, $onlyIds = false) {
        $response = [];
        $em = $this->settingManager->getEntityManager();
        $tile = $em->getRepository(Tile::class)->find($tileId);
        if (!$tile) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Tile.'];
        }

        if ($onlyIds) {
            $response = [];
        } else {
            $response['result'] = 'success';
        }
        if ($tile->getHubs()) {
            foreach ($tile->getHubs() as $hub) {
                if ($hub->getUser()->getId() == $userId) {
                    if ($onlyIds) {
                        $response[] = $hub->getId();
                    } else {
                        $response['data'][] = $hub->getId();
                    }
                }
                if ($hub->getHubUser()) {
                    foreach ($hub->getHubUser() as $hubUser) {
                        if ($hubUser->getShared() && $hubUser->getShared()->getId() == $userId) {
                            if ($onlyIds) {
                                $response[] = $hub->getId();
                            } else {
                                $response['data'][] = $hub->getId();
                            }
                        }
                    }
                }
            }
        }
        return $response;
    }

    /**
     * @param $tileId
     *
     * @return array
     */
    public function getSelectedTagIds($tileId) {
        $em = $this->settingManager->getEntityManager();
        $tile = $em->getRepository(Tile::class)->find($tileId);
        if (!$tile) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Tile.'];
        }

        $response['result'] = 'success';
        if ($tile->getTags()) {
            foreach ($tile->getTags() as $tag) {
                $response['data'][] = $tag->getId();
            }
        }

        return $response;
    }

    /**
     * @param $userId
     * @param $payload
     *
     * @return array
     */
    public function addTagForTile($userId, $payload) {
        $em = $this->settingManager->getEntityManager();
        $user = $this->settingManager->getUser();

        $tileId = (int) $payload["tileId"];
        $tile = $em->getRepository(Tile::class)->findOneByUserTile($userId, $tileId);
        if (!$tile) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Tile.'];
        }
        $linkedTagIds = [];
        if ($tile->getTags()) {
            foreach ($tile->getTags() as $tag) {
                $linkedTagIds[] = $tag->getId();
            }
        }
        $newTags = false;
        if (array_key_exists('tags', $payload)) {
            if (array_key_exists('tiles', $payload['tags'])) {
                if (is_array($payload['tags']['tiles'])) {
                    $tagTiles = $payload['tags']['tiles'];
                    $this->syncToRemoveTags($tile, $linkedTagIds, $tagTiles);
                    foreach ($tagTiles as $tagVal) {
                        if (isset($tagVal['id']) && $tagVal['id'] != "") {
                            $tag = $em->getRepository(Tag::class)
                                    ->findOneByTagAndUser($userId, $tagVal['id']);

                            if ($tag) {
                                $tile->addTag($tag);
                            }
                        } else {
                            $tag = new Tag();
                            $tag->setUser($user)
                                    ->setName($tagVal['name'])
                                    ->setCreated(new \DateTime())
                                    ->setUpdated(new \DateTime());
                            $em->persist($tag);
                            $em->flush();

                            $tile->addTag($tag);
                            $newTags = true;
                        }
                    }
                    $em->flush();
                }
            }
        }
        $isRemoved = false;
        if (!$newTags) {
            $removedTags = $this->tagManager->removeUnusedTag();
            if ($removedTags['isRemoved']) {
                $isRemoved = true;
            }
        }

        $response = ['result' => 'success', 'message' => 'Your Tag has been added.', 'isRemoved' => $isRemoved];

        return $response;
    }

    /**
     * @param $userId
     * @param $payload
     *
     * @return array
     */
    public function addHubForTile($userId, $payload) {
        $em = $this->settingManager->getEntityManager();
        $tileId = (int) $payload["tileId"];
        $tile = $em->getRepository(Tile::class)->find($tileId);
        if (!$tile) {
            return ['result' => 'error', 'error' => 'We couldn\'t find that Tile.'];
        }

        $linkedHubIds = [];
        if ($tile->getHubs()) {
            foreach ($tile->getHubs() as $hub) {
                $linkedHubIds[] = $hub->getId();
            }
        }

        if (array_key_exists('hubs', $payload)) {
            if (array_key_exists('tiles', $payload['hubs'])) {
                if (is_array($payload['hubs']['tiles'])) {
                    $hubTiles = $payload['hubs']['tiles'];
                    $this->syncToRemoveHubs($tile, $linkedHubIds, $hubTiles);
                    foreach ($hubTiles as $hubHash) {
                        $hub = $em->getRepository(Hub::class)
                                ->findOneBy(['hash' => $hubHash['hash']]);

                        if ($hub) {
                            $tile->addHub($hub);
                        }
                    }
                    $em->flush();
                }
            }
        }

        $response = ['result' => 'success', 'message' => 'Your Hub has been created.'];

        return $response;
    }

}
