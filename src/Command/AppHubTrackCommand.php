<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use App\Entity\HubTracking;
use App\Entity\HubUser;

class AppHubTrackCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'App:hub-track';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        $em = $this->getContainer()->get('doctrine')->getManager('default');
        $hubsTrack = $em->getRepository(HubTracking::class)->getGroupByHubIds($arg1);
        $allEmails = [];
        $allInfo = array();

        foreach($hubsTrack as $hubInfo) {
            $hubsUserInfo = $em->getRepository(HubUser::class)->getHubShareEmail($hubInfo->getHub());
            foreach($hubsUserInfo as $hubUserEmail) {
                $sendMailId = $hubUserEmail->getEmail();
                if(empty($sendMailId) && $hubUserEmail->getShared()->getId() > 0) {
                    $sendMailId = $hubUserEmail->getShared()->getEmail();
                }
                if(!empty($sendMailId)) {
                    $allInfo['hubId'] = $hubUserEmail->getHub()->getId();
                    $allInfo['emailId'] = $sendMailId;
                    $allInfo['actionType'] = $hubInfo->getActionType();
                    $allInfo['hubName'] = $hubUserEmail->getHub()->getName();
                    array_push($allEmails, $allInfo);
                }
            }
        }

        if ($allEmails) {
            $this->getContainer()->get('hub.manager')->sendMailUpdateHub($allEmails);
        }

        $io->success('Command run successfully');
    }
}
