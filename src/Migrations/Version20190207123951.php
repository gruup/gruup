<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190207123951 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hub_tracking DROP FOREIGN KEY FK_6F59C5B76C786081');
        $this->addSql('DROP INDEX IDX_6F59C5B76C786081 ON hub_tracking');
        $this->addSql('ALTER TABLE hub_tracking ADD hub VARCHAR(255) NOT NULL, ADD tile VARCHAR(255) NOT NULL, ADD action_type VARCHAR(255) NOT NULL, DROP hub_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hub_tracking ADD hub_id INT DEFAULT NULL, DROP hub, DROP tile, DROP action_type');
        $this->addSql('ALTER TABLE hub_tracking ADD CONSTRAINT FK_6F59C5B76C786081 FOREIGN KEY (hub_id) REFERENCES hub (id)');
        $this->addSql('CREATE INDEX IDX_6F59C5B76C786081 ON hub_tracking (hub_id)');    
    }
}