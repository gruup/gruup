<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180809083248 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hub (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, hash VARCHAR(255) NOT NULL, is_shared TINYINT(1) NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, INDEX IDX_4871CE4DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hub_tile (hub_id INT NOT NULL, tile_id INT NOT NULL, INDEX IDX_70C81BB86C786081 (hub_id), INDEX IDX_70C81BB8638AF48B (tile_id), PRIMARY KEY(hub_id, tile_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hub_tag (hub_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_F28E14C66C786081 (hub_id), INDEX IDX_F28E14C6BAD26311 (tag_id), PRIMARY KEY(hub_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hub_user (id INT AUTO_INCREMENT NOT NULL, hub_id INT DEFAULT NULL, shared_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, INDEX IDX_8BD464F56C786081 (hub_id), INDEX IDX_8BD464F53B943F60 (shared_id), INDEX IDX_8BD464F57E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, password VARCHAR(64) NOT NULL, email VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, account_type VARCHAR(255) NOT NULL, created DATETIME NOT NULL, image LONGTEXT DEFAULT NULL, confirmation_token VARCHAR(254) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tile (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, linked_user_id INT DEFAULT NULL, remote_file_id VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, file_path VARCHAR(255) DEFAULT NULL, thumb_img VARCHAR(255) DEFAULT NULL, favicon VARCHAR(255) DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, file_type VARCHAR(255) DEFAULT NULL, file_size VARCHAR(255) DEFAULT NULL, file_ext VARCHAR(255) DEFAULT NULL, mime_type VARCHAR(255) DEFAULT NULL, source VARCHAR(255) DEFAULT NULL, url LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, embed_code LONGTEXT DEFAULT NULL, is_view TINYINT(1) DEFAULT NULL, is_delete TINYINT(1) DEFAULT \'0\', created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, INDEX IDX_768FA904A76ED395 (user_id), INDEX IDX_768FA904CC26EB02 (linked_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tile_tag (tile_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_12540F4C638AF48B (tile_id), INDEX IDX_12540F4CBAD26311 (tag_id), PRIMARY KEY(tile_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, INDEX IDX_389B783A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE linked_user (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, google_id VARCHAR(255) DEFAULT NULL, source VARCHAR(255) DEFAULT NULL, access_token LONGTEXT DEFAULT NULL, refresh_token LONGTEXT DEFAULT NULL, token LONGTEXT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, sync_start DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, sync_end DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_59E40333A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hub ADD CONSTRAINT FK_4871CE4DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE hub_tile ADD CONSTRAINT FK_70C81BB86C786081 FOREIGN KEY (hub_id) REFERENCES hub (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hub_tile ADD CONSTRAINT FK_70C81BB8638AF48B FOREIGN KEY (tile_id) REFERENCES tile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hub_tag ADD CONSTRAINT FK_F28E14C66C786081 FOREIGN KEY (hub_id) REFERENCES hub (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hub_tag ADD CONSTRAINT FK_F28E14C6BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hub_user ADD CONSTRAINT FK_8BD464F56C786081 FOREIGN KEY (hub_id) REFERENCES hub (id)');
        $this->addSql('ALTER TABLE hub_user ADD CONSTRAINT FK_8BD464F53B943F60 FOREIGN KEY (shared_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE hub_user ADD CONSTRAINT FK_8BD464F57E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tile ADD CONSTRAINT FK_768FA904A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tile ADD CONSTRAINT FK_768FA904CC26EB02 FOREIGN KEY (linked_user_id) REFERENCES linked_user (id)');
        $this->addSql('ALTER TABLE tile_tag ADD CONSTRAINT FK_12540F4C638AF48B FOREIGN KEY (tile_id) REFERENCES tile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tile_tag ADD CONSTRAINT FK_12540F4CBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B783A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE linked_user ADD CONSTRAINT FK_59E40333A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hub_tile DROP FOREIGN KEY FK_70C81BB86C786081');
        $this->addSql('ALTER TABLE hub_tag DROP FOREIGN KEY FK_F28E14C66C786081');
        $this->addSql('ALTER TABLE hub_user DROP FOREIGN KEY FK_8BD464F56C786081');
        $this->addSql('ALTER TABLE hub DROP FOREIGN KEY FK_4871CE4DA76ED395');
        $this->addSql('ALTER TABLE hub_user DROP FOREIGN KEY FK_8BD464F53B943F60');
        $this->addSql('ALTER TABLE hub_user DROP FOREIGN KEY FK_8BD464F57E3C61F9');
        $this->addSql('ALTER TABLE tile DROP FOREIGN KEY FK_768FA904A76ED395');
        $this->addSql('ALTER TABLE tag DROP FOREIGN KEY FK_389B783A76ED395');
        $this->addSql('ALTER TABLE linked_user DROP FOREIGN KEY FK_59E40333A76ED395');
        $this->addSql('ALTER TABLE hub_tile DROP FOREIGN KEY FK_70C81BB8638AF48B');
        $this->addSql('ALTER TABLE tile_tag DROP FOREIGN KEY FK_12540F4C638AF48B');
        $this->addSql('ALTER TABLE hub_tag DROP FOREIGN KEY FK_F28E14C6BAD26311');
        $this->addSql('ALTER TABLE tile_tag DROP FOREIGN KEY FK_12540F4CBAD26311');
        $this->addSql('ALTER TABLE tile DROP FOREIGN KEY FK_768FA904CC26EB02');
        $this->addSql('DROP TABLE hub');
        $this->addSql('DROP TABLE hub_tile');
        $this->addSql('DROP TABLE hub_tag');
        $this->addSql('DROP TABLE hub_user');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE tile');
        $this->addSql('DROP TABLE tile_tag');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE linked_user');
    }
}
