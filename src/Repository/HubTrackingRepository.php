<?php

namespace App\Repository;

use App\Entity\HubTracking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class HubTrackingRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HubTracking::class);
    }

    public function getGroupByHubIds($lastDateTime)
    {
    	if(empty($lastDateTime))
    	{
    		$lastDateTime = '24';
    	}
    	$date = new \DateTime();
		$date->modify('-'.$lastDateTime.' hour');

    	return  $this->createQueryBuilder('h')
    		->andWhere('h.created > :datetime')
    		->setParameter(':datetime', $date)
            ->groupBy('h.hub')
            ->orderBy('h.hub', 'desc')
            ->getQuery()
            ->getResult();
    }
}

?>