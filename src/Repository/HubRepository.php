<?php

namespace App\Repository;

use App\Entity\Hub;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Hub|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hub|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hub[]    findAll()
 * @method Hub[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HubRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Hub::class);
    }

    //    /**
    //     * @return Hub[] Returns an array of Hub objects
    //     */

    public function getHubLists($userId)
    {
        return  $this->createQueryBuilder('h')
            ->leftJoin('h.hubUser', 'hu')
            ->andWhere('h.user = :userId')
            ->orWhere('hu.shared = :sharedId')
            ->setParameter('userId', $userId)
            ->setParameter('sharedId', $userId)
            ->groupBy('h.id')
            ->orderBy('h.id', 'desc')
            ->getQuery()
            ->getResult();
    }

    public function findOneByHubDetails($userId, $hash)
    {
        return $this->createQueryBuilder('h')
            ->leftJoin('h.hubUser', 'hu')
            ->andWhere('h.hash = :hash')
            ->setParameter('hash', $hash)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneBySharedHub($hash, $email)
    {
        return $this->createQueryBuilder('h')
            ->leftJoin('h.hubUser', 'hu')
            ->andWhere('h.hash = :hash')
            ->andWhere('h.hubType != :hubType')
            ->andWhere('hu.email = :email')
            ->setParameter('hubType', 'private')
            ->setParameter('hash', $hash)
            ->setParameter('email', $email)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByName($userId, $name, $hubId = null)
    {
        $qb = $this->createQueryBuilder('h')
            ->select('count(DISTINCT h.id)')
            ->andWhere('h.user = :userId')
            ->andWhere('h.name = :name')
            ->setParameter('userId', $userId)
            ->setParameter('name', $name);

        if ($hubId != "") {
            $qb->andWhere('h.id != :hubId')
                ->setParameter('hubId', $hubId);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findOneByUser($userId, $hash)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.user = :userId')
            ->andWhere('h.hash = :hash')
            ->setParameter('userId', $userId)
            ->setParameter('hash', $hash)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByHubId($userId, $hubId)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.user = :userId')
            ->andWhere('h.id = :hubId')
            ->setParameter('userId', $userId)
            ->setParameter('hubId', $hubId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByHubIds($userId, $hubIds)
    {
        $qb = $this->createQueryBuilder('h');
        $qb->andWhere(
                    $qb->expr()->in('h.id', $hubIds)
                );
        return $qb->getQuery()->getResult();
    }

    public function getSearchHubs($userId, $searchText, $offset = 0, $limit = 20)
    {
        $qb = $this->createQueryBuilder('h')
            ->select('h.id as id,h.hash,h.name')
            ->leftJoin('h.hubUser', 'hu');

        if ($searchText != "") {
            $expr[] = $qb->expr()->like('h.name', $qb->expr()->literal('%' . $searchText. '%'));
            $qb->andWhere(
                '(' . implode(' OR ', $expr) .')'
            );
        }
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->eq('h.user', ':userId'),
                $qb->expr()->eq('hu.shared', ':userId')
            )
        )
            ->setParameter('userId', $userId)
            ->groupBy('id')
            ->orderBy('h.id', 'DESC');

        return $qb->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getArrayResult();
    }

    public function getHubListArray($userId)
    {
        return  $this->createQueryBuilder('h')
            ->select('h.id as id')
            ->leftJoin('h.hubUser', 'hu')
            ->andWhere('h.user = :userId')
            ->orWhere('hu.shared = :sharedId')
            ->setParameter('userId', $userId)
            ->setParameter('sharedId', $userId)
            ->groupBy('id')
            ->getQuery()
            ->getArrayResult();
    }

    public function getHubCount($userId)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.user = :userId')
            ->setParameter('userId', $userId)
            ->select('COUNT(h.id) as hubCount')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
