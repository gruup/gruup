<?php

namespace App\Repository;

use App\Entity\HubUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HubUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method HubUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method HubUser[]    findAll()
 * @method HubUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HubUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HubUser::class);
    }

    public function findOneByEmail($email): ?HubUser
    {
        return $this->createQueryBuilder('hu')
            ->andWhere('hu.email = :email')
            ->setParameter('email', $email)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getOneBySharedUser($userId, $hubId, $sharedId, $email): ?HubUser
    {
        $qb = $this->createQueryBuilder('hu')
            ->select('hu')
            ->andWhere('hu.owner = :userId')
            ->andWhere('hu.hub = :hubId')
            ->setParameter('userId', $userId)
            ->setParameter('hubId', $hubId);
        if ($sharedId == "") {
            $qb->andWhere('hu.email = :email')
                ->setParameter('email', $email);
        } else {
            $qb->andWhere('hu.shared = :sharedId')
                ->setParameter('sharedId', $sharedId);
        }

        return $qb->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
    }

    public function findOneByNonRegisterUserEmail($email, $hubId): ?HubUser
    {
        return $this->createQueryBuilder('hu')
            ->andWhere('hu.email = :email')
            ->andWhere('hu.hub = :hubId')
            ->setParameter('email', $email)
            ->setParameter('hubId', $hubId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function removeSharedHubUser($userId, $hubId)
    {
        return $this->createQueryBuilder('hu')
                ->delete()
                ->andWhere('hu.owner = :userId')
                ->andWhere('hu.hub = :hubId')
                ->setParameter('userId', $userId)
                ->setParameter('hubId', $hubId)
                ->getQuery()
                ->getResult();
    }

    public function removeSharedHub($userId, $hubId)
    {
        return $this->createQueryBuilder('hu')
                ->delete()
                ->andWhere('hu.shared = :userId')
                ->andWhere('hu.hub = :hubId')
                ->setParameter('userId', $userId)
                ->setParameter('hubId', $hubId)
                ->getQuery()
                ->getResult();
    }

    public function getSharedHubUserList($hubId, $ownerId)
    {
        return $this->createQueryBuilder('hu')
            ->select('hu')
            ->andWhere('hu.hub = :hubID')
            ->andWhere('hu.owner = :ownerID')
            ->setParameter('hubID', $hubId)
            ->setParameter('ownerID', $ownerId)
            ->getQuery()
            ->getResult();
    }

    public function removeSharedHubsUser($hubUserId, $hubId, $ownerId)
    {
        return $this->createQueryBuilder('hu')
            ->andWhere('hu.id = :hubUserID')
            ->andWhere('hu.hub = :hubID')
            ->andWhere('hu.owner = :ownerID')
            ->setParameter('hubUserID', $hubUserId)
            ->setParameter('hubID', $hubId)
            ->setParameter('ownerID', $ownerId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getHubShareEmail($hubId)
    {
        return $this->createQueryBuilder('hu')
            ->select('hu')
            ->andWhere('hu.hub = :hubID')
            ->setParameter('hubID', $hubId)
            ->getQuery()
            ->getResult();
    }
}
