<?php

namespace App\Repository;

use App\Entity\Tile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method Tile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tile[]    findAll()
 * @method Tile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TileRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tile::class);
    }

    //    /**
    //     * @return Tile[] Returns an array of Tile objects
    //     */

    public function findByUserTiles($userId)
    {
        return $this->createQueryBuilder('t')
            ->select('count(DISTINCT t.id)')
            ->andWhere('t.user = :userId')
            ->andWhere('t.source = :source')
            ->andWhere('t.isDelete != 1')
            ->setParameter('userId', $userId)
            ->setParameter('source', 'URL')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getSyncTilesCount($userId, $linkedUserId)
    {
        return $this->createQueryBuilder('t')
            ->select('count(DISTINCT t.id)')
            ->andWhere('t.user = :userId')
            ->andWhere('t.linkedUser = :linkedUserId')
            ->setParameter('userId', $userId)
            ->setParameter('linkedUserId', $linkedUserId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param                                           $userId
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param null                                      $hubId
     * @param null                                      $tagId
     *
     * @return array
     *
     */
    public function findByAllTiles($userId, Request $request, $hubId = null, $tagId = null, $onlyCount = false)
    {
        $offset = 0;
        $limit = 15; // add
        $searchText = '';
        if (is_numeric($request->get('limit')) && $request->get('limit') != "") {
            $limit = $request->get('limit');
        }
        if (is_numeric($request->get('offset')) && $request->get('offset') != "") {
            $offset= $request->get('offset');
        }
        if ($request->get('searchText')  && $request->get('searchText') != "") {
            $searchText= $request->get('searchText');
        }

        $qb = $this->createQueryBuilder('t')
            ->select('t.id as tileId,t.url as path,t.created,t.name,t.thumbImg,t.source,t.filePath,t.fileSize,t.fileType,t.fileExt,t.mimeType,t.description,t.favicon,t.remoteThumbUrl,u.email as owner,u.id as userId, t.remoteFileId as remoteFileId')
            ->leftJoin('t.linkedUser', 'lu', 'with', 'lu.id = t.linkedUser')
            ->leftJoin('t.hubs', 'h')
            ->leftJoin('h.hubUser', 'hu')
            ->leftJoin('t.user', 'u')
            ->andWhere('t.isDelete != 1')
            ->groupBy('t.id');
        if ($hubId) {
            $qb->andWhere('h.id = :hubId')
                ->setParameter('hubId', $hubId);
        } elseif ($tagId) {
            $qb->leftJoin('t.tags', 'ta')
                ->andWhere('ta.id = :tagId')
                ->setParameter('tagId', $tagId);
        } elseif ($searchText != "") {
            $expr[] = $qb->expr()->like('t.name', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.fileExt', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.fileType', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.url', $qb->expr()->literal('%' . $searchText. '%'));
            $qb->andWhere(
                    '(' . implode(' OR ', $expr) .')'
                );
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('t.user', ':userId'),
                    $qb->expr()->eq('hu.shared', ':userId')
                )
            )
                ->setParameter('userId', $userId);
        } else {
            $qb->andWhere('t.user = :userId')
                ->setParameter('userId', $userId);
        }
        if ($onlyCount) {
            $qb ->select('count(t.id)');
            $result = $qb->getQuery()->getArrayResult();
            $result = array_map('current', $result);
            return count($result) > 0 ? count($result) : 0;
        }
        return $qb->orderBy('t.modifiedTime', 'DESC')
            ->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getArrayResult();
    }

    public function findTileDetails($userId, $tileId)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t.id as tileId,t.url as path,t.created,t.name,t.thumbImg,t.source,t.filePath,t.fileSize,t.fileType,t.fileExt,t.mimeType,t.description,t.favicon,t.remoteThumbUrl,u.email as owner,u.id as userId, t.remoteFileId as remoteFileId')
            ->leftJoin('t.hubs', 'h')
            ->leftJoin('h.hubUser', 'hu')
            ->join('t.user', 'u')
            ->andWhere('t.id = :tileId')
            ->setParameter('tileId', $tileId);
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->eq('t.user', ':userId'),
                $qb->expr()->eq('hu.shared', ':userId')
            )
        )
            ->setParameter('userId', $userId)
            ->groupBy('t.id');


        return $qb->orderBy('t.id', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function findOneByUserTile($userId, $tileId)
    {
        $qb = $this->createQueryBuilder('t')
            ->addSelect('u')
            ->addSelect('hu')
            ->leftJoin('t.user', 'u')
            ->leftJoin('u.owner', 'hu')
            ->andWhere('t.id = :tileId')
            ->setParameter('tileId', $tileId);
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->eq('t.user', ':userId'),
                $qb->expr()->eq('hu.shared', ':userId')
            )
        )
            ->setParameter('userId', $userId);

        return $qb->orderBy('t.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByAllTilesgetTilesCount($userId, $searchText = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('count(DISTINCT t.id)')
            ->leftJoin('t.linkedUser', 'lu', 'with', 'lu.id = t.linkedUser')
            ->leftJoin('t.hubs', 'h')
            ->leftJoin('h.hubUser', 'hu')
            ->andWhere('t.isDelete != 1');

        // $qb->andWhere(
        //     $qb->expr()->orX(
        //         $qb->expr()->andX(
        //             $qb->expr()->eq('lu.isActive', true),
        //             $qb->expr()->eq('t.source', ':googleDrive')
        //         ),
        //         $qb->expr()->andX(
        //             $qb->expr()->eq('t.source', ':url')
        //         )
        //     )
        // );
        if ($searchText != "") {
            $expr[] = $qb->expr()->like('t.name', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.fileExt', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.fileType', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.description', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.url', $qb->expr()->literal('%' . $searchText. '%'));
            $qb->andWhere(
                '(' . implode(' OR ', $expr) .')'
            );
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('t.user', ':userId'),
                    $qb->expr()->eq('hu.shared', ':userId')
                )
            );
        } else {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('t.user', ':userId')
                )
            );
        }

        // ->setParameter('googleDrive', 'GoogleDrive')
        //     ->setParameter('url', 'URL')
        $qb->setParameter('userId', $userId);
        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findByRemoteFileIds($linkedUserId)
    {
        return $this->createQueryBuilder('t')
            ->select('t.remoteFileId')
            ->andWhere('t.linkedUser = :linkedUserId')
            ->setParameter('linkedUserId', $linkedUserId)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    public function removeFiles($diffGoogleFileIds, $linkedUserId)
    {
        $qb = $this->createQueryBuilder('t')->delete();
        $qb->andWhere(
            $qb->expr()->in('t.remoteFileId', $diffGoogleFileIds)
        )->andWhere('t.linkedUser = :linkedUser')
        ->setParameter('linkedUser', $linkedUserId);
        return $qb->getQuery()->getResult();
    }

    public function findOneByUrl($userId, $url, $withHttp, $withHttpsW3, $withHttps)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = :userId')
            ->andWhere('t.url = :url OR t.url = :withHttp OR t.url = :withHttpsW3 OR t.url = :withHttps')
            ->andWhere('t.isDelete != 1')
            ->setParameter('userId', $userId)
            ->setParameter('withHttp', $withHttp)
            ->setParameter('withHttpsW3', $withHttpsW3)
            ->setParameter('withHttps', $withHttps)
            ->setParameter('url', $url)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByUserFileId($userId, $fileId)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = :userId')
            ->andWhere('t.remoteFileId = :fileId')
            ->setParameter('userId', $userId)
            ->setParameter('fileId', $fileId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function countTilesByHub($hubId)
    {
        return $this->createQueryBuilder('t')
            ->select('count(DISTINCT t.id)')
            ->join('t.hubs', 'h')
            ->andWhere('h.id = :hubId')
            ->andWhere('t.isDelete != 1')
            ->setParameter('hubId', $hubId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countTilesByTag($tagId)
    {
        return $this->createQueryBuilder('t')
            ->select('count(DISTINCT t.id)')
            ->join('t.tags', 'ta')
            ->andWhere('ta.id = :tagId')
            ->andWhere('t.isDelete != 1')
            ->setParameter('tagId', $tagId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getMultipleTileByIds($tileIds)
    {
        $qb = $this->createQueryBuilder('t');
        $qb->andWhere(
            $qb->expr()->in('t.id', $tileIds)
        );
        return $qb->getQuery()->getResult();
    }

    public function getSearchTiles($userId, $searchText, $offset = 0, $limit = 20)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t.id as tileId,t.url as path,t.created,t.name,t.thumbImg,t.source,t.filePath,t.fileSize,t.fileType,t.fileExt,t.mimeType,t.description,t.favicon,t.remoteThumbUrl,u.email as owner,u.id as userId, t.remoteFileId as remoteFileId')
            ->leftJoin('t.linkedUser', 'lu', 'with', 'lu.id = t.linkedUser')
            ->leftJoin('t.hubs', 'h')
            ->leftJoin('h.hubUser', 'hu')
            ->leftJoin('t.user', 'u')
            ->andWhere('t.isDelete != 1');
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->andX(
                    $qb->expr()->eq('lu.isActive', true),
                    $qb->expr()->eq('t.source', ':googleDrive')
                ),
                $qb->expr()->andX(
                    $qb->expr()->eq('t.source', ':url')
                )
            )
        );
        if ($searchText != "") {
            $expr[] = $qb->expr()->like('t.name', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.fileExt', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.fileType', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.description', $qb->expr()->literal('%' . $searchText. '%'));
            $expr[] = $qb->expr()->like('t.url', $qb->expr()->literal('%' . $searchText. '%'));
            $qb->andWhere(
                '(' . implode(' OR ', $expr) .')'
            );
        }
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->eq('t.user', ':userId'),
                $qb->expr()->eq('hu.shared', ':userId')
            )
        )
            ->setParameter('googleDrive', 'GoogleDrive')
            ->setParameter('url', 'URL')
            ->setParameter('userId', $userId)
            ->groupBy('t.id')
            ->orderBy('t.id', 'DESC');

        return $qb->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getArrayResult();
    }

    public function findOneByHubUserTile($userId, $tileId)
    {
        $qb = $this->createQueryBuilder('t')
            ->addSelect('u')
            ->addSelect('hu')
            ->leftJoin('t.user', 'u')
            ->leftJoin('u.owner', 'hu')
            ->andWhere('t.id = :tileId')
            ->setParameter('tileId', $tileId);
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->eq('t.user', ':userId'),
                $qb->expr()->eq('hu.shared', ':userId')
            )
        )
            ->setParameter('userId', $userId);

        return $qb->orderBy('t.id', 'ASC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public function findOneByFileId($fileId)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.id = :fileId')
            ->andWhere('t.isDelete != 1')
            ->setParameter('fileId', $fileId)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public function getTileCount($userId)
    {
        return $this->createQueryBuilder('t')
            ->select('count(DISTINCT t.id)')
            ->andWhere('t.user = :userId')
            ->andWhere('t.isDelete != 1')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $user
     * @return mixed
     */
    public function deleteAllTilesByUserId($user)
    {
        return $this->createQueryBuilder('t')
            ->delete('App:Tile', 't')
            ->where('t.user = :user')
            ->andWhere('t.source = :source')
            ->setParameters([
                'user' => $user,
                'source' => 'GoogleDrive'
            ])
            ->getQuery()
            ->execute();
    }


}
