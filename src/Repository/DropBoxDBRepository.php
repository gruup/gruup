<?php

namespace App\Repository;

use App\Entity\DropBoxDB;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DropBoxDB|null find($id, $lockMode = null, $lockVersion = null)
 * @method DropBoxDB|null findOneBy(array $criteria, array $orderBy = null)
 * @method DropBoxDB[]    findAll()
 * @method DropBoxDB[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DropBoxDBRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DropBoxDB::class);
    }

    // /**
    //  * @return DropBoxDB[] Returns an array of DropBoxDB objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DropBoxDB
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
