<?php

namespace App\Repository;

use App\Entity\LinkedUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LinkedUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method LinkedUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method LinkedUser[]    findAll()
 * @method LinkedUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkedUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LinkedUser::class);
    }

    //    /**
    //     * @return LinkedUser[] Returns an array of LinkedUser objects
    //     */

    public function findOneByGoogleUser($userId, $googleEmail, $googleId)
    {
        return $this->createQueryBuilder('lu')
            ->where('lu.email = :email')
            ->setParameter('email', $googleEmail)
            ->andWhere('lu.googleId = :googleId')
            ->setParameter('googleId', $googleId)
            ->andWhere('lu.user = :userId')
            ->setParameter('userId', $userId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByGoogleLoginUser($userId)
    {
        return $this->createQueryBuilder('lu')
            ->where('lu.user = :userId')
            ->andWhere('lu.isActive = 1')
            ->setParameter('userId', $userId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByUser($userId)
    {
        return $this->createQueryBuilder('lu')
            ->where('lu.user = :userId')
            ->andWhere('lu.isActive = 1')
            ->setParameter('userId', $userId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
