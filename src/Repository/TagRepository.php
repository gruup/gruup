<?php

namespace App\Repository;

use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Tag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tag[]    findAll()
 * @method Tag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    //    /**
    //     * @return Tag[] Returns an array of Tag objects
    //     */

    public function findByUser($userId)
    {
        return $this->createQueryBuilder('t')
            ->select('t.id,t.id as value,t.created,t.name,u.email as email')
            ->leftJoin('t.user', 'u')
            ->andWhere('t.user = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    public function findOneByTagDetails($userId, $tagId)
    {
        return $this->createQueryBuilder('t')
            ->addSelect('t')
            ->andWhere('t.user = :userId')
            ->andWhere('t.id = :tagId')
            ->setParameter('userId', $userId)
            ->setParameter('tagId', $tagId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByTagAndUser($userId, $tagId)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = :userId')
            ->andWhere('t.id = :tagId')
            ->setParameter('userId', $userId)
            ->setParameter('tagId', $tagId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getUserTagsByTile($tileId, $userId = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t.id as id, t.name as name ')
            ->join('t.tile', 'ti')
            ->andWhere('ti.id = :tileId')
            ->setParameter('tileId', $tileId);

        if ($userId) {
            $qb->andWhere('t.user = :userId')
                ->setParameter('userId', $userId);
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function getUserTagsByHub($hubId, $userId)
    {
        return $this->createQueryBuilder('t')
            ->select('t.id, t.name')
            ->join('t.hubs', 'h')
            ->andWhere('h.id = :hubId')
            ->andWhere('t.user = :userId')
            ->setParameter('userId', $userId)
            ->setParameter('hubId', $hubId)
            ->getQuery()
            ->getArrayResult();
    }

    public function findByTagIds($userId, $tagIds)
    {
        $qb = $this->createQueryBuilder('t')
            ->andWhere('t.user = :userId')
            ->setParameter('userId', $userId);
        $qb->andWhere(
            $qb->expr()->in('t.id', $tagIds)
        );
        return $qb->getQuery()->getResult();
    }

    public function getSearchTags($userId, $searchText, $offset = 0, $limit = 20)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t.id,t.name')
            ->leftJoin('t.user', 'u')
            ->andWhere('t.user = :userId')
            ->setParameter('userId', $userId);

        if ($searchText != "") {
            $expr[] = $qb->expr()->like('t.name', $qb->expr()->literal('%' . $searchText. '%'));
            $qb->andWhere(
                '(' . implode(' OR ', $expr) .')'
            );
        }
        $qb->groupBy('t.id')
            ->orderBy('t.id', 'DESC');

        return $qb->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getArrayResult();
    }
}
