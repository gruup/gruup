<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HubRepository")
 */
class Hub
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="hubs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tile", inversedBy="hubs")
     */
    private $tile;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="hubs")
     */
    private $tags;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hash;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isShared;

    /**
     * Datetime added when transaction is added
     *
     * @ORM\Column(name="created", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created;

    /**
     * Datetime when transaction is updated
     *
     * @ORM\Column(name="updated", type="datetime", columnDefinition="DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HubUser", mappedBy="hub", orphanRemoval=true)
     */
    private $hubUser;

    /**
     * @var string
     * @ORM\Column(type="string", name="hub_type" , nullable=true)
     */
    private $hubType;


    public function __construct()
    {
        $this->tile = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->hubUser = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getIsShared(): ?bool
    {
        return $this->isShared;
    }

    public function setIsShared(bool $isShared): self
    {
        $this->isShared = $isShared;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Tile[]
     */
    public function getTile(): Collection
    {
        return $this->tile;
    }

    public function addTile(Tile $tile): self
    {
        if (!$this->tile->contains($tile)) {
            $this->tile[] = $tile;
        }

        return $this;
    }

    public function removeTile(Tile $tile): self
    {
        if ($this->tile->contains($tile)) {
            $this->tile->removeElement($tile);
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return Collection|HubUser[]
     */
    public function getHubUser(): Collection
    {
        return $this->hubUser;
    }

    public function addHubUser(HubUser $hubUser): self
    {
        if (!$this->hubUser->contains($hubUser)) {
            $this->hubUser[] = $hubUser;
            $hubUser->setHubUser($this);
        }

        return $this;
    }

    public function removeHubUser(HubUser $hubUser): self
    {
        if ($this->hubUser->contains($hubUser)) {
            $this->hubUser->removeElement($hubUser);
            // set the owning side to null (unless already changed)
            if ($hubUser->getHubUser() === $this) {
                $hubUser->setHubUser(null);
            }
        }

        return $this;
    }

    public function getHubType(): ?string
    {
        return $this->hubType;
    }

    public function setHubType(?string $hubType): self
    {
        $this->hubType = $hubType;

        return $this;
    }
}
