<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Linked HubUser
 *
 * @ORM\Table(name="hub_user")
 * @ORM\Entity(repositoryClass="App\Repository\HubUserRepository")
 */
class HubUser
{
    /**
     * Autoincrement & unique id value for each record inserted
     *
     * @var integer
     *
     * @ORM\Column(name="id",                   type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Relation with hub entity
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Hub", cascade={"persist"}, inversedBy="hubUser")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="hub_id",                referencedColumnName="id")
     * })
     */
    private $hub;

    /**
     * Relation with user entity
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="shared")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="shared_id",              referencedColumnName="id")
     * })
     */
    private $shared;

    /**
     * Relation with user entity
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="owner")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="owner_id",               referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * @var string
     * @ORM\Column(type="string", name="email" , nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(name="created_at", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    protected $created;

    /**
     * @ORM\Column(name="updated_at", type="datetime",  columnDefinition="DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")
     */
    protected $updated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getShared(): ?User
    {
        return $this->shared;
    }

    public function setShared(?User $shared): self
    {
        $this->shared = $shared;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getHub(): ?Hub
    {
        return $this->hub;
    }

    public function setHub(?Hub $hub): self
    {
        $this->hub = $hub;

        return $this;
    }
}
