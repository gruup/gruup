<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TileRepository")
 * @ORM\Table(indexes={@ORM\Index(name="remote_file_id_index", columns={"remote_file_id"})})
 */
class Tile
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tiles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * Relation with LinkedUser entity
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\LinkedUser", cascade={"persist"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="linked_user_id", referencedColumnName="id")
     * })
     */
    private $linkedUser;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Hub", mappedBy="tile")
     */
    private $hubs;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="tile")
     */
    private $tags;

    /**
     * @var string
     * @ORM\Column(type="string", name="remote_file_id" , nullable=true)
     */
    private $remoteFileId;

    /**
     * @var string
     * @ORM\Column(type="string", name="name" , nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text", name="file_path" , nullable=true)
     */
    private $filePath;

    /**
     * @var string
     * @ORM\Column(type="text", name="thumb_img" , nullable=true)
     */
    private $thumbImg;

    /**
     * @var string
     * @ORM\Column(type="text", name="remote_thumb_url" , nullable=true)
     */
    private $remoteThumbUrl;

    /**
     * @var string
     * @ORM\Column(type="string", name="favicon" , nullable=true)
     */
    private $favicon;

    /**
     * @var string
     * @ORM\Column(type="string", name="color" , nullable=true)
     */
    private $color;

    /**
     * @var string
     * @ORM\Column(type="string", name="file_type" , nullable=true)
     */
    private $fileType;

    /**
     * @var string
     * @ORM\Column(type="string", name="file_size", nullable=true)
     */
    private $fileSize;

    /**
     * @var string
     * @ORM\Column(type="string", name="file_ext" , nullable=true)
     */
    private $fileExt;

    /**
     * @var string
     * @ORM\Column(type="string", name="mime_type", nullable=true)
     */
    private $mimeType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text", nullable=true)
     */
    private $url;

    /**
     *
     * @var string
     * @ORM\Column(type="text", name="description", nullable = true)
     */
    private $description;

    /**
     *
     * @var string
     * @ORM\Column(type="text", name="embed_code", nullable = true)
     */
    private $embedCode;

    /**
     *
     * @var Boolean
     *
     * @ORM\Column(type="boolean", name="is_view", nullable=true)
     */
    private $isView;

    /**
     *
     * @var Boolean
     *
     * @ORM\Column(type="boolean", name="is_delete", nullable=true, options={"default"="0"})
     */
    private $isDelete;

    /**
     * Datetime added when transaction is added
     *
     * @ORM\Column(name="created", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created;

    /**
     * Datetime added when transaction is added
     *
     * @ORM\Column(name="modified_time", type="datetime", nullable=true)
     */
    private $modifiedTime;

    /**
     * Datetime when transaction is updated
     *
     * @ORM\Column(name="updated", type="datetime", columnDefinition="DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")
     */
    private $updated;

    public function __construct()
    {
        $this->hubs = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRemoteFileId(): ?string
    {
        return $this->remoteFileId;
    }

    public function setRemoteFileId(?string $remoteFileId): self
    {
        $this->remoteFileId = $remoteFileId;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getThumbImg(): ?string
    {
        return $this->thumbImg;
    }

    public function setThumbImg(?string $thumbImg): self
    {
        $this->thumbImg = $thumbImg;

        return $this;
    }

    public function getFavicon(): ?string
    {
        return $this->favicon;
    }

    public function setFavicon(?string $favicon): self
    {
        $this->favicon = $favicon;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getFileType(): ?string
    {
        return $this->fileType;
    }

    public function setFileType(?string $fileType): self
    {
        $this->fileType = $fileType;

        return $this;
    }

    public function getFileSize(): ?string
    {
        return $this->fileSize;
    }

    public function setFileSize(?string $fileSize): self
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    public function getFileExt(): ?string
    {
        return $this->fileExt;
    }

    public function setFileExt(?string $fileExt): self
    {
        $this->fileExt = $fileExt;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(?string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsView(): ?bool
    {
        return $this->isView;
    }

    public function setIsView(?bool $isView): self
    {
        $this->isView = $isView;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLinkedUser(): ?LinkedUser
    {
        return $this->linkedUser;
    }

    public function setLinkedUser(?LinkedUser $linkedUser): self
    {
        $this->linkedUser = $linkedUser;

        return $this;
    }

    /**
     * @return Collection|Hub[]
     */
    public function getHubs(): Collection
    {
        return $this->hubs;
    }

    public function addHub(Hub $hub): self
    {
        if (!$this->hubs->contains($hub)) {
            $this->hubs[] = $hub;
            $hub->addTile($this);
        }

        return $this;
    }

    public function removeHub(Hub $hub): self
    {
        if ($this->hubs->contains($hub)) {
            $this->hubs->removeElement($hub);
            $hub->removeTile($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsDelete(): ?bool
    {
        return $this->isDelete;
    }

    public function setIsDelete(?bool $isDelete): self
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    public function getEmbedCode(): ?string
    {
        return $this->embedCode;
    }

    public function setEmbedCode(?string $embedCode): self
    {
        $this->embedCode = $embedCode;

        return $this;
    }

    public function getModifiedTime(): ?\DateTimeInterface
    {
        return $this->modifiedTime;
    }

    public function setModifiedTime(\DateTimeInterface $modifiedTime): self
    {
        $this->modifiedTime = $modifiedTime;

        return $this;
    }

    public function getRemoteThumbUrl(): ?string
    {
        return $this->remoteThumbUrl;
    }

    public function setRemoteThumbUrl(?string $remoteThumbUrl): self
    {
        $this->remoteThumbUrl = $remoteThumbUrl;

        return $this;
    }
}
