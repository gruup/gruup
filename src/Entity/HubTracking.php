<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * HubTracking
 *
 * @ORM\Table(name="hub_tracking")
 * @ORM\Entity(repositoryClass="App\Repository\HubTrackingRepository")
 */
class HubTracking
{
	/**
     * Autoincrement & unique id value for each record inserted
     *
     * @var integer
     *
     * @ORM\Column(name="id",                   type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hub;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tile;

    /**
     * @ORM\Column(name="action_type", type="string")
     */
    protected $actionType;

    /**
     * @ORM\Column(name="created_at", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    protected $created;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHub(): ?string
    {
        return $this->hub;
    }

    public function setHub(?string $hub): self
    {
        $this->hub = $hub;

        return $this;
    }

    public function getTile(): ?string
    {
        return $this->tile;
    }

    public function setTile(?string $tile): self
    {
        $this->tile = $tile;

        return $this;
    }

    public function getActionType()
    {
        return $this->actionType;
    }

    public function setActionType($actionType)
    {
        $this->actionType = $actionType;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }
}

?>