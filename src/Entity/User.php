<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $account_type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tile", mappedBy="user", orphanRemoval=true)
     */
    private $tiles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Hub", mappedBy="user", orphanRemoval=true)
     */
    private $hubs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tag", mappedBy="user", orphanRemoval=true)
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HubUser", mappedBy="shared", orphanRemoval=true)
     */
    private $shared;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HubUser", mappedBy="owner", orphanRemoval=true)
     */
    private $owner;

    /**
     *
     * @var string
     * @ORM\Column(type="text", name="image", nullable = true)
     */
    private $image;

    /**
     * string value added for the confirmation token sent to activate the user
     *
     * @var string
     *
     * @ORM\Column(name="confirmation_token", type="string" , length=254 ,nullable=true)
     */
    protected $confirmationToken;

    /**
     * string value added for the confirmation token sent to activate the user
     *
     * @var string
     *
     * @ORM\Column(name="api_token", type="string" , length=254 ,nullable=true)
     */
    protected $api_token;

    /**
     * datetime added when password recovery is requested by member
     *
     * @var datetime
     *
     * @ORM\Column(name="password_requested_at", type="datetime" , nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * string value added for the activation token sent to new register user
     *
     * @var string
     *
     * @ORM\Column(name="activation_token", type="string" , length=254 ,nullable=true)
     */
    protected $activationToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $controlToken;

    public function __construct() {
        $this->tiles = new ArrayCollection();
        $this->hubs = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->shared = new ArrayCollection();
        $this->owner = new ArrayCollection();
    }

    public function __toString() {
        return $this->email;
    }

    public function getId() {
        return $this->id;
    }

    public function getPassword(): ?string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getIsActive(): ?bool {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self {
        $this->is_active = $is_active;

        return $this;
    }

    public function getAccountType(): ?string {
        return $this->account_type;
    }

    public function setAccountType(string $account_type): self {
        $this->account_type = $account_type;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self {
        $this->created = $created;

        return $this;
    }

    public function getPlainPassword() {
        return $this->plainPassword;
    }

    public function setPlainPassword($password) {
        $this->plainPassword = $password;
    }

    public function getUsername() {
        return $this->email;
    }

    public function getSalt() {
        return null;
    }

    public function getRoles() {
        $roles = ['ROLE_USER'];
        if ($this->getIsActive()) {
            $roles[] = 'ROLE_ACTIVE';
        }
        return $roles;
    }

    public function eraseCredentials() {

    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize() {
        return serialize(
                array(
                    $this->id,
                    $this->email,
                    $this->password,
                )
        );
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized) {
        list(
                $this->id,
                $this->email,
                $this->password,
                ) = unserialize($serialized);
    }

    /**
     * @return Collection|Tile[]
     */
    public function getTiles(): Collection {
        return $this->tiles;
    }

    public function addTile(Tile $tile): self {
        if (!$this->tiles->contains($tile)) {
            $this->tiles[] = $tile;
            $tile->setUser($this);
        }

        return $this;
    }

    public function removeTile(Tile $tile): self {
        if ($this->tiles->contains($tile)) {
            $this->tiles->removeElement($tile);
            // set the owning side to null (unless already changed)
            if ($tile->getUser() === $this) {
                $tile->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Hub[]
     */
    public function getHubs(): Collection {
        return $this->hubs;
    }

    public function addHub(Hub $hub): self {
        if (!$this->hubs->contains($hub)) {
            $this->hubs[] = $hub;
            $hub->setUser($this);
        }

        return $this;
    }

    public function removeHub(Hub $hub): self {
        if ($this->hubs->contains($hub)) {
            $this->hubs->removeElement($hub);
            // set the owning side to null (unless already changed)
            if ($hub->getUser() === $this) {
                $hub->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection {
        return $this->tags;
    }

    public function addTag(Tag $tag): self {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->setUser($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            // set the owning side to null (unless already changed)
            if ($tag->getUser() === $this) {
                $tag->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HubUser[]
     */
    public function getShared(): Collection {
        return $this->shared;
    }

    public function addShared(HubUser $shared): self {
        if (!$this->shared->contains($shared)) {
            $this->shared[] = $shared;
            $shared->setShared($this);
        }

        return $this;
    }

    public function removeShared(HubUser $shared): self {
        if ($this->shared->contains($shared)) {
            $this->shared->removeElement($shared);
            // set the owning side to null (unless already changed)
            if ($shared->getShared() === $this) {
                $shared->setShared(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HubUser[]
     */
    public function getOwner(): Collection {
        return $this->owner;
    }

    public function addOwner(HubUser $owner): self {
        if (!$this->owner->contains($owner)) {
            $this->owner[] = $owner;
            $owner->setOwner($this);
        }

        return $this;
    }

    public function removeOwner(HubUser $owner): self {
        if ($this->owner->contains($owner)) {
            $this->owner->removeElement($owner);
            // set the owning side to null (unless already changed)
            if ($owner->getOwner() === $this) {
                $owner->setOwner(null);
            }
        }

        return $this;
    }

    public function getImage(): ?string {
        return $this->image;
    }

    public function setImage(?string $image): self {
        $this->image = $image;

        return $this;
    }

    public function getConfirmationToken(): ?string {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getApiToken(): ?string {
        return $this->api_token;
    }

    public function setApiToken(?string $apiToken): self {
        $this->api_token = $apiToken;
        return $this;
    }

    public function getPasswordRequestedAt(): ?\DateTimeInterface {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(?\DateTimeInterface $passwordRequestedAt): self {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    public function getActivationToken(): ?string {
        return $this->activationToken;
    }

    public function setActivationToken(?string $activationToken): self {
        $this->activationToken = $activationToken;

        return $this;
    }

    public function getControlToken(): ?string {
        return $this->controlToken;
    }

    public function setControlToken(?string $controlToken): self {
        $this->controlToken = $controlToken;

        return $this;
    }

}
