<?php

//src/Entity/DropBoxDB.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DropBoxDBRepository")
 */
class DropBoxDB {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $account_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dbUid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token_type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_on;

    public function gettoken_type(): ?string {
        return $this->token_type;
    }

    public function settoken_type(string $token_type): self {
        $this->token_type = $token_type;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getaccount_id(): ?int {
        return $this->account_id;
    }

    public function setaccount_id(string $account_id): self {
        $this->account_id = $account_id;

        return $this;
    }

    public function getdbUid(): ?int {
        return $this->dbUid;
    }

    public function setdbUid(string $dbUid): self {
        $this->dbUid = $dbUid;

        return $this;
    }

    public function getUser(): ?int {
        return $this->user_id;
    }

    public function setUser(int $user): self {
        $this->user_id = $user;

        return $this;
    }

    public function getToken(): ?string {
        return $this->token;
    }

    public function setToken(string $token): self {
        $this->token = $token;

        return $this;
    }

    public function getCreatedOn(): ?\DateTimeInterface {
        return $this->created_on;
    }

    public function setCreatedOn(\DateTimeInterface $created_on): self {
        $this->created_on = $created_on;
        return $this;
    }

}
