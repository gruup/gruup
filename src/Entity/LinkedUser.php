<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Linked User
 *
 * @ORM\Table(name="linked_user")
 * @ORM\Entity(repositoryClass="App\Repository\LinkedUserRepository")
 */
class LinkedUser
{
    /**
     * Autoincrement & unique id value for each record inserted
     *
     * @var integer
     *
     * @ORM\Column(name="id",                   type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Relation with user entity
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="user_id",                referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string", name="email" , nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", name="google_id" , nullable=true)
     */
    private $googleId;

    /**
     * @var string
     * @ORM\Column(type="string", name="source" , nullable=true)
     */
    private $source;

    /**
     *
     * @var string
     * @ORM\Column(type="text", name="access_token", nullable = true)
     */
    private $accessToken;

    /**
     *
     * @var string
     * @ORM\Column(type="text", name="refresh_token", nullable = true)
     */
    private $refreshToken;

    /**
     *
     * @var string
     * @ORM\Column(type="text", name="token", nullable = true)
     */
    private $token;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tile", mappedBy="user", orphanRemoval=true)
     */
    private $tiles;

    /**
     *
     * @var Boolean
     *
     * @ORM\Column(type="boolean", name="is_active", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(name="created_at", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    protected $created;

    /**
     * @ORM\Column(name="updated_at", type="datetime",  columnDefinition="DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")
     */
    protected $updated;

    /**
     * @ORM\Column(name="sync_start", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    protected $syncStart;

    /**
     * @ORM\Column(name="sync_end", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    protected $syncEnd;

    /**
     * @ORM\Column(name="start_page_token", type="text", nullable=true)
     */
    protected $startPageToken;


    public function __construct()
    {
        $this->tiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(?string $accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(?string $refreshToken): self
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Tile[]
     */
    public function getTiles(): Collection
    {
        return $this->tiles;
    }

    public function addTile(Tile $tile): self
    {
        if (!$this->tiles->contains($tile)) {
            $this->tiles[] = $tile;
            $tile->setUser($this);
        }

        return $this;
    }

    public function removeTile(Tile $tile): self
    {
        if ($this->tiles->contains($tile)) {
            $this->tiles->removeElement($tile);
            // set the owning side to null (unless already changed)
            if ($tile->getUser() === $this) {
                $tile->setUser(null);
            }
        }

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getSyncStart(): ?\DateTimeInterface
    {
        return $this->syncStart;
    }

    public function setSyncStart(\DateTimeInterface $syncStart): self
    {
        $this->syncStart = $syncStart;

        return $this;
    }

    public function getSyncEnd(): ?\DateTimeInterface
    {
        return $this->syncEnd;
    }

    public function setSyncEnd(\DateTimeInterface $syncEnd): self
    {
        $this->syncEnd = $syncEnd;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getStartPageToken(): ?string
    {
        return $this->startPageToken;
    }

    public function setStartPageToken(?string $startPageToken): self
    {
        $this->startPageToken = $startPageToken;

        return $this;
    }
}
