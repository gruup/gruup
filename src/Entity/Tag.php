<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    /**
     * Autoincrement & unique id value for each record inserted
     *
     * @var integer
     *
     * @ORM\Column(name="id",                   type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tags")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * File in the tag.
     *
     * @var                                            File[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Tile", mappedBy="tags")
     **/
    private $tile;

    /**
     * Hub in the tag
     *
     * @var hub
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Hub", mappedBy="tags")
     */
    private $hubs;

    /**
     * @var string
     * @ORM\Column(type="string", name="name" , nullable=true)
     */
    private $name;

    /**
     * Datetime added when transaction is added
     *
     * @ORM\Column(name="created", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created;

    /**
     * Datetime when transaction is updated
     *
     * @ORM\Column(name="updated", type="datetime",  columnDefinition="DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")
     */
    private $updated;

    public function __construct()
    {
        $this->tile = new ArrayCollection();
        $this->hubs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Tile[]
     */
    public function getTile(): Collection
    {
        return $this->tile;
    }

    public function addTile(Tile $tile): self
    {
        if (!$this->tile->contains($tile)) {
            $this->tile[] = $tile;
            $tile->addTag($this);
        }

        return $this;
    }

    public function removeTile(Tile $tile): self
    {
        if ($this->tile->contains($tile)) {
            $this->tile->removeElement($tile);
            $tile->removeTag($this);
        }

        return $this;
    }

    /**
     * @return Collection|Hub[]
     */
    public function getHubs(): Collection
    {
        return $this->hubs;
    }

    public function addHub(Hub $hub): self
    {
        if (!$this->hubs->contains($hub)) {
            $this->hubs[] = $hub;
            $hub->addTag($this);
        }

        return $this;
    }

    public function removeHub(Hub $hub): self
    {
        if ($this->hubs->contains($hub)) {
            $this->hubs->removeElement($hub);
            $hub->removeTag($this);
        }

        return $this;
    }
}
