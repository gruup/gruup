<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Entity\Tile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ApiTagController extends Controller
{
    /**
     * @Route("/api/tag/{userId}", name="api_tag_get_all_tags", methods={"GET"}, options={"expose"=true})
     * @param $userId
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAllTags($userId, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false
                && !$this->getUser() && $this->getUser()->getId() != $userId
            ) {
                throw new AccessDeniedHttpException('Access Denied');
            }

            $response = $this->get('tag.manager')->getAllTagLists();
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTagController getAllTags", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/tag/details/{userId}/{tagId}",
     *         name="api_view_tag_details",
     *         methods={"GET"}, options={"expose"=true}
     * )
     * @param                                                                              $userId
     * @param                                                                              $tagId
     * @param \Symfony\Component\HttpFoundation\Request                                    $request
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getTagDetails(
        $userId,
        $tagId,
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker
    ): JsonResponse {
        $response = [];
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false
                && !$this->getUser() && $this->getUser()->getId() != $userId
            ) {
                throw new AccessDeniedHttpException('Access Denied');
            }

            $response = $this->get('tag.manager')->getTagInfo($userId, $tagId, $request);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTagController getTagDetails", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }
}
