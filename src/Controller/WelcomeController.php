<?php

namespace App\Controller;

use App\Entity\Hub;
use App\Entity\Tile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class WelcomeController extends Controller
{
    /**
     * @Route("/welcome", name="welcome_home", options={"expose"=true})
     * @return Response
     */
    public function welcome(): Response
    {
        return $this->render(
            'welcome/welcome.html.twig',
            [
            "PRE_LOADED_STATE" => ""
            ]
        );
    }

    /**
     * @Route("/welcome/recent", name="welcome_recent")
     * @return Response
     */
    public function recent(): Response
    {
        $now = new \DateTime();
        $back = $now->sub(\DateInterval::createFromDateString('30 days'));

        $hubs = $this->getDoctrine()->getRepository(Hub::class)->findBy(
            [
            'user' => $this->getUser()->getId(),
            'created' => $back
            ]
        );

        $tiles = $this->getDoctrine()->getRepository(Tile::class)->findBy(
            [
            'user' => $this->getUser()->getId(),
            'created' => $back
            ]
        );

        return $this->render(
            'welcome/recent.html.twig',
            [
            'hubs' => $hubs,
            'tiles' => $tiles
            ]
        );
    }

    /**
     * @Route("/welcome/shared", name="welcome_shared")
     * @return Response
     */
    public function shared(): Response
    {
        return $this->render('welcome/recent.html.twig');
    }
    
    /**
     * @Route("/welcome/view/hub/{hash}", name="view_hub_details", options={"expose"=true})
     * @param $hash
     * @return Response
     */
    public function viewHubDetails($hash): Response
    {
        return $this->render('welcome/welcome.html.twig');
    }

    /**
     * @Route("/welcome/view/tag/{tagId}", name="view_tag_details", options={"expose"=true})
     * @param $tagId
     * @return Response
     */
    public function viewTagDetails($tagId): Response
    {
        return $this->render('welcome/welcome.html.twig');
    }
}
