<?php

namespace App\Controller;

use App\Entity\Hub;
use App\Entity\Tag;
use App\Entity\Tile;
use App\Entity\LinkedUser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ApiTileController extends Controller
{
    /**
     * @Route("/api/tile/{userId}", name="api_tile_get_all_tiles", methods={"GET"}, options={"expose"=true})
     *
     * @param                                                                              $userId
     * @param \Symfony\Component\HttpFoundation\Request                                    $request
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAllTiles(
        $userId,
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker
    ): JsonResponse {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false &&
                !$this->getUser() && $this->getUser()->getId() !== $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }

            $em = $this->getDoctrine()->getManager();

            $searchText = "";

            if ($request->get('searchText')  && $request->get('searchText') != "") {
                $searchText = $request->get('searchText');
            }

            $tilesCount = $em->getRepository(Tile::class)->findByAllTilesgetTilesCount($userId, $searchText);
            $userCreatedTilesCount = $em->getRepository(Tile::class)->findByUserTiles($userId);
            $linkedUser = $em->getRepository(LinkedUser::class)->findOneByGoogleLoginUser($userId);
            
            $syncTilesCount = 0;
            if ($linkedUser) {
                $syncTilesCount = $em->getRepository(Tile::class)->getSyncTilesCount($userId, $linkedUser->getId());
            }

            $response['result'] = 'success';
            $response['total'] = $tilesCount;
            $response['firstTile'] = ($userCreatedTilesCount > 0 || $syncTilesCount > 0) ? false : true;
            $response['googleSync'] = ($linkedUser) ? false : true;
            $response['data'] = [];
            if ($tilesCount > $request->get('offset')) {
                $response['data'] = $this->get('tile.manager')->getTilesByOptions($userId, $request);
            }
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController getAllTiles", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/tiles/new", name="api_add_new_tile", methods={"POST"}, options={"expose"=true})
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function addNewTile(Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false || !$this->getUser()) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $user = $this->getUser();
            $payload = json_decode($request->getContent(), true);
            $response = $this->get('tile.manager')->addTile($payload);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController addNewTile", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/tile-details/{tileId}", name="api_get_tile_details", methods={"GET"}, options={"expose"=true})
     * @return JsonResponse
     */
    public function getTileDetails($tileId, Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];

        try {
            if ($this->getUser()) {
                $userId = $this->getUser()->getId();
            } else {
                $em = $this->getDoctrine()->getManager();
                $tile = $em->getRepository(Tile::class)->find($tileId);
                if (!$tile) {
                    return $this->json(['result' => 'error', 'error' => 'We couldn\'t find that Tile.']);
                }
                $userId = $tile->getUser()->getId();
            }
            $payload = json_decode($request->getContent(), true);
            $response['result'] = 'success';
            $response['data'] = $this->get('tile.manager')->getTilesDetails($userId, $tileId);
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController getTileDetails", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/tile-hub/{userId}", name="api_hub_add_tiles_to_hub", methods={"PATCH"})
     * @param $userId
     * @param Request                       $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function addTileToHub($userId, Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];

        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false &&
                !$this->getUser() && $this->getUser()->getId() !== $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $payload = json_decode($request->getContent(), true);
            $response = $this->get('tile.manager')->addHubForTile($userId, $payload);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController addTileToHub", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/selected-hub/{tileId}", name="api_tile_selected_hub", methods={"GET"})
     * @param $tileId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getSelectedHub($tileId, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if ($this->getUser()) {
                $userId = $this->getUser()->getId();
            } else {
                $em = $this->getDoctrine()->getManager();
                $tile = $em->getRepository(Tile::class)->find($tileId);
                if (!$tile) {
                    return $this->json(['result' => 'error', 'error' => 'We couldn\'t find that Tile.']);
                }
                $userId = $tile->getUser()->getId();
            }
            $response = $this->get('tile.manager')->getSelectedHubIds($userId, $tileId);
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController getSelectedHub", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }


    /**
     * @Route("/api/tile-tag/{userId}", name="api_tag_add_tiles_to_tag", methods={"PATCH"})
     * @param $userId
     * @param Request                       $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function addTileToTag($userId, Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false &&
                !$this->getUser() && $this->getUser()->getId() !== $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $payload = json_decode($request->getContent(), true);
            $response = $this->get('tile.manager')->addTagForTile($userId, $payload);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController addTileToTag", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/selected-tag/{tileId}", name="api_tile_selected_tag", methods={"GET"})
     * @param $tileId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getSelectedTag($tileId, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser() && $this->getUser() != $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $response = $this->get('tile.manager')->getSelectedTagIds($tileId);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController getSelectedTag", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/tiles/{id}/delete", name="api_delete_tile", methods={"GET"}, options={"expose"=true})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function deleteTile(Request $request, AuthorizationCheckerInterface $authorizationChecker, Tile $tile): JsonResponse
    {
        if (!$this->getUser()) {
            return $this->json(['result' => 'error', 'error' => 'You\'re not logged in. Please login.']);
        }

        $response = [];
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false
                && !$this->getUser()
                || ($tile->getUser()->getId() !== $this->getUser()->getId())
            ) {
                throw new AccessDeniedHttpException('Access Denied');
            }

            $payload = json_decode($request->getContent(), true);
            $em = $this->getDoctrine()->getManager();
            if ($tile->getFileType() == 'URL') {
                $em->remove($tile);
            } else {
                $tile->setIsDelete(true);
            }
            $em->flush();
            $this->get('tag.manager')->removeUnusedTag();
            $response = ['result' => 'success', 'message' => 'Your Tile has been deleted.'];
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController deleteTile", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/add-hub-tag-on-tiles/{userId}", name="api_add_hub_tag_on_tiles", methods={"POST"}, options={"expose"=true})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function addHubsAndTagsOnTiles($userId, Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser()) || $this->getUser()->getId() != $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $user = $this->getUser();
            $payload = json_decode($request->getContent(), true);

            if (!isset($payload["tileIds"]) && empty($payload["tileIds"])) {
                return $this->json(['result' => 'error', 'error' => 'You must select a Tile first.']);
            }
            $response = $this->get('tile.manager')->addHubAndTagOnTiles($user->getId(), $payload);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController addHubsAndTagsOnTiles", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/search", name="api_search_tiles", methods={"POST"}, options={"expose"=true})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function searchTiles(Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser())) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
            $payload = json_decode($request->getContent(), true);
            if (isset($payload["searchText"]) && empty($payload["searchText"])) {
                return $this->json([]);
            }
            $searchText = $payload["searchText"];
            $tilesCount = $em->getRepository(Tile::class)
            ->findByAllTilesgetTilesCount($user->getId(), $searchText);
            $response['result'] = 'success';
            $response['total'] = $tilesCount;
            $response['data'] = $this->get('search.manager')
            ->getSearchResult($user->getId(), $searchText);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController searchTiles", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/tiles/delete/{userId}", name="api_multi_delete_tiles", methods={"POST"}, options={"expose"=true})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function multiDeleteTiles($userId, Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser()) || $this->getUser()->getId() != $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $payload = json_decode($request->getContent(), true);
            $response = $this->get('tile.manager')->deleteTiles($userId, $payload);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController multiDeleteTiles", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/remove-hub-on-tiles/{userId}", name="api_remove_hub_on_tiles", methods={"POST"}, options={"expose"=true})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function removeHubOnTiles($userId, Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser())
                || $this->getUser()->getId() != $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $user = $this->getUser();
            $payload = json_decode($request->getContent(), true);
            $response = $this->get('hub.manager')->removeHubs($payload);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController removeHubOnTiles", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/sync-tile-hint", name="api_sync_tile_hint", methods={"GET"}, options={"expose"=true})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function syncTileHint(Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser())) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $user = $this->getUser();
            $client = $this->get('google.drive.client.manager')->client($user->getId());
            if (!$client['client']) {
                return $this->json([
                    'status' => 'error',
                    'message' => ' ERROR sync Tile hint! ' . $client['error'],
                    'redirect' => 'welcome_home'
                ]);
            }
            $response = $this->get('fileListsManager')->getSyncHint($client['client']);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController removeHubOnTiles", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/save-thumb-image/{id}", name="api_save_thumb_image", methods={"GET"}, options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\Response
     * @return JsonResponse
     */
    public function saveThumbImage($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $tile = $em->getRepository(Tile::class)->find($id);
        if (!$tile) {
            return [
                    'status' => 'error',
                    'message' => 'We couldn\'t find that Tile.'
                ];
        }
        $user = $this->getUser();
        $client = $this->get('google.drive.client.manager')->client($user->getId());
        if (!$client['client']) {
            return $this->json([
                'status' => 'error',
                'message' => ' ERROR get file! ' . $client['error']
            ]);
        }

        return $this->get('fileListsManager')->saveThumb($tile, $client);
    }
}
