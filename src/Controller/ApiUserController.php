<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Dotenv\Dotenv;

class ApiUserController extends Controller {

    /**
     * @Route("/api/account/{userId}", name="api_get_account", methods={"GET"}, options={"expose"=true})
     * @param $userId
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function getAccount($userId, Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser()) || $this->getUser()->getId() != $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $user = $this->getUser();
            $response = ["data" => $this->get('user.manager')->getAccountInfo($user)];
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiTileController getAllTiles", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/username/{userName}", name="api_get_username", methods={"GET"}, options={"expose"=true})
     * @param $userName
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function getAccountByName($userName, Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse {


        return $this->json($userName);
    }

}
