<?php

namespace App\Controller;

use App\Form\UserType;
use App\Form\ForgotPasswordType;
use App\Form\ResendEmailType;
use App\Form\UpdatePasswordType;
use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Entity\HubUser;
use App\Entity\Tile;
use Symfony\Component\Validator\Constraints\Date;

class SecurityController extends Controller
{
    /**
     * @Route("/register", name="user_registration", options={"expose"=true})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        if ($this->getUser() && $this->getUser()->getIsActive()) {
            return $this->redirectToRoute('welcome_home');
        }
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $tokenGenerator = new UriSafeTokenGenerator();
                $activationToken = $tokenGenerator->generateToken();
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());

                $user->setPassword($password)
                    ->setAccountType("FREE")
                    ->setIsActive(false)
                    ->setActivationToken($activationToken)
                    ->setCreated(new \DateTime());

                // 4) save the User!
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);

                $tile = new Tile();
                $tile->setUser($user)
                    ->setName("CEO Welcome Letter")
                    ->setFilePath("/pdf/CEO-Welcome-Letter.pdf")
                    ->setDescription("CEO Welcome Letter")
                    ->setFileType("pdf")
                    ->setFavicon("/favicon.ico")
                    ->setFilesize(77349)
                    ->setFileExt("pdf")
                    ->setMimeType("application/pdf")
                    ->setSource("gruup")
                    ->setIsView(false)
                    ->setIsDelete(false)
                    ->setCreated(new \DateTime())
                    ->setUpdated(new \DateTime())
                    ->setModifiedTime(new \DateTime());
                $em->persist($tile);

                $tile2 = new Tile();
                $tile2->setUser($user)
                    ->setName("Gruup Five Top Tips")
                    ->setFilePath("http://static1.squarespace.com/static/5b928f6ea2772cf6ca598d0c/5bbc84c04785d3ab538d2746/5bc9e763b208fcae320b0603/1540814118595/check-class-desk-7103.jpg?format=1000w")
                    ->setThumbImg("http://static1.squarespace.com/static/5b928f6ea2772cf6ca598d0c/5bbc84c04785d3ab538d2746/5bc9e763b208fcae320b0603/1540814118595/check-class-desk-7103.jpg?format=1000w")
                    ->setDescription("Gruup Five Top Tips")
                    ->setFileType("URL")
                    ->setFavicon("https://static1.squarespace.com/static/5b928f6ea2772cf6ca598d0c/t/5b9291fe352f53daeea21803/favicon.ico")
                    ->setSource("gruup")
                    ->setUrl("https://www.gruup.space/blog/2018/10/8/gruup-five-top-tips")
                    ->setDescription("“There’s more than one way to skin a cat”, they say. Less messily, there’s more than one way to take advantage of the functionality of Gruup, to help you and your teams work smarter, work better and work with more, well, teamwork.")
                    ->setIsView(false)
                    ->setIsDelete(false)
                    ->setCreated(new \DateTime())
                    ->setUpdated(new \DateTime())
                    ->setModifiedTime(new \DateTime());
                $em->persist($tile2);
                $em->flush();

                $tile3 = new Tile();
                $tile3->setUser($user)
                    ->setName("Get Started")
                    ->setFilePath("https://i.vimeocdn.com/video/753582654_640.jpg")
                    ->setThumbImg("https://i.vimeocdn.com/video/753582654_640.jpg")
                    ->setDescription("Get Started")
                    ->setFileType("video")
                    ->setFavicon("https://i.vimeocdn.com/favicon/main-touch_180")
                    ->setSource("gruup")
                    ->setUrl("https://vimeo.com/312508338/e6b1cb0144")
                    ->setDescription("Get Started")
                    ->setIsView(false)
                    ->setIsDelete(false)
                    ->setCreated(new \DateTime())
                    ->setUpdated(new \DateTime())
                    ->setModifiedTime(new \DateTime());
                $em->persist($tile3);
                $em->flush();

                // Send register user info mail
                $this->get('user.manager')
                    ->sendRegisterInfoMail($user)
                    ->sendWelcomeMail($user, $activationToken);

                $session = new Session();
                $session->getFlashBag()->add(
                    'sentActiveToken',
                    'Please check your email. You need to activate your account with the link we\'ve 
                    just sent you before you can login.'
                );

                return $this->redirectToRoute('login');
            } catch (UniqueConstraintViolationException $uniqueConstraintViolationException) {
                $form->get('email')->addError(new FormError("Email address is already registered"));
            }
        }

        return $this->render(
            'security/register.html.twig',
            array('form' => $form->createView(),'page' => 'register')
        );
    }

    /**
     * @route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser() && $this->getUser()->getIsActive()) {
            return $this->redirectToRoute('welcome_home');
        }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'security/login.html.twig',
            [
                'last_username' => $lastUsername,
                'error'         => $error,
                'page' => 'login'
            ]
        );
    }

    /**
     * @route("/forgot-password", name="forgotpassword")
     * @param Request $request
     * @return Response
     */
    public function forgotPassword(Request $request): Response
    {
        if ($this->getUser() && $this->getUser()->getIsActive()) {
            return $this->redirectToRoute('welcome_home');
        }
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ForgotPasswordType::class);
        $form->handleRequest($request);

        $email = $form->get('email')->getData();
        $exitsUser = $em->getRepository(User::class)->findOneByEmail($email);
        $tokenGenerator = new UriSafeTokenGenerator();

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$exitsUser) {
                $session->getFlashBag()->add(
                    'error',
                    'Sorry, that Email does not exist in your account.'
                );
                return $this->render(
                    'security/forgotpassword.html.twig',
                    [
                        'form' => $form->createView(),'error'
                    ]
                );
            }
            $token = $tokenGenerator->generateToken();
            $exitsUser->setConfirmationToken($token);
            $exitsUser->setPasswordRequestedAt(new \Datetime());
            $em->persist($exitsUser);
            $em->flush();

            $this->get('email.send')->sendEmail(
                $email,
                'Gruup password reset',
                $this->getResetPasswordMailBody($token)
            );
            $session->getFlashBag()->add('success', 'Send reset password link in your email.');
        }

        return $this->render(
            'security/forgotpassword.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @param $token
     * @return string
     */
    private function getResetPasswordMailBody($token)
    {
        $url =  $this->generateUrl(
            'user_reset_token',
            [
                'token' => $token
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        return $this->renderView(
            'security/reset-password.html.twig',
            [
                'url' => $url
            ]
        );
    }

    /**
     * @route("/reset/token/{token}", name="user_reset_token")
     * @param $token
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function resetToken($token, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['confirmationToken' => $token]);
        $error = null;
        $currentDate = new \DateTime('now');
        $currentDate->modify('-2 days');
        if (!$user) {
            $error = true;
        } else {
            if ($user->getPasswordRequestedAt() < $currentDate) {
                $error = true;
            }
        }
        if ($error) {
            $session->getFlashBag()->add('errorToken', 'Invalid token.');
            return $this->redirect($this->generateUrl('user_registration'));
        }
        $form = $this->createForm(UpdatePasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('plainPassword')->getData();
            $password = $passwordEncoder->encodePassword($user, $plainPassword);
            $user->setPassword($password)
                ->setConfirmationToken('');
            $em->persist($user);
            $em->flush();

            $token = new UsernamePasswordToken($user, null, "main", $user->getRoles());
            $this->get("security.token_storage")->setToken($token);
            $this->get("session")->set("_security_main", serialize($token));

            $session->getFlashBag()->add('success', 'Password changed successfully.');
            return $this->redirect($this->generateUrl('welcome_home'));
        }

        return $this->render(
            'security/resetPassword.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @route("/privacy")
     * @return Response
     */
    public function privacy(): Response
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://www.iubenda.com/api/privacy-policy/65211544');
        $jsonData = json_decode($response->getBody());

        return $this->render(
            'security/privacy.html.twig',
            [
                'data' => $jsonData
            ]
        );
    }

    /**
         * @route("/active/token/{token}", name="user_active_token")
     * @param $token
     * @param Request $request
     * @return RedirectResponse
     */
    public function activeToken($token, Request $request): RedirectResponse
    {
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['activationToken' => $token]);
        $error = null;
        if (!$user) {
            $error = true;
        }
        if ($error) {
            $session->getFlashBag()->add('errorToken', 'Invalid token.');
            return $this->redirect($this->generateUrl('login'));
        }

        $user->setIsActive(true)
             ->setActivationToken("");
        $em->persist($user);
        $em->flush();

        /* set user for shared hub with non-register user */
        $hubUser = $em->getRepository(HubUser::class)->findOneByEmail($user->getEmail());
        if ($hubUser) {
            $hubUser->setShared($user)
                ->setEmail(null);
            $em->flush($hubUser);
        }

        $loginToken = new UsernamePasswordToken($user, null, "main", $user->getRoles());
        $this->get("security.token_storage")->setToken($loginToken);
        $this->get("session")->set("_security_main", serialize($loginToken));
        $event = new InteractiveLoginEvent($request, $loginToken);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $session->getFlashBag()->add('success', 'Account activated successfully.');
        return $this->redirect($this->generateUrl('welcome_home'));
    }

    /**
     * @route("/checkuser", name="checkuser")
     * @return RedirectResponse
     */
    public function checkUser(Request $request): RedirectResponse
    {
        $session = $request->getSession();

        if ($this->getUser() && $this->getUser()->getIsActive()) {
            $client = $this->get('google.drive.client.manager')->client($this->getUser()->getId());
            if ($client['client']) {
                $response = $this->get('fileListsManager')->getSyncHint($client['client']);
                if ($response['modifyCount'] > 0) {
                    echo "New Content";
                    $message = $response['modifyCount'];
                    $session->getFlashBag()->add('modifyCount', $message);
                }
            }

            $path = $session->get('_security.main.target_path', $this->generateUrl('welcome_home'));
            return $this->redirect($path);
        }

        // set flash message
        $session->getFlashBag()->add(
            'inactiveUser',
            'Before you can login, Please active your account with the activation link sent to your email address.'
        );
        return $this->redirect($this->generateUrl('login'));
    }

       /**
     * @route("/resend_Email", name="resend-Email")
     * @param Request $request
     * @return Response
     */
    public function resendEmail(Request $request): Response
    {
        $user = $this->getUser();
        
        $session = new Session();
        if ($this->getUser() && $this->getUser()->getIsActive()) {
            return $this->redirectToRoute('welcome_home');
        }
        if(!$user) {
            return $this->redirect($this->generateUrl('login'));    
        }
        
        $activationToken = $this->getUser()->getActivationToken();
        $FormuserEmail = $this->getUser()->getEmail();
       
        $this->get('user.manager')
        ->sendWelcomeMail($user, $activationToken);
       
        $session->getFlashBag()->add('success', 'We have sent an activation link again. Please check your mail');
        return $this->redirect($this->generateUrl('login'));
    }

}
