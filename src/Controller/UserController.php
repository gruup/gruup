<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\LinkedUser;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserController extends Controller
{
    /**
     * @Route("/account", name="account")
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     */
    public function account(AuthorizationCheckerInterface $authorizationChecker): Response
    {
        $session = new Session();
        if ($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser()) {
            $session->getFlashBag()->add('error', 'You\'re not logged in. Please login.');
            return $this->redirectToRoute('login');
        }
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $linkedUser = $em->getRepository(LinkedUser::class)->findOneByGoogleLoginUser($user->getId());

        return $this->render('user/account.html.twig', array('linkedUser' => $linkedUser));
    }
}
