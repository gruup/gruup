<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Tile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class GoogleDriveController extends Controller
{
    /**
     * Link to this controller to start the "get_drive" process
     *
     * @Route("/google/drive", name="get_drive", methods={"POST"}, options={"expose"=true})
     * @param \Symfony\Component\HttpFoundation\Request                                    $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @return RedirectResponse
     */
    public function getDriveAction(Request $request)
    {
        $user = $this->getUser();
        $session = new Session();

        if (!$user) {
            return $this->json([
                'status' => 'error',
                'message' => 'You\'re not logged in. Please login.',
                'redirect' => 'login'
            ]);
        }

        try {
            $client = $this->get('google.drive.client.manager')->client($user->getId());

            if (!$client['client']) {
                return $this->json([
                    'status' => 'error',
                    'message' => ' ERROR 1 ' . $client['error'],
                    'redirect' => 'welcome_home'
                ]);
            }
            $payload = json_decode($request->getContent(), true);

            $fileListsManager = $this->get('fileListsManager')
                ->getFileLists($client['client'], $payload);

            if ($fileListsManager['result'] == 'error') {
                return $this->json([
                    'status' => 'error',
                    'message' => ' ERROR 2 ' . $fileListsManager['error'],
                    'redirect' => 'welcome_home'
                ]);
            }
            return $this->json($fileListsManager);
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("GoogleDriveController getDriveAction", $e);
            return $this->json([
                'status' => 'error',
                'message' => $error,
                'redirect' => 'welcome_home'
            ]);
        }
    }

    /**
     * @Route("/google/drive/download/{fileId}", name="download_drive_file")
     *
     * @param $fileId
     * @return RedirectResponse
     */
    public function getDriveFileAction($fileId)
    {
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $tile = $em->getRepository(Tile::class)->findOneByFileId($fileId);
        if (!$tile) {
            return $this->json(['result' => 'error', 'error' => 'We couldn\'t find that Tile.']);
        }
        // if($this->getUser()){
        //     $userId = $this->getUser()->getId();
        // }else{
        $userId = $tile->getUser()->getId();
        // }

        $client = $this->get('google.drive.client.manager')->client($userId);

        if (!$client['client']) {
            //throw or return error
            $session->getFlashBag()->add('error', $client['error']);
            return $this->redirectToRoute('welcome_home');
        }
        $tileFileId = $tile->getRemoteFileId();
        $openDownloadFilesManager = $this->get('openDownloadFilesManager')->accessFiles($tileFileId);

        return $openDownloadFilesManager['results'];
    }
}
