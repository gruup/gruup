<?php

namespace App\Controller;

use App\Entity\HubUser;
use App\Entity\Hub;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Dotenv\Dotenv;

class ApiHubUserController extends Controller
{
    /**
     * @Route("/api/share/hub/{userId}/{hubId}",
     *     name="api_create_share_hub", methods={"GET|POST"}, options={"expose"=true
     * })
     *
     * @param                                                                              $userId
     * @param                                                                              $hubId
     * @param \Symfony\Component\HttpFoundation\Request                                    $request
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function shareHub(
        $userId,
        $hubId,
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker
    ): Response {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false
                    && !$this->getUser()) || ($this->getUser()->getId() != $userId)) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $em = $this->getDoctrine()->getManager();
            $hub = $em->getRepository(Hub::class)->find($hubId);

            if (!$hub) {
                return $this->json(['result' => 'error', 'error' => 'We couldn\'t find that Hub.']);
            }

            $payload = json_decode($request->getContent(), true);
            if (empty($payload["email"])) {
                return $this->json(['result' => 'error', 'error' => 'Please enter an email address.']);
            }
            $emails = explode(',', $payload["email"]);
            $hub->setIsShared(true);
            $em->flush($hub);
            $response = $this->get('hub.manager')->createShareHub($userId, $hub, $emails);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubUserController shareHub", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/shared/hub/{hubId}/{ownerId}",
     *     name="api_shared_hub_user_list", methods={"GET|POST"}, options={"expose"=true
     * })
     *
     * @param                                                                              $hubId
     * @param                                                                              $ownerId
     * @param \Symfony\Component\HttpFoundation\Request                                    $request
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sharedHubUserList($hubId, $ownerId, Request $request, AuthorizationCheckerInterface $authorizationChecker): Response {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false
                    && !$this->getUser()) || ($this->getUser()->getId() != $ownerId)) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $em = $this->getDoctrine()->getManager();
            $sharedHubUsersList = $em->getRepository(HubUser::class)->getSharedHubUserList($hubId, $ownerId);
            if(!$sharedHubUsersList) {
                return $this->json(['result' => 'success', 'data' => array()]);
            }
            $response['result'] = 'success';
            foreach($sharedHubUsersList as $shubul) {
                $email = $shubul->getEmail();
                if(empty($email) && $shubul->getShared()->getId() > 0) {
                    $email = $shubul->getShared()->getEmail();
                }
                $response['data'][] = [
                    "id" => $shubul->getId(),
                    "hubId" => $shubul->getHub()->getId(),
                    "email" => $email,
                ];
            }
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubUserController sharedHubUserList", $e);
            $response = ['result' => 'error', 'message' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/sharedhub/removeuser/{hubUserId}/{hubId}/{ownerId}",
     *     name="api_remove_user_at_shared_hub", methods={"GET|POST"}, options={"expose"=true
     * })
     *
     * @param                                                                              $hubUserId
     * @param                                                                              $hubId
     * @param                                                                              $ownerId
     * @param \Symfony\Component\HttpFoundation\Request                                    $request
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeUserAtSharedHub($hubUserId, $hubId, $ownerId, Request $request, AuthorizationCheckerInterface $authorizationChecker): Response {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false
                    && !$this->getUser()) || ($this->getUser()->getId() != $ownerId)) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $em = $this->getDoctrine()->getManager();
            $removeUser = $em->getRepository(HubUser::class)->removeSharedHubsUser($hubUserId, $hubId, $ownerId);
            if(!$removeUser) {
                return $this->json(['result' => 'error', 'message' => 'Hub user not found.']);
            }

            $hubUserMainId = $removeUser->getId();
            $sendEmailId = $removeUser->getEmail();
            $hubName = $removeUser->getHub()->getName();
            if(empty($sendEmailId) && $removeUser->getShared()->getId() > 0) {
                $sendEmailId = $removeUser->getShared()->getEmail();
            }
            if($hubUserMainId > 0)
            {
                $em->remove($removeUser);
                $em->flush();

                $this->get('hub.manager')->sendMailUnshareHub($sendEmailId, $hubName);
                $response = ['result' => 'success', 'message' => 'User remove from the shared hub.'];
            }
            else
            {
                return $this->json(['result' => 'error', 'message' => 'User not remove successfully.']);
            }

        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubUserController removeUserAtSharedHub", $e);
            $response = ['result' => 'error', 'message' => $error];
        }

        return $this->json($response);
    }
}
