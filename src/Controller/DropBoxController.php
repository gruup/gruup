<?php

namespace App\Controller;

use App\Entity\DropBoxDB;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Description of DropBoxController
 *
 * DropBoxController provides API access to the Dropbox API v2.
 *
 * One a button click the user is directed to the DropBox authourisation page
 * There a token is returned along with other data to allow credentials and
 * confirmation of the user.
 *
 * The user MUIST be signed in to Gruup.io in order to use this connection
 *
 *
 * @author James Crawford
 */
class DropBoxController extends Controller {

    public $token;

    /**
     *
     *
     *
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     *
     * A dynamic page request.  Once user has selected
     * 'Authourise Drop Box' they will be taken to dropbox
     *
     *
     * @Route("/dropBox/auth/", name="dropbox_authentication", options={"expose"=true})
     * @param None
     * @return Response object with HTML template provided
     *
     */
    public function dropBox_authenticate(): Response {



        return $this->render(
                        'connections/dbox/dbox.auth.html.twig',
                        [
                            'title' => "Connect with Dropbox"
                        ]
        );
    }

    /**
     *
     * Once authenticated by dropbox, the user is returned to this page
     * The token and any other assets returned will be stored into the database
     *
     * @Route("/dropBox/response", name="dropbox_response", options={"expose"=true})
     * @var view
     *
     */
    public function dropBox_response(): Response {

        return $this->render(
                        'connections/dbox/dbox.success.html.twig',
                        [
                            'title' => "New Response"
                        ]
        );
    }

    /**
     *
     * Once authenticated by dropbox, the user is returned to this page
     * The token and any other assets returned will be stored into the database
     *
     * @Route("/dropBox/ajax"), name='dropbox_ajaxResponse')
     * @param $request the page object
     * @var view
     *
     */
    public function dropBox_AjaxResponse(Request $request): Response {
        $method = $request->request->get('method');

        switch ($method) {
            case 'newAuth':
                $response = $this->dropBox_newAuth($request);
                break;
            case 'getDirectoryContents':
                $response = $this->dropBox_getDirectoryContents($request);
                break;
            default:
                $response = "No valid method provided";
                break;
        }

        return new Response($response);
    }

    private function dropBox_newAuth($request) {

        $user = $this->getUser();
        $uid = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $tokenData = new DropBoxDB();
        $tokenData->setUser($uid);  # ($uid);
        $tokenData->setToken($request->request->get('access_token')); #('siurvba7b5q85p4cBUA5V3P7C4UAP5V3Q74C'); #;
        $tokenData->setaccount_id($request->request->get('account_id')); #('iuvaeuiaberpiavbpeirvb'); #($request->request->get('uid'));
        $tokenData->setdbUid($request->request->get('account_id')); #('iuvaeuiaberpaethwhw45245qh24h53iavbpeirvb'); #($request->request->get('uid'));
        $tokenData->settoken_type($request->request->get('token_type')); #('bearer'); #($request->request->get('token_type'));
        $now = new \DateTime();
        $tokenData->setCreatedOn($now);
        $response = json_encode($tokenData);
        $entityManager->persist($tokenData);
        $entityManager->flush();
        return ($response);
    }

    /**
     *
     * get the token from the database and use this to get the list of files
     * The token and any other assets returned will be stored into the database
     *
     * @Route("/dropBox/ajax/getFiles"), name='dropbox_getFiles')
     * @param $request the page object
     * @var view
     *
     */
    public function dropBox_getDirectoryContents(Request $request) {

        $user = $this->getUser();
        if (!$user || $user == null) {
            return $this->redirectToRoute('login');
        } else {
            $uid = $user->getId();
        }
        $token = $this->get_dbToken($uid);
        $dropbox = new \Dropbox\Dropbox($token);
        return $this->redirectToRoute('welcome_home');
    }

    /**
     *
     * get the token from the database and use this to get the list of files
     * The token and any other assets returned will be stored into the database
     *
     * @Route("/dropBox/imageTest"), name='dropbox_getImages')
     * @param $request the page object
     * @var view
     *
     */
    public function image_test() {
        $user = $this->getUser();
        if (!$user || $user == null) {
            return $this->redirectToRoute('login');
        }
        $uid = $user->getId();
        $token = $this->get_dbToken($uid);
        $dropbox = new \Dropbox\Dropbox($token);
        $path = "/checkFolder/logo.png";
        $target = "/checkFolder";

        $imgData = $dropbox->files->get_thumbnail($path, $uid . '/');
        ob_start();
        $this->printd($imgData, "image Data");
        $resp = ob_get_clean();

        return $this->render(
                        'connections/dbox/dbox.list.html.twig',
                        [
                            'files' => $resp
                        ]
        );
    }

    /**
     *
     * get the token from the database and use this to get the list of files
     * The token and any other assets returned will be stored into the database
     *
     * @Route("/dropBox/treeRoot"), name='dropbox_treeRoot')
     * @param $request the page object
     * @var view
     *
     */
    public function get_treeRoot() {
        $user = $this->getUser();
        if (!$user || $user == null) {
            return $this->redirectToRoute('login');
        } else {
            $mtArray = array();
            $treeArray = $this->treeRoot_build('', $mtArray);
            return $treeArray;
        }
    }

    public function treeRoot_build($currPath, $treeArray = false) {
        if (!$treeArray) {
            $treeArray = array();
        }
        $user = $this->getUser();
        $uid = $user->getId();
        $token = $this->get_dbToken($uid);
        $dropbox = new \Dropbox\Dropbox($token);
        $files = $dropbox->files->list_folder($currPath);
        if (!empty($files['entries'])) {
            foreach ($files['entries'] as $entry => $file) {
                if ($file['.tag'] == "folder") {
                    $chkPath = $file['name'];
                    $newPath = $currPath . "/" . $file['name']; {
                        $treeArray[] = $newPath;
                        $treeArray = $this->treeRoot_build($newPath, $treeArray);
                    }
                }
            }
        }
        return $treeArray;
    }

    /**
     *
     * get the token from the database and use this to get the list of files
     * The token and any other assets returned will be stored into the database
     *
     * @Route("/dropBox/getTiles"), name='dropBox_getTiles')
     * @param $request the page object
     * @var view
     *
     */
    public function dropBox_getTiles() {
        $user = $this->getUser();
        if (!$user || $user == null) {
            return $this->redirectToRoute('login');
        } else {
            $uid = $user->getId();
            $token = $this->get_dbToken($uid);
            if ($token) {
                $dropbox = new \Dropbox\Dropbox($token);
                $files = $dropbox->files->list_folder('', true);
                foreach ($files['entries'] as $entry => $file) {
                    $fileID = $file['id'];
                    if ($file['.tag'] !== "folder") {
                        $fileID = $file['id'];
                        $extArray = explode(".", $file['name']);
                        $ext = end($extArray);
                        if (in_array($ext, array('gif', 'tif', 'psd'))) {
                            str_replace($ext, "png", strtolower($file['name']));
                            $saveExtension = "png";
                        } else {
                            $saveExtension = strtolower($ext);
                        }
                        $filename['name'] = strtolower($uid . $file['name']);
                        $filename['uid'] = $uid;

                        if (in_array(strtoupper($ext), array('PNG', 'TIF', '.TIF', 'GIF', '.GIF', 'JPG', 'JPEG', 'BMP', '.PNG', '.JPG', '.JPEG', '.BMP'))) {
                            $remotePath = strtolower($file['path_lower']);
                            $thumbnail = $dropbox->files->get_thumbnail($remotePath, $filename);
                            $thumbnailPathUrl = "https://dev.gruup.io/images/" . $uid . $filename['name'];
                            $payload['thumb_img'] = $thumbnailPathUrl;
                            $payload['mimeType'] = 'image/' . $saveExtension;
                            $payload['fileType'] = $ext;
                        } else {
                            $payload['thumb_img'] = null;
                            $payload['mimeType'] = 'application/' . $saveExtension;
                        }
                        $payload['fileType'] = $ext;
                        $payload['source'] = 'dropbox';
                        $payload['name'] = $filename['name'];
                        $payload['color'] = '#0060ff';
                        $payload['remote_file_id'] = $file['id'];
                        $shareLink = $this->send_SharedLinkRequest($file['path_lower'], $token);
                        $previewLink = $shareLink;
                        $payload['url'] = $previewLink;
                        $payload['filePath'] = $payload['url'];
                        $payload['mimeType'] = $ext;
                        $payload['favicon'] = 'https://cfl.dropboxstatic.com/static/images/favicon-vflUeLeeY.ico';
                        $tile = $this->get('tile.manager')->addTile($payload);
                    } else {
//
//                        $payload['fileType'] = 'folder';
//                        $payload['source'] = 'dropbox';
//                        $payload['name'] = $file['name'];
//                        $payload['color'] = '#0060ff';
//                        $payload['remote_file_id'] = $file['id'];
//                        $shareLink = $this->send_SharedLinkRequest($file['path_lower'], $token);
//
//                        $fileUrl = (isset($shareLink['url']) ? $shareLink['url'] : $file['path_lower']);
//                        $payload['url'] = $fileUrl;
//                        $payload['filePath'] = $fileUrl;
//                        $payload['mimeType'] = 'image/png';
//                        $payload['thumb_img'] = 'images/sharedFolder.png';
//                        $payload['mimeType'] = 'image/png';
//                        $payload['favicon'] = 'https://cfl.dropboxstatic.com/static/images/favicon-vflUeLeeY.ico';
//                        //  $tile = $this->get('tile.manager')->addTile($payload);
                    }
                }
                return $this->redirectToRoute('welcome_home');
            } else {
                $resp['error'] = "Token Not recognised, please reauthourise";
                return $this->render(
                                'connections/dbox/dbox.list.html.twig',
                                [
                                    'files' => $resp
                                ]
                );
            }
        }
    }

    public function get_dbToken($uid) {
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->createQuery(
                        'SELECT db.token
        FROM App\Entity\DropBoxDB db
        WHERE db.user_id = :user_id'
                )->setParameter('user_id', $uid);
        $dbData = $query->execute();
        if ($dbData) {
            $token = $dbData[0]['token'];
        } else {
            $token = false;
        }
        return $token;
    }

    public static function printd($var, $title = false, $die = false) {
        echo "\n---------------------------- start/of " . gettype($var);
        if ($title) {
            echo $title;
        }
        echo "---------------------------- \n\r\n";

        if (is_array($var) || is_object($var)) {
            foreach ($var as $key => $value) {

//                if (is_array($value) || is_object($value)) {
//                    $this->printd($var, "Sub-call from" . $title);
//                }
                print_r($key);
                echo " : ";
                print_r($value);
                echo "\n\r\n";
            }
        } else {
            print_r($var);
        }
        echo "---------------------------- end/of " . gettype($var);
        if ($title) {
            echo $title;
        }
        echo "---------------------------- \r\n\r\n\n";
        if ($die) {
            die();
        }
    }

    private function send_SharedLinkRequest($path, $token) {
        $endpoint = "https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings";
        $headers = array(
            "Authorization: Bearer " . $token,
            "Content-Type: application/json",
        );
        $postdata = json_encode(array("path" => $path));
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $endpoint,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => $postdata
        ]);
        $resp = curl_exec($curl);
        curl_close($curl);
        $shareLink = json_decode($resp);
        if (isset($shareLink->error->shared_link_already_exists->metadata->url)) {
            return $shareLink->error->shared_link_already_exists->metadata->url;
        } if (isset($shareLink->url)) {
            return $shareLink->url;
        } else {
            return null;
        }
    }

    /**
     *
     * get the token from the database and use this to get the list of files
     * The token and any other assets returned will be stored into the database
     *
     * @Route("/dropBox/getSharedLinks"), name='dropBox_getSharedLinks')
     * @param $request the page object
     * @var view
     *
     */
    public function dropBox_getSharedLinks() {
        $user = $this->getUser();
        $files = array();
        ob_start();
        if (!$user || $user == null) {
            return $this->redirectToRoute('login');
        } else if ($user) {
            $uid = $user->getId();
            $token = $this->get_dbToken($uid);
            if ($token) {
                $dropbox = new \Dropbox\Dropbox($token);
                $files = $dropbox->files->list_folder_by_limit('', 100, true);
                if ($files['has_more']) {
                    $files = $this->getListsContinue($files, $dropbox, $files['cursor']);
                }
            }
            return $this->redirectToRoute('welcome_home');
        }
    }

    public function getListsContinue($files, $dropbox, $cursor) {


        $newFiles = $dropbox->files->list_folder_continue($cursor);

        array_push($files, $newFiles);

        if ($newFiles['has_more'] == "true") {
            $cursor = $files['cursor'];
            return $this->getListsContinue($files, $dropbox, $cursor);
        }
        return $files;
    }

    /**
     *
     * get the token from the database and use this to get the list of files
     * The token and any other assets returned will be stored into the database
     *
     * @Route("/dropBox/getLimitedLinks"), name='dropBox_getLimitedLinks')
     * @param $request the page object
     * @var view
     *
     */
    public function dropBox_getLimitedLinks() {
        $user = $this->getUser();
        $uid = $user->getId();
        $token = $this->get_dbToken($uid);
        if ($token) {
            $dropbox = new \Dropbox\Dropbox($token);

            $directories = $this->get_treeRoot();
            $files = $dropbox->sharing->list_shared_links('');
        }
        return $this->redirectToRoute('welcome_home');
    }

}
