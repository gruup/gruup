<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use App\Entity\User;
use App\Security\ApiKeyAuthenticator;
use App\Security\ApiKeyUserProvider;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ApiController extends Controller {

    private $encoder;

    public function createToken(Request $request, $username, $password, $providerKey) {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    /**
     * @Route("/api/{token}", name="options_api_user_auth", methods={"OPTIONS"}, requirements={"token"=".+"})
     */
    public function corsHelper(Request $request) {
        $response = new Response();
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
        return $response;
    }

    /**
     * Endpoint for plugin.
     *
     * Plugin posts username and password and will return a json response
     *
     * @Route("/api/user/auth", name="api_user_auth")
     * @Method("POST")
     */
    public function postAuthData(Request $request) {
        $username = $request->request->get('username');
        $password = urldecode($request->request->get('password'));

        $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneByEmail($username);
        if ($password != "" && $username != "") {
            $checkMatch = $this->encoder->isPasswordValid($user, $password);
            if ($checkMatch) {
                $accessToken = $this->build_AccessToken($user);

                $data = ['true' => $accessToken];
            } else {
                $data = ['error' => 'user Not Found'];
            }
        } else {
            if ($password == "" && $username == "") {
                $data = ['error' => 'No password or username sent'];
            } else
            if ($password == "") {
                $data = ['error' => 'No password recieved'];
            } else if ($username == "") {
                $data = ['error' => 'No username recieved'];
            } else {
                $data = ['error' => 'user Not Found'];
            }
        }
        $response = new Response();
        // Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', "*");
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
        $response->headers->set('Vary', 'Origin');
        $response->setContent(json_encode($data));
        return $response;
    }

    /**
     * Endpoint for plugin.
     *
     * Plugin sends as querystring username and password and will return a json response
     *
     * @Route("api/user/auth/{username}/{passHash}", name="api_post_auth")
     * @Method("GET")
     */
    public function loggedInTest($username, $passHash, Request $request) {
        $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneByEmail($username);
        $checkMatch = $this->encoder->isPasswordValid($user, $passHash);

        if ($checkMatch) {
            $accessToken = $this->build_AccessToken($user);
            $user->setApiToken($accessToken);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $newUser = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findOneByToken($accessToken);
            if ($newUser) {
                $data = ['true' => $accessToken];
            } else {
                $data = ['error' => 'Token Not Stored'];
            }
        } else {
            $data = ['error' => 'user Not Found'];
        }
        $response = new Response();
        // Allow all websites
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
        $response->headers->set('Vary', ' Accept-Encoding, Origin');
        $response->setContent(json_encode($data));
        return $response;
    }

    /**
     * Providing a bookmark and an access token allows a user to add a bookmark to their
     *
     * This is the GET (query string) version of the post version
     * gruup portfolio
     *
     * @Route("/api/newBookMark/{bookmark}/{accessToken}", name="api_add_bookmark")
     */
    public function addNewBookmark($bookmark, $accessToken, Request $request) {

        $tokenData = $this->validate_AccessToken(($accessToken));
        if ($tokenData !== null) {
            $userData = explode("-", $tokenData);
            /* login the user */
            $user = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(array('email' => $userData[1]));
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
            if ($user->getId()) {
                $payload['url'] = ( 'https://' . $bookmark);
                $tile = $this->get('tile.manager')->addTile($payload);
                $data = ['ok' => 'BookMark Added', 'tile' => $tile];
            } else {
                $data = ['error' => 'No User Found'];
            }
        } else {
            $data = ['error' => 'Invalid Token'];
        }
        $response = new Response();
        // Allow all websites
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
        $response->headers->set('Vary', ' Accept-Encoding, Origin');
        $response->setContent(json_encode($data));
        return $response;
    }

    /**
     * Post a bookmark with an Access token to the API and the
     * bookmark is added to the users dashboard
     *
     * @Route("/api/postBookMark/", name="api_post_bookmark")
     *
     */
    public function postNewBookmark(Request $request) {
        $bookmark = $request->request->get('bookmark');
        $accessToken = urldecode($request->request->get('accessToken'));
        $tokenData = $this->validate_AccessToken(urldecode($accessToken));
        $userData = explode("-", $tokenData);
        /* login the user */
        $user = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(array('email' => $userData[1]));
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        if ($user->getId()) {
            $payload['url'] = urldecode($bookmark);
            $tile = $this->get('tile.manager')->addTile($payload);
            $data = ['ok' => 'BookMark Added', 'tile' => $tile];
        } else {
            $data = ['error' => 'No User Found'];
        }
        $response = new Response();
        // Allow all websites
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
        $response->headers->set('Vary', ' Accept-Encoding, Origin');
        $response->setContent(json_encode($data));
        return $response;
    }

    /**
     * create an accesstoken hash
     * @param object $user
     * @return string -An access Token
     */
    private function build_AccessToken($user) {
        $uid = $user->getID();
        $uidStr = (string) $uid;
        $primeId = $uidStr[strlen($uidStr) - 1];
        $userName = $user->getUsername();
        $now = time();
        $primes = $this->listPrimes($primeId);
        $token = $uid . "-" . $userName . "-" . $now;

        $cipher_method = 'AES-256-OFB';
        $enc_key = openssl_digest($primes, 'SHA256', TRUE);
        $enc_iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher_method));
        $crypted_token = openssl_encrypt($token, $cipher_method, $enc_key, 0, $enc_iv) . "::" . bin2hex($enc_iv);
        if ($this->strposa($crypted_token, ["+", "/", "\\"]) !== false) {
            return $this->build_AccessToken($user);
        } else {
            $user->setApiToken($crypted_token . $primeId);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $crypted_token . $primeId;
        }
    }

    private function strposa($haystack, $needle, $offset = 0) {
        if (!is_array($needle))
            $needle = array($needle);
        foreach ($needle as $query) {
            if (strpos($haystack, $query, $offset) !== false)
                return true; // stop on first true result
        }
        return false;
    }

    /**
     * Decrypt and validate the token, if it is Not valid return null response
     * @param string $crypted_token
     * @return string
     */
    private function validate_AccessToken($crypted_uidtoken) {
        $primeId = $crypted_uidtoken[strlen($crypted_uidtoken) - 1];
        $crypted_token = substr($crypted_uidtoken, 0, -1);
        $needle = "::";
        $explodeToken = explode("::", $crypted_token);
        if (count($explodeToken) <= 1) {
            return null;
        } else {
            list($crypted_token, $enc_iv) = $explodeToken;
            $cipher_method = 'AES-256-OFB';
            $primes = $this->listPrimes($primeId);
            $enc_key = openssl_digest($primes, 'SHA256', TRUE);
            $token = openssl_decrypt($crypted_token, $cipher_method, $enc_key, 0, hex2bin($enc_iv));
            unset($crypted_token, $cipher_method, $enc_key, $enc_iv);
            return $token;
        }
    }

    /* When building the hash, a random prime number of selected from the list
     * below, this value is taken from the user ID's units digit
     *
     * These are the main hash keys
     * thiese values must NOT be lost or all tokens will be invalidated
     */

    private function listPrimes($primeId) {

        $randPrimes = [
            2074722246773485207821695222107608587480996474721117292752992589912196684750549658310084416732550077,
            2367495770217142995264827948666809233066409497699870112003149352380375124855230068487109373226251983,
            1814159566819970307982681716822107016038920170504391457462563485198126916735167260215619523429714031,
            5371393606024775251256550436773565977406724269152942136415762782810562554131599074907426010737503501,
            6513516734600035718300327211250928237178281758494417357560086828416863929270451437126021949850746381,
            5628290459057877291809182450381238927697314822133923421169378062922140081498734424133112032854812293,
            2908511952812557872434704820397229928450530253990158990550731991011846571635621025786879881561814989,
            2193992993218604310884461864618001945131790925282531768679169054389241527895222169476723691605898517,
            5202642720986189087034837832337828472969800910926501361967872059486045713145450116712488685004691423,
            7212610147295474909544523785043492409969382148186765460082500085393519556525921455588705423020751421];
        return $randPrimes[$primeId];
    }

}
