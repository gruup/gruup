<?php

namespace App\Controller;

use App\Entity\Hub;
use App\Form\HubType;
use App\Repository\HubRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HubController extends Controller
{
    /**
     * @Route("/welcome/hubs/new", name="hub_new", methods="GET|POST", options={"expose"=true})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request): Response
    {
        return $this->render(
            'welcome/welcome.html.twig',
            [
            "PRE_LOADED_STATE" => ""
            ]
        );
    }

    /**
     * @Route("/welcome/hubs/{id}/edit", name="hub_edit", methods="GET|POST")
     * @param Request $request
     * @param Hub     $hub
     * @return Response
     */
    public function edit(Request $request, Hub $hub): Response
    {
        if ($hub->getUser()->getId() !== $this->getUser()->getId()) {
            return $this->redirectToRoute('welcome_home');
        }

        $form = $this->createForm(HubType::class, $hub);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'notice',
                "Your hub has been updated, you can view it <a href='" . $this->generateUrl("view_hub_details", ["hash" => $hub->getHash()])."'>here</a>"
            );
            return $this->redirectToRoute('hub_edit', ['id' => $hub->getId()]);
        }

        return $this->render(
            'hub/edit.html.twig',
            [
            'hub' => $hub,
            'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/welcome/hubs/{id}", name="hub_delete", methods="DELETE")
     * @param Request $request
     * @param Hub     $hub
     * @return Response
     */
    public function delete(Request $request, Hub $hub): Response
    {
        if ($hub->getUser()->getId() !== $this->getUser()->getId()) {
            return $this->redirectToRoute('welcome_home');
        }

        if ($this->isCsrfTokenValid('delete'.$hub->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hub);
            $em->flush();
        }

        return $this->redirectToRoute('welcome_home');
    }
}
