<?php

namespace App\Controller;

use App\Entity\HubUser;
use App\Form\HubUserType;
use App\Repository\HubUserRepository;
use App\Entity\Hub;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/hub/user")
 */
class HubUserController extends Controller
{
    /**
     * @Route("/{hubId}/new", name="hub_user_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $hubId = $request->get('hubId');
        $hub = $em->getRepository(Hub::class)->find($hubId);

        $hubUser = new HubUser();

        $form = $this->createForm(HubUserType::class, $hubUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emails =  explode(',', $form->get('email')->getData());
            if (!empty($emails)) {
                foreach ($emails as $key => $email) {
                    $sharedUser = $em->getRepository(User::class)->findOneByEmail($email);
                    $sharedId = "";
                    if ($sharedUser) {
                        $sharedId = $sharedUser->getId();
                    }
                    $sharedHub = $em->getRepository(HubUser::class)
                        ->getOneBySharedUser(
                            $user->getId(),
                            $hubId,
                            $sharedId,
                            $email
                        );

                    if (!$sharedHub) {
                        if ($sharedUser) {
                            $hubUser = new HubUser();
                            $hubUser->setOwner($user)
                                ->setShared($sharedUser)
                                ->setHub($hub)
                                ->setCreated(new \DateTime())
                                ->setUpdated(new \DateTime());
                            $em->persist($hubUser);
                            $em->flush();
                        } else {
                            $hubUser = new HubUser();
                            $hubUser->setOwner($user)
                                ->setEmail($email)
                                ->setHub($hub)
                                ->setCreated(new \DateTime())
                                ->setUpdated(new \DateTime());
                            $em->persist($hubUser);
                            $em->flush();
                        }
                    }
                }
            }

            return $this->redirectToRoute('view_hub_details', ['hash' => $hub->getHash()]);
        }

        return $this->render(
            'hub_user/new.html.twig',
            [
            'hub_user' => $hubUser,
            'form' => $form->createView(),
            'hubId' => $hubId,
            ]
        );
    }
}
