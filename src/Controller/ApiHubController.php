<?php

namespace App\Controller;

use App\Entity\Hub;
use App\Entity\HubUser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ApiHubController extends Controller
{
    /**
     * @Route("/api/hub/{userId}", name="api_hub_get_all_hubs", methods={"GET"}, options={"expose"=true})
     * @param $userId
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAllHubs($userId, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        $response = [];
        try {
            if (($authorizationChecker->isGranted("ROLE_USER") === false &&
                    !$this->getUser()) || ($this->getUser()->getId() != $userId)) {
                throw new AccessDeniedHttpException('Access Denied');
            }

            /* display all hubs with shared hubs */
            $hubs = $this->getDoctrine()->getRepository(Hub::class)->getHubLists($userId);

            if (!$hubs) {
                return $this->json(['result' => 'success', 'firstHub' => true, 'data' => []]);
            }

            $response['result'] = 'success';
            $firstHub = true;

            foreach ($hubs as $hub) {
                $addToHub = ((int)$hub->getUser()->getId() !== (int)$userId &&
                    $hub->getHubType() === 'shared') ? false : true;

                $response['data'][] = [
                    "id" => $hub->getId(),
                    "hash" => $hub->getHash(),
                    "email" => $hub->getUser()->getEmail(),
                    "created" => $hub->getCreated()->format("H:i:s d-m-Y"),
                    "type" => $this->get('hub.manager')->getHubType($hub),
                    "hubType" => $hub->getHubType(),
                    "name" => $hub->getName(),
                    "addToHub" => $addToHub,
                    "isDelete" => ($hub->getUser()->getId() == $userId) ? true : false
                ];
                if ((int)$hub->getUser()->getId() === (int)$userId) {
                    $firstHub = false;
                }
            }
            $response['firstHub'] = $firstHub;
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubController getAllHubs", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/hub/{userId}/{hash}", name="api_view_hub_details", methods={"GET"}, options={"expose"=true})
     * @param $userId
     * @param $hash
     * @param Request                       $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function getHubDetails(
        $userId,
        $hash,
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker
    ): JsonResponse {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false &&
                !$this->getUser() && $this->getUser()->getId() != $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $response = $this->get('hub.manager')->getHubInfo($userId, $hash, $request);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubController getHubDetails", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/hubs/new", name="api_add_new_hub", methods={"POST"}, options={"expose"=true})
     *
     * @param \Symfony\Component\HttpFoundation\Request                                    $request
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addNewHub(Request $request, AuthorizationCheckerInterface $authorizationChecker): JsonResponse
    {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser()) {
                throw new AccessDeniedHttpException('Access Denied');
            }

            $payload = json_decode($request->getContent(), true);
            $response = $this->get('hub.manager')->addHub($payload);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubController addNewHub", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/hubs/{id}/edit", name="api_edit_hub", methods={"POST"}, options={"expose"=true})
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param Hub $hub
     * @return JsonResponse
     */
    public function editHub(
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker,
        Hub $hub
    ): JsonResponse {
        if (!$this->getUser()) {
            return $this->json(['result' => 'error', 'error' => 'You\'re not logged in. Please login.']);
        }

        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false
                && !$this->getUser()
                || ($hub->getUser()->getId() !== $this->getUser()->getId())
            ) {
                throw new AccessDeniedHttpException('Access Denied');
            }

            $payload = json_decode($request->getContent(), true);
            $response = $this->get('hub.manager')->updateHub($payload, $hub);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubController editHub", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/hubs/{id}/delete", name="api_delete_hub", methods={"GET"}, options={"expose"=true})
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     * @param \App\Entity\Hub                                                              $hub
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteHub(
        AuthorizationCheckerInterface $authorizationChecker,
        Hub $hub
    ): JsonResponse {
        if (!$this->getUser()) {
            return $this->json(['result' => 'error', 'error' => 'You\'re not logged in. Please login.']);
        }

        $response = [];
        $shredEmails = [];
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false
                && !$this->getUser()) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $em = $this->getDoctrine()->getManager();
            $userId = $this->getUser()->getId();
            $hubName = $hub->getName();

            if ((int)$hub->getUser()->getId() === (int)$userId) {
                $sharedHubUsersList = $em->getRepository(HubUser::class)->getSharedHubUserList($hub->getId(), $userId);
                if($sharedHubUsersList) {
                    foreach($sharedHubUsersList as $shubul) {
                        $email = $shubul->getEmail();
                        if(empty($email) && $shubul->getShared()->getId() > 0) {
                            $email = $shubul->getShared()->getEmail();
                        }
                        array_push($shredEmails, $email);
                    }
                    $this->get('hub.manager')->sendMailRemoveHub($shredEmails, $hubName);
                }
                $em->remove($hub);
                $em->flush();
            } else {
                $em->getRepository(HubUser::class)->removeSharedHub($userId, $hub->getId());
            }

            $response['result'] = 'success';
            $response = ['result' => 'success', 'message' => 'Your Hub has been deleted.'];
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubController deleteHub", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }


    /**
     * @Route("/api/hub/tag/{userId}", name="api_add_tag_for_hub", methods={"PATCH"}, options={"expose"=true})
     * @param $userId
     * @param Request                       $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return JsonResponse
     */
    public function addTagForHub(
        $userId,
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker
    ): JsonResponse {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false &&
                !$this->getUser() && $this->getUser() != $userId) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $payload = json_decode($request->getContent(), true);
            $response = $this->get('hub.manager')->addTagsOnHub($payload);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubController addTagForHub", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/hubs/{id}/create-copy", name="api_create_copy_hub", methods={"POST"}, options={"expose"=true})
     * @param                                                                              $id
     * @param \Symfony\Component\HttpFoundation\Request                                    $request
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function createCopyHub(
        $id,
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker
    ): JsonResponse {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser()) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $payload = json_decode($request->getContent(), true);
            $response = $this->get('hub.manager')->hubClone($payload, $id);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubController createCopyHub", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/hub-selected-tag/{hubId}", name="api_hub_selected_tag", methods={"GET"}, options={"expose"=true})
     * @param                                                                              $hubId
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getSelectedTag(
        $hubId,
        AuthorizationCheckerInterface $authorizationChecker
    ): JsonResponse {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false &&
                !$this->getUser()) {
                throw new AccessDeniedHttpException('Access Denied');
            }
            $response = $this->get('hub.manager')->getSelectedTagIds($hubId);
        } catch (AccessDeniedHttpException $e) {
            $response = ['result' => 'error', 'error' => $e->getMessage()];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ApiHubController getSelectedTag", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }
}
