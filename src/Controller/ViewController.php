<?php

namespace App\Controller;

use App\Entity\Hub;
use App\Entity\Tile;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class ViewController extends Controller
{
    /**
     * @Route("/shared-hub/{hash}", name="show_shared_hub", options={"expose"=true})
     * @param $hash
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function viewHubDetails($hash, Request $request)
    {
        $session = new Session();
        try {
            if (!$request->get('email')) {
                throw new AccessDeniedHttpException('Access Denied!, Please login or register.');
            }

            $email = $request->get('email');
            if ($this->getUser()) {
                if ($this->getUser()->getEmail() === $email) {
                    return $this->redirectToRoute('view_hub_details', ['hash' => $hash]);
                }
            }
            $em = $this->getDoctrine()->getManager();
            $registerUser = $em->getRepository(User::class)->findOneByEmail($email);
            if ($registerUser) {
                throw new AccessDeniedHttpException('Please login or register.');
            }

            $hub = $em->getRepository(Hub::class)->findOneBySharedHub($hash, $email);
            if (!$hub) {
                throw new AccessDeniedHttpException('Access Denied!, Please login or register.');
            }

            return $this->render(
                'welcome/welcome.html.twig',
                [
                    'hash' => $hash,
                    'email' => $email,
                    'isSharedHub' => true
                ]
            );
        } catch (AccessDeniedHttpException $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
            return $this->redirectToRoute('login');
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ViewController viewHubDetails", $e);
            $session->getFlashBag()->add('error', $error);
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/api/shared-hub/{hash}/{email}",
     *     name="api_shared_hub", requirements={"hash"}, methods={"GET"}, options={"expose"=true})
     * @param $hash
     * @param $email
     * @param Request $request
     * @return JsonResponse
     */
    public function show($hash, $email, Request $request): JsonResponse
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $hub = $em->getRepository(Hub::class)->findOneBySharedHub($hash, $email);

            if (!$hub) {
                return $this->json(['result' => 'error', 'message' => 'We couldn\'t find that Hub.']);
            }

            $tags = [];
            foreach ($hub->getTags() as $tag) {
                $tags[] = $tag->getName();
            }
            $hubUserId = $hub->getUser()->getId();
            $tilesCount = $em->getRepository(Tile::class)
                ->findByAllTiles($hubUserId, $request, $hub->getId(), null, true);
            $response = [
                "id" => $hub->getId(),
                "hash" => $hub->getHash(),
                "email" => $hub->getUser()->getEmail(),
                "type" => $this->get('hub.manager')->getHubType($hub),
                "hubType" => $hub->getHubType(),
                "isDelete" => false,
                "nonRegister" => true,
                "addTile" => false,
                "name" => $hub->getName(),
                "total" => $tilesCount,
                "tags" => $tags,
                "data" => $this->get('tile.manager')->getTilesByOptions($hubUserId, $request, $hub->getId()),
                "result" => "success"
            ];
        } catch (\Exception $e) {
            $error = $this->get('setting.manager')->getExceptionError("ViewController show", $e);
            $response = ['result' => 'error', 'error' => $error];
        }

        return $this->json($response);
    }
}
