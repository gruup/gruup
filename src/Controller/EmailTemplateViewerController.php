<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EmailTemplateViewerController extends AbstractController
{
    /**
     * @Route("/_email/verification", name="email_template_verification")
     */
    public function verification()
    {
        return $this->render('emails/verification.html.twig', [
            'url' => 'http://localhost:8000/_email/verification/91238784534023492358845734853748954375892742'
        ]);
    }

    /**
     * @Route("/_email/share", name="email_template_viewer_share")
     */
    public function share()
    {
        return $this->render('emails/share.html.twig', [
            'url' => 'http://localhost:8000/_email/verification/91238784534023492358845734853748954375892742',
            'user' => [
                'email' => 'davinder@gruup.space'
            ],
            'sharedLink' => 'http://localhost:8000/_email/verification/91238784534023492358845734853748954375892742'
        ]);
    }
}
