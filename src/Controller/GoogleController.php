<?php

namespace App\Controller;

use App\Entity\LinkedUser;
use App\Entity\Tile;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Dotenv\Dotenv;

class GoogleController extends Controller
{
    /**
     * @Route("/connect/google", name="connect_google", options={"expose"=true})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return RedirectResponse
     */
    public function connectAction(AuthorizationCheckerInterface $authorizationChecker): RedirectResponse
    {
        // will redirect to Google!
        $session = new Session();

        if ($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser()) {
            $session->getFlashBag()->add('error', 'You\'re not logged in. Please login.');
            return $this->redirectToRoute('login');
        }

        $user = $this->getUser();

        $client = $this->get('google_client');
        $auth_url = $client->createAuthUrl();
        return new RedirectResponse($auth_url);
    }

    /**
     * @Route("/connect/google/check", name="connect_google_check")
     * @param Request $request
     * @return RedirectResponse
     */
    public function connectCheckAction(Request $request): RedirectResponse
    {
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        if (!$this->getUser()) {
            $session->getFlashBag()->add('error', 'You\'re not logged in. Please login.');
            return $this->redirectToRoute('login');
        }

        $client = $this->get('google_client');

        if ($request->query->get('code')) {
            $client->fetchAccessTokenWithAuthCode($request->query->get('code'));
            $access_token = $client->getAccessToken();
            $client->setAccessToken($access_token);
            $googleOauthManager = $this->get('google.oauth.manager')->checkOauth();
            if (!$googleOauthManager['IsLoggedIn']) {
                //throw or return error
                $session->getFlashBag()->add('error', $googleOauthManager['error']);
                return $this->redirectToRoute('welcome_home');
            }
        } else {
            $client = $this->get('google.drive.client.manager')->client($this->getUser()->getId());
            if (!$client['client']) {
                //throw or return error
                $session->getFlashBag()->add('error', $client['error']);
                return $this->redirectToRoute('welcome_home');
            }
        }

        $user = $this->getUser();
        $linkedUser = $em->getRepository(LinkedUser::class)
                ->findOneByGoogleLoginUser($user->getId());

        if ($linkedUser) {
            $session->getFlashBag()->add('isConnectGoogle', true);
        }

        return $this->redirectToRoute('welcome_home');
    }

    /**
     * @Route("/connections/google/logout", name="logout_google", options={"expose"=true})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return RedirectResponse
     */
    public function logoutGoogle(AuthorizationCheckerInterface $authorizationChecker): RedirectResponse
    {
        try {
            if ($authorizationChecker->isGranted("ROLE_USER") === false && !$this->getUser()) {
                throw new AccessDeniedHttpException('Access Denied');
            }

            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $session = new Session();

            $linkedUser = $em->getRepository(LinkedUser::class)->findOneByGoogleLoginUser($user->getId());

            if (!$linkedUser) {
                $session->getFlashBag()->add('error', 'No linked google account found.');
                return $this->redirectToRoute('welcome_home');
            }

            $linkedUser->setIsActive(false);
            $linkedUser->setAccessToken(null);
            $linkedUser->setRefreshToken(null);
            $linkedUser->setToken(null);
            $linkedUser->setStartPageToken(null);
            $em->flush();

            $em->getRepository(Tile::class)->deleteAllTilesByUserId($user);

            $session->getFlashBag()->add('success', 'You have successfully logout with google.');
            return $this->redirectToRoute('welcome_home');


        } catch (AccessDeniedException $accessDeniedException) {
            return $this->redirectToRoute('login');

        } catch (\Exception $exception) {
            return $this->redirectToRoute('welcome_home');
        }
    }
}
