<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PagesController extends Controller
{
    /**
     * @Route("/", name="pages_homepage")
     */
    public function index()
    {
        return $this->redirectToRoute('login');
    }
}
