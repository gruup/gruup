<?php

namespace App\Controller;

use App\Entity\Tile;
use App\Form\TileType;
use App\Repository\TileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TileController extends Controller
{
    /**
     * @Route("/welcome/tile/new", name="tile_new", methods="GET|POST", options={"expose"=true})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $tile = new Tile();
        $form = $this->createForm(TileType::class, $tile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tile->setUser($this->getUser())
                ->setFileType("URL")
                ->setSource("URL")
                ->setIsView(false)
                ->setIsDelete(false)
                ->setCreated(new \DateTime())
                ->setUpdated(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($tile);
            $em->flush();

            return $this->redirectToRoute('welcome_home');
        }

        return $this->render(
            'tile/new.html.twig',
            [
            'tile' => $tile,
            'form' => $form->createView(),
            ]
        );
    }
}
