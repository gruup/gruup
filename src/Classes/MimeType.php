<?php

namespace App\Classes;

/**
 * Provides a best-guess mapping of mime type to file extension.
 */
class MimeType
{
    private $defaultMimeType = [
        "application/vnd.google-apps.folder" => "folder",
        "application/vnd.google-apps.document" =>
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.google-apps.spreadsheet" =>
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "application/vnd.google-apps.drawing" => "application/pdf",
        "application/vnd.google-apps.script" => "application/vnd.google-apps.script+json",
    ];

    /**
     * {@inheritdoc}
     */
    public function getMimeType($mimeType)
    {
        return isset($this->defaultMimeType[$mimeType]) ? $this->defaultMimeType[$mimeType] : null;
    }
}
