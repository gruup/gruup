<?php

namespace App\Classes;

/**
 * Provides a best-guess mapping of extension to file extension.
 */
class Extension
{
    protected $defaultFileType = array(
        'application/x-zip' => 'sketch',
        'application/vnd.corel-draw' => 'cdr',
        'application/dot' => 'dot',
        'application/x-dot' => 'dot',
        'application/doc' => 'dot',
        'application/msword-template' => 'dot',
        'application/microsoft_word' => 'dot',
        'application/mswor2c' => 'dot',
        'application/x-msword' => 'dot',
        'zz-application/zz-winassoc-dot' => 'dot',
    );

    /**
     * {@inheritdoc}
     */
    public function getFileExtension($mimeType)
    {
        return isset($this->defaultFileType[$mimeType]) ? $this->defaultFileType[$mimeType] : null;
    }
}
