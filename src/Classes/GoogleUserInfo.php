<?php

namespace App\Classes;

/**
 * Class GoogleUserInfo
 * @package App\Classes
 */
class GoogleUserInfo
{
    protected $googleUser;

    /**
     * GoogleUserInfo constructor.
     * @param $servicePlus
     */
    public function __construct($servicePlus)
    {
        $this->googleUser = $servicePlus->people->get('me');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->googleUser->id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->googleUser->getEmails()[0]->getValue();
    }
}
