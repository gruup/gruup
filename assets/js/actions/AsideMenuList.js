/* global fetch, Routing */

// import { NotificationManager } from 'react-notifications';
import { reset } from 'redux-form';
export const setHubState = payload => ({
type: 'HUB_SET',
        payload,
        });
export const getAllHubs = ({ id }) => async (dispatch) => {
try {
const url = Routing.generate('api_hub_get_all_hubs', { userId: id });
        const response = await fetch(url, {
        method: 'GET',
                credentials: 'same-origin',
        });
        if (response.ok) {
const { data = [], firstHub } = await response.json()||[];
        const hubs = data.map(item => ({ ...item, value: item.hash }));
        return dispatch({
        type: 'HUB_SET',
                payload: { hubs, firstHub },
        });
}
throw new Error('getAllHubs response not ok');
} catch (e) {
console.log(`getAllHubs ${e.message}`);
        return dispatch({
        type: 'HUB_SET',
                payload: {},
        });
}
};
export const getAllTags = ({ id, tagId = null }, params = {}) => async (dispatch) => {
const { isRemoved = false, history = {} } = params;
        try {
        const url = Routing.generate('api_tag_get_all_tags', { userId: id });
                const response = await fetch(url, {
                method: 'GET',
                        credentials: 'same-origin',
                });
                if (response.ok) {
        const { data = [] } = await response.json();
                dispatch({
                type: 'TAG_SET',
                        payload: { tags: data },
                });
                if ((tagId&&isRemoved)||data.length<=0) {
        history.push('/welcome');
        }
        }
        throw new Error('getAllTags response not ok');
        } catch (e) {
console.log(`getAllTags ${e.message}`);
        return dispatch({
        type: 'TAG_SET',
                payload: {},
        });
}
};
export const getHubSelectedTag = hubId => async () => {
try {
const url = Routing.generate('api_hub_selected_tag', { hubId });
        const response = await fetch(url, {
        method: 'GET',
                credentials: 'same-origin',
        });
        if (response.ok) {
const json = await response.json();
        return json;
}
} catch (e) {
console.log(e.message);
}
return {};
        };
export const setHub = payload => ({
type: 'HUB_SET',
        payload,
        });
export const addEditHub = ({ hub, hubType = '', id = null }) => async (dispatch, getState) => {
try {
let {
hub: {
hubs = [],
} = {},
} = getState();
        let url = Routing.generate('api_add_new_hub');
        if (id) {
url = Routing.generate('api_edit_hub', { id });
}

const response = await fetch(url, {
method: 'POST',
        credentials: 'same-origin',
        body: JSON.stringify({ name: hub, hubType }),
});
        if (response.ok) {
const {
result,
        error = '',
        data,
        message,
} = await response.json();
        if (result==='error') {
throw new Error(error);
}

if (message) {
// NotificationManager.success(message, 'Success', 5000);
}

// update hub data
if (id) {
hubs = hubs.map((hubObj) => {
if (hubObj.id===id) {
return data;
}

return hubObj;
});
} else {
hubs = [{ ...data, addToHub: true }, ...hubs];
}

return dispatch(setHub({ hubs, firstHub: false }));
}

let errorMessage = 'Hub not add try again.';
        if (id) {
errorMessage = 'Hub not update try again.';
}

throw new Error(errorMessage);
} catch ({ message }) {
console.log(`addEditHub ${message}`);
        // NotificationManager.warning(message, 'Warning', 5000);
        throw new Error(message);
}
};
export const setTag = payload => ({
type: 'TAG_SET',
        payload,
        });
export const deleteHub = id => async (dispatch, getState) => {
const {
user: {
id: userId,
},
} = getState();
        try {
        const url = Routing.generate('api_delete_hub', { id });
                const response = await fetch(url, {
                method: 'GET',
                        credentials: 'same-origin',
                });
                if (response.ok) {
        const {
        result,
                error = '',
                message,
        } = await response.json();
                if (result==='error') {
        throw new Error(error);
        }
        // NotificationManager.success(message, 'Success', 5000);
                return dispatch(getAllHubs({ id: userId }));
        }

        throw new Error('deleteHub response not ok');
        } catch (e) {
console.log(`deleteHub ${e.message}`);
        // NotificationManager.error(e.message, 'Error', 5000);
        return dispatch(getAllHubs({ id: userId }));
}
};
        /**
         *
         * @param email
         * @param hubId
         * @returns {Function}
         */
export const shareHub = (email, hubId) => async (dispatch, getState) => {
try {
const {
user: {
id: userId,
},
} = getState();
        const url = Routing.generate('api_create_share_hub', { userId, hubId });
        const response = await fetch(url, {
        method: 'POST',
                credentials: 'same-origin',
                body: JSON.stringify(email),
        });
        if (response.ok) {
const {
result,
        error = '',
        message = '',
        warningMessage = '',
} = await response.json();
        if (result==='error') {
throw new Error(error);
}

if (result==='success') {
if (message) {
// NotificationManager.success(message, 'Success', 5000);
}

if (warningMessage) {
// NotificationManager.warning(warningMessage, 'Warning', 5000);
}
}

return dispatch(getAllHubs({ id: userId }));
}
throw new Error('shareHub response is not ok');
} catch (e) {
console.log(`shareHub ${e.message}`);
        // return NotificationManager.warning(e.message, 'Error', 5000);
}
};
export const createCopy = (name, id) => async (dispatch, getState) => {
try {
const {
user: {
id: userId,
},
} = getState();
        const url = Routing.generate('api_create_copy_hub', { id });
        const response = await fetch(url, {
        method: 'POST',
                credentials: 'same-origin',
                body: JSON.stringify(name),
        });
        if (response.ok) {
const {
result,
        error = '',
} = await response.json();
        if (result==='error') {
throw new Error(error);
}
if (result==='success') {
// NotificationManager.success('Hub copied successfully.', 'Success', 3000);
        dispatch(getAllHubs({ id: userId }));
}
}
} catch ({ message }) {
console.log(`createCopy ${message}`);
        // NotificationManager.warning(message, 'Warning', 3000);
        throw new Error(message);
}
};
export const addTag = ({ hubId, tags }) => async (dispatch, getState) => {
try {
const {
user: {
id: userId,
} = {},
} = getState();
        const url = Routing.generate('api_add_tag_for_hub', { userId });
        const response = await fetch(url, {
        method: 'PATCH',
                credentials: 'same-origin',
                body: JSON.stringify({
                hubId,
                        tags,
                }),
        });
        if (response.ok) {
dispatch(reset('AddTag'));
        const {
        result,
                error = '',
                message,
        } = await response.json();
        if (result==='error') {
throw new Error(error);
}
// NotificationManager.success(message, 'Success', 5000);
        if (userId) {
dispatch(getAllTags({ id: userId }));
}
}
} catch (e) {
console.log(`addTag ${e.message}`);
        // NotificationManager.warning(encodeURIComponent(), 'Warning', 5000);
}
};
export const removeUserAtSharedHub = ({ id: hubUserId, hubId }) => async (dispatch, getState) => {
try {
const {
user: {
id: ownerId,
} = {},
        hub: {
        hubUsers: list = [],
        },
} = getState();
        if (!ownerId) {
// NotificationManager.error('Access Denied', 'Error', 5000);
        return false;
}

const url = Routing.generate('api_remove_user_at_shared_hub', { hubUserId, hubId, ownerId });
        dispatch(setHubState({ isHubUserLoading: true }));
        const { result, message = '' } = await fetch(url, {
method: 'GET',
        credentials: 'same-origin',
}).then(res => res.json());
        if (result==='success') {
const hubUsers = list.filter(i => i.id!==hubUserId);
        dispatch(setHubState({ hubUsers }));
        // NotificationManager.success(message, 'Success', 5000);
} else if (message) {
// NotificationManager.error(message, 'Error', 5000);
}

dispatch(setHubState({ isHubUserLoading: false }));
} catch (e) {
// NotificationManager.error('Not remove User from shared hub', 'Error', 5000);
        dispatch(setHubState({ isHubUserLoading: false }));
        console.log(`removeUserAtSharedHub ${e.message}`);
}
};
export const getHubUsers = hubId => async (dispatch, getState) => {
try {
const {
user: {
id: ownerId,
} = {},
} = getState();
        const url = Routing.generate('api_shared_hub_user_list', { hubId, ownerId });
        dispatch(setHubState({ isHubUserLoading: true }));
        const { result, data = [] } = await fetch(url, {
method: 'GET',
        credentials: 'same-origin',
}).then(res => res.json());
        if (result==='success') {
return dispatch(setHubState({ hubUsers: data, isHubUserLoading: false }));
}

dispatch(setHubState({ isHubUserLoading: false }));
} catch (e) {
console.log(`getHubUsers ${e.message}`);
        return dispatch(setHubState({ hubUsers: [], isHubUserLoading: false }));
}
};
