/* global fetch, Routing */

import { setSearch } from './account';

const otherFileType = ['image', 'video', 'spreadsheet', 'document', 'pdf', 'ppt', 'url'];
export const setFileType = (fileType) => {
  if (!fileType) {
    fileType = '';
  }
  const isOtherFile = otherFileType.some(i => i.toLowerCase() == fileType.toLowerCase());

  if (!isOtherFile) {
    return 'other';
  }

  return fileType;
};


export const dataFormat = (data = {}) => {
  const newData = {};
  Object.keys(data).map((key) => {
    const imgType = setFileType(data[key].fileType);

    newData[key] = { ...data[key], fileSize: bytesToSize(data[key].fileSize), imgType };
  });
  return newData;
};

export const bytesToSize = (bytes) => {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0 || bytes == null) return false;
  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return `${(bytes / Math.pow(1024, i)).toFixed(2)} ${sizes[i]}`;
};

export const setTile = (payload = {}) => ({
  type: 'TILE_SET',
  payload,
});

/**
 * Get list type
 *
 * */
export const getListType = () => {
  const pathname = window.location.pathname;
  let type = 'tile';
  if (pathname.includes('welcome/view/tag')) {
    type = 'tag';
  } else if (pathname.includes('welcome/view/hub')) {
    type = 'hub';
  }

  return type;
};

export const getAllTiles = ({
  id, hubId, tagId, isPagination = false,
}) => async (dispatch, getState) => {
  try {
    const {
      tile,
      hub,
      tag,
      account: {
        searchText = null,
      } = {},
    } = getState();


    const listType = getListType();
    let offset = 0;
    let limit = tile.limit;
    let url = '';

    const existingData = tile.data;
    if (listType === 'hub') {
      if (isPagination) {
        offset = hub.offset;
      }
      limit = hub.limit;
      url = Routing.generate('api_view_hub_details', {
        userId: id, hash: hubId, offset, limit, searchText,
      });
    } else if (listType === 'tag') {
      if (isPagination) {
        offset = tag.offset;
      }
      limit = tag.limit;
      url = Routing.generate('api_view_tag_details', {
        userId: id, tagId, offset, limit, searchText,
      });
    } else {
      if (isPagination) {
        offset = tile.offset;
      }

      url = Routing.generate('api_tile_get_all_tiles', {
        userId: id, offset, limit, searchText,
      });
    }

    dispatch(setTile({ isLoading: true, isPagination }));

    const response = await fetch(url, {
      method: 'GET',
      credentials: 'same-origin',
    });

    if (response.ok) {
      const {
        total: totalItem,
        data = {},
        name = '',
        firstTile,
        googleSync,
        addTile = true,
        ...extraData
      } = await response.json();
      const total = totalItem ? Number(totalItem) : 0;

      const formattedData = dataFormat(data);
      let mainData = formattedData;

      if (isPagination) {
        mainData = [
          ...(Object.values(existingData)),
          ...(Object.values(formattedData)),
        ];
      }

      if (listType === 'hub') {
        dispatch({
          type: 'HUB_SET',
          payload: { total, isLoading: false },
        });
        dispatch(setSearch({ isSearchView: false, searchText: '' }));
      } else if (listType === 'tag') {
        dispatch({
          type: 'TAG_SET',
          payload: { total, isLoading: false },
        });
        dispatch(setSearch({ isSearchView: false, searchText: '' }));
      }

      dispatch(setTile({
        total,
        data: { ...mainData },
        name,
        currentTagId: tagId,
        currentHubId: hubId,
        firstTile,
        googleSync,
        addTile,
        ...extraData,
        listType,
      }));
    }
    dispatch(setTile({ isLoading: false, isPagination: false }));
  } catch ({ message }) {
    dispatch(setTile({ isLoading: false, isPagination: false }));
  }
};


export const tileDataOnScroll = () => (dispatch, getState) => {
  let {
    tile: {
      offset, limit, total, isLoading,
      currentHubId: hubId, currentTagId: tagId,
    },
    user: { id },
  } = getState();
  if (!isLoading) {
    offset += limit;

    if (total <= offset) {
      return false;
    }
    dispatch({
      type: 'TILE_SET',
      payload: { offset, isLoading: true },
    });

    dispatch(getAllTiles({
      id, hubId, tagId, isPagination: true,
    }));
  }
};

export const hubDataOnScroll = () => (dispatch, getState) => {
  let {
    hub: {
      offset, limit, total, isLoading,
    },
    tile: { currentHubId: hubId, currentTagId: tagId },
    user: { id },
  } = getState();

  if (!isLoading) {
    offset += limit;

    if (total <= offset) {
      return false;
    }
    dispatch(setTile({ isLoading: true }));
    dispatch({
      type: 'HUB_SET',
      payload: { offset, isLoading: true },
    });

    dispatch(getAllTiles({
      id, hubId, tagId, isPagination: true,
    }));
  }
};

export const tagDataOnScroll = () => (dispatch, getState) => {
  let {
    tag: {
      offset, limit, total, isLoading,
    },
    tile: { currentHubId: hubId, currentTagId: tagId },
    user: { id },
  } = getState();

  if (!isLoading) {
    offset += limit;

    if (total <= offset) {
      return false;
    }
    dispatch(setTile({ isLoading: true }));
    dispatch({
      type: 'TAG_SET',
      payload: { offset },
    });

    dispatch(getAllTiles({
      id, hubId, tagId, isPagination: true,
    }));
  }
};

export const loadMoreOnScroll = () => (dispatch, getState) => {
  const listType = getListType();

  if (listType == 'hub') {
    dispatch(hubDataOnScroll());
  } else if (listType == 'tag') {
    dispatch(tagDataOnScroll());
  } else {
    dispatch(tileDataOnScroll());
  }
};
