// eslint-disable-next-line
export const setUser = (payload = {}) => ({
  type: 'USER_SET',
  payload,
});
