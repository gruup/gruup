
const fetchError = { result: null, message: 'Something wrong try after some time' };

export default () => {
  self.addEventListener('message', ({ data = {} }) => {
    const { url = '', body = {} } = data;

    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      try {
        if (this.status == 200 && this.readyState == 4) {
          this.response = this.response || {};
          const res = JSON.parse(this.response);
          return self.postMessage(res);
        } if (this.status !== 200 && this.readyState == 4) {
          self.postMessage(fetchError);
        }
      } catch (e) {
        self.postMessage(fetchError);
      }
    };
    xhttp.open('POST', url);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify(body));
  });
};
