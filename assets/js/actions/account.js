/* global fetch, Routing */
/* eslint-disable no-console */

import { setTile, setFileType } from './pagination';

export const setSearch = (payload = {}) => ({
  type: 'SEARCH_SET',
  payload,
});

export const getUserDetails = () => async (dispatch, getState) => {
  try {
    const { user: { id: userId } } = getState();
    const url = Routing.generate('api_get_account', { userId });
    dispatch(setSearch({ isLoadingUser: true, isLoadingLog: true }));
    const response = await fetch(url, {
      method: 'GET',
      credentials: 'same-origin',
    });

    const { data = {} } = await response.json() || {};
    dispatch(setSearch({ isLoadingUser: false, isLoadingLog: false }));
    return dispatch(setSearch({ data }));
  } catch (e) {
    console.log(e.message);
    dispatch(setSearch({ isLoadingUser: false, isLoadingLog: false }));
    return dispatch(setSearch());
  }
};

/**
 * Search
 * @param {string} searchText
 * @param {boolean} isSetToTile
 *
 * */
export const searchTile = (searchText = '', isSetToTile = false) => async (dispatch, getState) => {
  try {
    const url = Routing.generate('api_search_tiles');

    dispatch(setSearch({ isLoading: true }));

    const response = await fetch(url, {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify({ searchText }),
    });

    if (response.ok) {
      let {
        data: {
          tileData = [],
          hubData = [],
          tagData = [],
        } = {},
        total = 0,
      } = await response.json() || {};

      hubData = hubData.map((i) => {
        i.fileType = 'hub';
        i.fileExt = 'hub';
        return i;
      });

      tagData = tagData.map((i) => {
        i.fileType = 'tag';
        i.fileExt = 'tag';
        return i;
      });

      const combineData = [...hubData, ...tagData, ...tileData];

      const mainData = {};
      if (combineData) {
        combineData.map((item) => {
          const fileType = setFileType(item.fileType);
          if (!mainData[fileType]) {
            mainData[fileType] = [];
          }

          item = { ...item, fileType: fileType.toUpperCase() };
          mainData[fileType].push(item);
        });
      }
      dispatch(setSearch({ searchData: mainData }));

      // Set fetched tile
      if (isSetToTile) {
        const tileFilterData = tileData.map((item) => {
          const imgType = setFileType(item.fileType) || 'other';
          return { ...item, imgType };
        });
        dispatch(setSearch({ searchText, isSearchView: true }));
        dispatch(setTile({
          data: { ...tileFilterData },
          total: total || tileData.length,
          offset: 0,
        }));
      } else {
        dispatch(setSearch({ searchText: null }));
      }
    }
    dispatch(setSearch({ isLoading: false }));
  } catch (e) {
    dispatch(setSearch({ isLoading: false }));
    throw new Error(e.message);
  }
};

export const getClassName = () => async (dispatch, getState) => {
  const { account: { changeClass } } = getState();

  return dispatch(setSearch({ changeClass: !changeClass }));
};
