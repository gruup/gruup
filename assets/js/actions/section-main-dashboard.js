/* global fetch, Routing */
// import { NotificationManager } from 'react-notifications';
import { reset } from 'redux-form';
import { getAllTags, getAllHubs } from './AsideMenuList';
import {
  loadMoreOnScroll as pagination,
  getAllTiles as paginationGetAllTiles,
  getListType,
  bytesToSize,
  setFileType,
  dataFormat,
} from './pagination';
import {
  resetHubModal,
  resetTagModal,
  resetDeleteTileModal,
  setModals,
  resetRemoveFromHubModal,
} from './modals';
import { searchTile } from './account';

/**
 * Set tile detail modal
 * @param {object} payload
 * */
export const setTile = (payload = {}) => ({
  type: 'TILE_SET',
  payload,
});

/**
 * Set drive data
 * @param {object} payload
 * */
export const setDriveData = (payload = {}) => ({
  type: 'SET_DRIVE_DATA',
  payload,
});

/**
 * Set google notify
 * @param {object} data
 * @param {boolean} isClose
 * */
export const setGoogleNotify = (data = {}, isClose = false) => (dispatch) => {
  if (data.isOpen && isClose) {
    setTimeout(() => {
      dispatch({
        type: 'SET_GOOGLE_NOTIFY',
        payload: { isOpen: false, message: '' },
      });
    }, 5000);
  }

  return dispatch({
    type: 'SET_GOOGLE_NOTIFY',
    payload: { ...data },
  });
};

/**
 * @type {loadMoreOnScroll}
 */
export const loadMoreOnScroll = pagination;

/**
 * @type {getAllTiles}
 */
export const getAllTiles = paginationGetAllTiles;

/**
 * Get current update tile remove tag
 * @param {string|number} tileId
 * @param {string|number} tagId
 * @param {boolean} isRemoveTile
 * @returns {Function}
 */
export const getUpdatedTile = (tileId, tagId = '', isRemoveTile = false) => async (dispatch, getState) => {
  try {
    const {
      tile,
    } = getState();
    if (!tileId) {
      return;
    }
    const listType = getListType();
    let {
      total,
    } = tile;
    const response = await fetch(`/api/tile-details/${tileId}`, {
      method: 'GET',
      credentials: 'same-origin',
    });

    if (response.ok) {
      const { data: updateTile = {} } = await response.json();
      const updatedData = {};
      let isCurrentTag = false;
      if (tagId) {
        isCurrentTag = updateTile.tags.some(tag => tagId === tag.id);
      }

      let key = 0;
      Object.values(tile.data).map((t) => {
        if ((!isCurrentTag && tagId) && (t.tileId === tileId)) {
          return true;
        }

        if (t.tileId === tileId) {
          const imgType = setFileType(updateTile.fileType);
          const fileSize = updateTile.fileSize ? bytesToSize(updateTile.fileSize) : '';
          t = {
            ...updateTile, fileSize, imgType, isFlip: t.isFlip,
          };
        }

        updatedData[key] = { ...t };
        key++;
      });

      if (isRemoveTile && listType === 'tag') {
        total -= 1;
      }

      dispatch(setTile({ data: updatedData, total }));
    }
  } catch (e) {}
};

export const addTileToTag = ({
  id, tileId, tags, tagId,
}, history) => async (dispatch, getState) => {
  const { user: { id: userId } = {}, tile } = getState();
  try {
    const response = await fetch(`/api/tile-tag/${id}`, {
      method: 'PATCH',
      credentials: 'same-origin',
      body: JSON.stringify({
        id,
        tileId,
        tags,
      }),
    });

    if (response.ok) {
      const {
        result, message, error = '', data, isRemoved = false,
      } = await response.json();
      if (result == 'error') {
        // NotificationManager.error(error, 'Error', 5000);
        return false;
      }
      const newTileData = {};

      const filterTags = tags.tiles.map(({ name, id }) => ({ name, id }));
      const isRemoveTile = !filterTags.some(({ id }) => tagId == id) && (tile.currentTagId == tagId);
      Object.keys(tile.data).map((key) => {
        if (tile.data[key].tileId == tileId) {
          tile.data[key].tags = [...filterTags];
        }

        newTileData[key] = { ...tile.data[key] };
      });

      dispatch({
        type: 'TILE_SET',
        payload: { data: newTileData },
      });
      dispatch(getAllTags({ id: userId, tagId: id }, { isRemoved, history }));
      dispatch(setTagsOnTileDetailModal(filterTags));
      dispatch(getUpdatedTile(tileId, tagId, isRemoveTile));
      return dispatch(reset('AddTileToTag'));
    }
  } catch (e) {
    console.log(e.message);
  }
};


export const getSelectedTag = tileId => async (dispatch) => {
  try {
    const response = await fetch(`/api/selected-tag/${tileId}`, {
      method: 'GET',
      credentials: 'same-origin',
    });

    if (response.ok) {
      const json = await response.json();
      return json;
    }
  } catch (e) {
    console.log(e.message);
  }
};

export const getSelectedHub = tileId => async (dispatch, getState) => {
  try {
    const response = await fetch(`/api/selected-hub/${tileId}`, {
      method: 'GET',
      credentials: 'same-origin',
    });

    if (response.ok) {
      const json = await response.json();
      return json;
    }
  } catch (e) {
    console.log(e.message);
  }
};

export const addNewTag = name => (dispatch, getState) => {
  const { tag: { tags: data = [] } = {} } = getState();

  const newTag = { name, value: '' };
  const tags = [...data, newTag];

  dispatch({
    type: 'TAG_SET',
    payload: { tags },
  });
};

export const addTile = ({ tile }) => async (dispatch, getState) => {
  try {
    const { tile: { data: oldData = [], currentHubId, currentTagId } = {} } = getState();
    const data = Object.values(oldData);
    const url = Routing.generate('api_add_new_tile');
    let param = {};

    if (currentTagId) {
      param = { tagId: currentTagId };
    } else if (currentHubId) {
      param = { hash: currentHubId };
    }

    const response = await fetch(url, {
      credentials: 'same-origin',
      method: 'POST',
      body: JSON.stringify({ url: tile, ...param }),
    });

    if (response.ok) {
      const {
        result, message, error = '', data: newData, total,
      } = await response.json();
      if (result == 'error') {
        // NotificationManager.error(error, 'Error', 5000);
        throw new Error(error);
      }
      // NotificationManager.success(message, 'Success', 5000);
      const imgType = setFileType(newData.fileType);
      const fileSize = newData.fileSize ? bytesToSize(newData.fileSize) : '';
      const newTile = { ...newData, imgType, fileSize };
      return dispatch({
        type: 'TILE_SET',
        payload: { total, data: [newTile, ...data] },
      });
    }
  } catch (e) {
    throw new Error(e.message);
  }
};

export const deleteTile = id => async (dispatch, getState) => {
  try {
    const {
      user: { id: userId }, tile: {
        data = {}, currentHubId: hubId, currentTagId: tagId, total,
      },
    } = getState();

    const url = Routing.generate('api_delete_tile', { id });
    dispatch(setModals({ isLoading: true }));
    const response = await fetch(url, {
      method: 'GET',
      credentials: 'same-origin',
    });

    if (response.ok) {
      const {
        result, error = '', message,
      } = await response.json();
      if (result == 'error') {
        // NotificationManager.error(message, 'Error', 5000);
        throw new Error(error);
      } else if (result == 'success') {
        // NotificationManager.success(message, 'Success', 5000);
        const newData = {};
        let key = 0;
        Object.values(data).map((item) => {
          if (id !== item.tileId) {
            newData[key] = item;
            key++;
          }
        });

        dispatch(setTile({
          data: newData,
          total: (total - 1),
        }));
      }
    }
    dispatch(setModals({ isLoading: false }));
    dispatch(resetDeleteTileModal());
  } catch ({ message }) {
    // NotificationManager.error(message, 'Error', 5000);
    dispatch(resetDeleteTileModal());
    dispatch(setModals({ isLoading: false }));
  }
};

export const getSelectedHubLength = tileId => async (dispatch, getState) => {
  try {
    const response = await fetch(`/api/selected-hub/${tileId}`, {
      method: 'GET',
      credentials: 'same-origin',
    });

    if (response.ok) {
      const { data } = await response.json();
      const { tile: { hubCount = {} } = {} } = getState();

      hubCount[tileId] = data;

      return dispatch(setTile({ hubCount }));
    }
  } catch (e) {
    console.log(e.message);
  }
};


/**
 * Set tile detail modal
 * @param {object} params
 * */
export const onSetTileDetailModal = (params = {}) => (dispatch, getState) => {
  let { tile: { tileDetailModal = {} } } = getState();

  tileDetailModal = { ...tileDetailModal, ...params };

  dispatch(setTile({ tileDetailModal }));
};

/**
 * Set update tile tag on tile detail modal
 * @param {object} tags
 * */
export const setTagsOnTileDetailModal = tags => (dispatch, getState) => {
  const { tile: { tileDetailModal = {} } } = getState();

  const tile = { ...tileDetailModal.tile, tags };
  dispatch(onSetTileDetailModal({ tile }));
};

/**
 * Reset tile detail modal
 * @param
 * */
export const onResetTileDetailModal = () => ({
  type: 'RESET_TILE_MODAL',
});


/**
 * Get tile by id
 * @param {string|number} tileId
 * */
export const getTileById = tileId => async (dispatch) => {
  try {
    dispatch(onSetTileDetailModal({ isLoading: true }));

    const url = Routing.generate('api_get_tile_details', { tileId });

    const response = await fetch(url, {
      method: 'GET',
      credentials: 'same-origin',
    });

    if (response.ok) {
      const { data = {} } = await response.json();
      const imgType = setFileType(data.fileType);
      return dispatch(onSetTileDetailModal({
        tile: { ...data, fileSize: bytesToSize(data.fileSize), imgType },
        isLoading: false,
      }));
    }
  } catch (e) {
    dispatch(onSetTileDetailModal({ isLoading: false }));
    console.log(e.message);
  }
};

/**
 * Move field
 * @param {object} arr
 * @param {object} field
 * @param {number} index
 *
 * */
const updateListField = (arr = [], field = {}, index = 0) => {
  arr.splice(index, 1);
  arr = [{ ...field }, ...arr];

  return arr.map((i, key) => {
    let isTabletMode = false;
    if (key == 0) {
      isTabletMode = true;
    }

    const order = key + 1;
    return { ...i, order, isTabletMode };
  });
};

/**
 * Set field/column
 * @param {object} field
 * @param {number} index
 *
 * */
export const setColumn = (field, index) => (dispatch, getState) => {
  let {
    tile: {
      listField = [],
    } = {},
  } = getState();


  listField = updateListField(listField, field, index);
  dispatch(setTile({ listField }));
};

/**
 * Set tile select mode
 * @param {boolean} isReset
 *
 * */
export const toggleSelectMode = (isReset = false) => (dispatch, getState) => {
  let {
    tile: {
      selectedTile = [],
      isSelectMode: isSelect = false,
    } = {},
  } = getState();

  let isSelectMode = !isSelect;

  if (!isSelectMode || (typeof isReset === 'boolean' && isReset)) {
    isSelectMode = false;
    selectedTile = [];
  }

  dispatch(setTile({ isSelectMode, selectedTile }));
};

/**
 * Select tile
 * @param {string|number} id
 *
 * */
// api_add_hub_tag_on_tiles
export const selectTile = (id = null) => (dispatch, getState) => {
  if (!id) {
    return false;
  }

  let {
    tile: {
      selectedTile = [],
    } = {},
  } = getState();

  const isHaveTile = selectedTile.some(tileId => tileId === id);

  if (isHaveTile) {
    selectedTile = selectedTile.filter(tileId => tileId !== id);
  } else {
    selectedTile = [...selectedTile, id];
  }

  dispatch(setTile({ selectedTile }));
};

/**
 * Post selected item
 * @param {object} values
 * @param {object} params
 *
 * */
export const postSelectedTile = (values, params) => async (dispatch, getState) => {
  dispatch(setModals({ isLoading: true }));
  try {
    const {
      tile: {
        selectedTile = [],
      } = {},
      user: { id: userId },
    } = getState();

    const { type, tileId = null, selectedTileId } = params;
    if (type === 'hubIds') {
      values = values.map(i => i.id);
    }

    let body = {
      [type]: values,
      tileIds: selectedTile,
    };

    if (tileId && selectedTile.length <= 0) {
      body = { ...body, tileIds: [tileId] };
    }

    const url = Routing.generate('api_add_hub_tag_on_tiles', { userId });
    const response = await fetch(url, {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(body),
    });

    if (response.ok) {
      const { result = null, message = null } = await response.json();

      if (result == 'success') {
        if (type === 'hubIds') {
          dispatch(getAllHubs({ id: userId }));
          dispatch(getUpdatedTile(selectedTileId));
          dispatch(resetHubModal());
        } else if (type === 'tags') {
          dispatch(getAllTags({ id: userId }));
          dispatch(resetTagModal());
        }
        if (!tileId) {
          dispatch(toggleSelectMode());
        }
        if (message) {
          // NotificationManager.success(message, 'Success', 5000);
        }
      } else if (message) {
        // NotificationManager.error(message, 'Error', 5000);
      }
    }
    dispatch(setModals({ isLoading: false }));
  } catch ({ message }) {
    // NotificationManager.error(message, 'Error', 5000);
    dispatch(setModals({ isLoading: false }));
  }
};

/**
 * Remove selected item/tile
 *
 * */
export const removeSelectedTile = () => async (dispatch, getState) => {
  try {
    const {
      tile: {
        selectedTile = [],
        currentHubId,
        currentTagId,
      } = {},
      user: { id: userId },
    } = getState();

    const url = Routing.generate('api_multi_delete_tiles', { userId });

    dispatch(setModals({ isLoading: true }));
    const response = await fetch(url, {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify({ tileIds: selectedTile }),
    });

    if (response.ok) {
      const { result = null, message = '' } = await response.json();

      if (result == 'success') {
        const listType = getListType();
        let hubId = null;
        let tagId = null;
        if (listType === 'hub') {
          dispatch(getAllHubs({ id: userId }));
          hubId = currentHubId;
        } else if (listType === 'tag') {
          dispatch(getAllTags({ id: userId }));
          tagId = currentTagId;
        }

        dispatch(getAllTiles({ id: userId, hubId, tagId }));
        dispatch(toggleSelectMode());

        if (message) {
          // NotificationManager.success(message, 'Success', 5000);
        }
      } else if (message) {
        // NotificationManager.error(message, 'Error', 5000);
      }
    }
    dispatch(setModals({ isLoading: false }));
    dispatch(resetDeleteTileModal());
  } catch ({ message }) {
    // NotificationManager.error(message, 'Error', 5000);
    dispatch(setModals({ isLoading: false }));
    dispatch(resetDeleteTileModal());
  }
};

/**
 * @param hubId
 * @returns {{
 *    tileId: string, path: string, created: string, name: string,
 *    thumbImg: string, source: string, filePath: string, fileSize: null,
 *    fileType: string, fileExt: null, mimeType: null, description: string,
 *    favicon: string, remoteThumbUrl: null, owner: string, userId: number,
 *    remoteFileId: null, isDelete: boolean, tags: Array, hubs: *[]
 *    }}
 */
const getBlogTile = ({ hubId }) => ({
  tileId: 'blog',
  path: 'https://www.gruup.space/blog/2018/10/8/gruup-five-top-tips',
  created: '11/11/2018',
  name: 'Gruup Five Top Tips',
  thumbImg: 'http://static1.squarespace.com/static/5b928f6ea2772cf6ca598d0c/5bbc84c04785d3ab538d2746/5bc9e763b208fcae320b0603/1540814118595/check-class-desk-7103.jpg?format=1000w',
  source: 'gruup',
  filePath: 'http://static1.squarespace.com/static/5b928f6ea2772cf6ca598d0c/5bbc84c04785d3ab538d2746/5bc9e763b208fcae320b0603/1540814118595/check-class-desk-7103.jpg?format=1000w',
  fileSize: null,
  fileType: 'URL',
  fileExt: null,
  mimeType: null,
  description: '\u201cThere\u2019s more than one way to skin a cat\u201d, they say. Less messily, there\u2019s more than one way to take advantage of the functionality of Gruup, to help you and your teams work smarter, work better and work with more, well, teamwork.',
  favicon: 'https://static1.squarespace.com/static/5b928f6ea2772cf6ca598d0c/t/5b9291fe352f53daeea21803/favicon.ico',
  remoteThumbUrl: null,
  owner: 'hello@gruup.space',
  userId: 0,
  remoteFileId: null,
  isDelete: false,
  tags: [],
  hubs: [hubId],
});

/**
 * @param hubId
 * @returns {{
 *    tileId: string, path: string, created: string, name: string,
 *    thumbImg: string, source: string, filePath: string, fileSize: null,
 *    fileType: string, fileExt: null, mimeType: null, description: string,
 *    favicon: string, remoteThumbUrl: null, owner: string, userId: number,
 *    remoteFileId: null, isDelete: boolean, tags: Array, hubs: *[]
 *    }}
 */
const getVimeoTile = ({ hubId }) => ({
  tileId: 'vimeo',
  path: 'https://vimeo.com/312513652/745542fbca',
  created: '11/11/2018',
  name: 'Get Started',
  thumbImg: 'https://i.vimeocdn.com/video/753584429_640.jpg',
  source: 'URL',
  filePath: 'https://i.vimeocdn.com/video/753584429_640.jpg',
  fileSize: null,
  fileType: 'video',
  fileExt: null,
  mimeType: null,
  description: 'Get Started',
  favicon: 'https://i.vimeocdn.com/favicon/main-touch_180',
  remoteThumbUrl: null,
  owner: 'hello@gruup.space',
  userId: 0,
  remoteFileId: null,
  isDelete: false,
  tags: [],
  hubs: [hubId]
});

/**
 * Get hub by hash
 * @param {string} hash
 *
 * */
export const getHubByHash = (hash, email) => async (dispatch, getState) => {
  try {
    const { tile } = getState();
    const url = Routing.generate('api_shared_hub', { hash, email });
    const response = await fetch(url, {
      method: 'GET',
      credentials: 'same-origin',
    });

    if (response.ok) {
      const data = await response.json();
      const {
        data: tileData,
        result,
        message,
        ...hubDetail
      } = data;

      if (result === 'success') {
        const hubs = [{ ...hubDetail }];
        const blogTile = getBlogTile({ hubId: hubDetail.id });
        const vimeoTile = getVimeoTile({ hubId: hubDetail.id });
        const tileDataWithBlog = [blogTile, vimeoTile, ...tileData];
        const newData = { ...data, data: dataFormat({ ...tileDataWithBlog }) };
        dispatch(setTile({ ...newData }));
        dispatch({
          type: 'HUB_SET',
          payload: { hubs },
        });
      } else if (message) {
        // NotificationManager.error(message, 'Error', 5000);
      }
    }
  } catch ({ message }) {
    if (message) {
      // NotificationManager.error(message, 'Error', 5000);
    }
  }
};

/**
 * Get param by name
 * @param {string} name
 * @param {string} queryParam
 *
 * */
export const getParameterByName = (name = '', queryParam) => (dispatch) => {
  name = name.replace(/[\[\]]/g, '\\$&');
  const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`);


  const results = regex.exec(queryParam);
  if (!results) return null;
  if (!results[2]) return null;
  return decodeURIComponent(results[2].replace(/\+/g, ' ')) || null;
};


/**
 * Get query param
 * @param {string} search
 *
 * */
export const getQueryParam = (search = '') => (dispatch) => {
  if (search) {
    dispatch(searchTile(search, true));
  }
};

/**
 * Remove from hub
 * @param {object} tileIds
 * @param {number} hubId
 * @param {boolean} isMulti
 *
 * */
export const removeTileFromHub = (tileIds = [], hubId, isMulti = false) => async (dispatch, getState) => {
  if (!hubId || tileIds.length == 0) {
    return false;
  }

  try {
    let { tile: { data, total = 0 }, user } = getState();
    const url = Routing.generate('api_remove_hub_on_tiles', { userId: user.id });

    dispatch(setModals({ isLoading: true }));
    const response = await fetch(url, {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify({ hubId, tileIds }),
    });

    if (response.ok) {
      const { result, message = '' } = await response.json();
      if (result == 'success') {
        const hubs = {};
        let key = 0;

        Object.values(data).map((item) => {
          if (!tileIds.includes(item.tileId)) {
            hubs[key] = item;
            key++;
          }
        });

        if (total) {
          total -= tileIds.length;
        }

        dispatch(setTile({
          data: hubs,
          total,
        }));
        dispatch(resetRemoveFromHubModal());

        if (isMulti) {
          dispatch(setTile({
            isSelectMode: false,
            selectedTile: [],
          }));
        }
        // NotificationManager.success(message, 'Success', 5000);
      } else if (message) {
        // NotificationManager.error(message, 'Error', 5000);
      }
    }
    dispatch(setModals({ isLoading: false }));
  } catch ({ message }) {
    if (message) {
      // NotificationManager.error(message, 'Error', 5000);
    }
    dispatch(setModals({ isLoading: false }));
  }
};


/**
 * Set hub dropdown
 * @param {object} payload
 * */
export const setHubDropdown = (payload = {}) => ({
  type: 'SET_HUB_DROPDOWN',
  payload,
});

/**
 * Toggle flip tile
 * @param {string|number} tileId
 * */
export const toggleFlipTile = tileId => (dispatch, getState) => {
  const { tile: { data = {} } } = getState();
  const filterData = {};
  let key = 0;
  if (!tileId) {
    return false;
  }

  Object.values(data).map((item) => {
    if (tileId === item.tileId) {
      item.isFlip = !item.isFlip;
    }


    filterData[key] = item;
    key++;
  });

  dispatch(setTile({ data: filterData }));
};

/**
 * Reset offset on view change
 *
 * */
export const resetOffset = () => (dispatch) => {
  dispatch(setTile({ offset: 0 }));
  dispatch({
    type: 'HUB_SET',
    payload: { offset: 0 },
  });
  dispatch({
    type: 'TAG_SET',
    payload: { offset: 0 },
  });
};

export const setActiveTab = activeTabName => setTile({ activeTabName });
