
/**
 * Set modals data
 * @param {object} payload
 *
 * */
export const setModals = (payload = {}) => ({
  type: 'SET_MODALS',
  payload,
});

/**
 * Set hub modal
 * @param {object} payload
 *
 * */
export const setHubModal = (payload = {}) => ({
  type: 'SET_HUB_MODAL',
  payload,
});

/**
 * Reset hub modal
 *
 * */
export const resetHubModal = () => ({
  type: 'RESET_HUB_MODAL',
});

/**
 * Set tag modal
 * @param {object} payload
 *
 * */
export const setTagModal = (payload = {}) => ({
  type: 'SET_TAG_MODAL',
  payload,
});

/**
 * Reset tah modal
 *
 * */
export const resetTagModal = () => ({
  type: 'RESET_TAG_MODAL',
});

/**
 * Set delete tile modal
 * @param {object} payload
 *
 * */
export const setDeleteTileModal = (payload = {}) => ({
  type: 'SET_DELETE_TILE_MODAL',
  payload,
});

/**
 * Reset delete tile modal
 *
 * */
export const resetDeleteTileModal = () => ({
  type: 'RESET_DELETE_TILE_MODAL',
});


/**
 * Set remove from hub modal
 * @param {object} payload
 *
 * */
export const setRemoveFromHubModal = (payload = {}) => ({
  type: 'SET_REMOVE_FROM_HUB_MODAL',
  payload,
});

/**
 * Reset remove from hub tile modal
 *
 * */
export const resetRemoveFromHubModal = () => ({
  type: 'RESET_REMOVE_FROM_HUB_MODAL',
});
