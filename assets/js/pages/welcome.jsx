/* global window, document */
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
// import { NotificationContainer } from 'react-notifications';
import { Provider, connect } from 'react-redux';

import initStore from '../initStore';
import AsideMenuList from '../containers/AsideMenuList';
import SectionMainDashboard from '../containers/SectionMainDashboard';
import ErrorBoundary from '../components/ErrorBoundary';
import Header from '../containers/account';

const initialState = window.PRE_LOADED_STATE;
const store = initStore(initialState);

// let Root = ({ tile = {} }) => {
//   const { isOpenSidebar } = tile;

//   return (
//     <div id="section-main-dashboard" className={`${isOpenSidebar ? 'full-width flex__grid' : 'flex__grid'}`}>
//         <Route exact path="/welcome" component={SectionMainDashboard} />
//         <Route exact path="/welcome/view/hub/:hubId" component={SectionMainDashboard} />
//         <Route exact path="/welcome/view/tag/:tagId" component={SectionMainDashboard} />
//         <Route path="/shared-hub/:hash" component={SectionMainDashboard} />
        // <NotificationContainer />
//       </div>
//     );
//   };
  
//   Root = connect(state => state)(Root);

let App = ({ tile }) => {
  const closeSidebar = tile.isOpenSidebar ? 'sidebar-is-close' : '';
  const isShowMobileSidebar = tile.isShowMobileSidebar ? 'open__menu_sidebar' : ''
  
  return (
    <Fragment>
      <div className={`${closeSidebar} flex__grid sidebar ${isShowMobileSidebar}`}>
        <Route path="/welcome" component={AsideMenuList} />
        <Route path="/shared-hub/:hash" component={AsideMenuList} />
      </div>
      <div className="flex__grid">
        <Route path="/welcome" component={Header} />
        <Route path="/shared-hub/:hash" component={Header} />
        <Switch>
          <div id="section-main-dashboard" className={`${tile.isOpenSidebar ? 'full-width flex__grid' : 'flex__grid'}`}>
            <Route exact path="/welcome" component={SectionMainDashboard} />
            <Route exact path="/welcome/view/hub/:hubId" component={SectionMainDashboard} />
            <Route exact path="/welcome/view/tag/:tagId" component={SectionMainDashboard} />
            <Route path="/shared-hub/:hash" component={SectionMainDashboard} />
            {/* <NotificationContainer /> */}
          </div>
        </Switch>
      </div>
    </Fragment>
  );
}

App = connect(state => state)(App);
  
  ReactDOM.render(
    <Provider store={store}>
    <ErrorBoundary>
      <Router>
        <App />
      </Router>
    </ErrorBoundary>
  </Provider>,
  document.getElementById('root-section'),
);

if (module.hot) {
  module.hot.accept();
}
