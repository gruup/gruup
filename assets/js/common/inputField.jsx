import React, { Component } from 'react';

class InputField extends Component {
  render() {
    const {
      className,
      input,
      label,
      type,
      placeholder,
      meta: {
        touched,
        error,
        warning,
      },
      isCheckUrl = false,
      serverError = false,
    } = this.props;
    return (
      <div>
        <label>
          {label}
        </label>
        <div>
          <input
            {...input}
            placeholder={placeholder}
            type={type}
            className={className}
          />

          {
            isCheckUrl
            && touched && (error || serverError ? <img src="/images/cross.png" className="success-tick" />
              : <img src="/images/success.png" className="success-tick" />
            )
          }

          {
            !isCheckUrl
            && touched && (
              (error && (
              <span className="error-display">
                {error}
              </span>
              ))
              || (warning && (
              <span>
                {warning}
              </span>
              ))
            )
          }
        </div>
      </div>
    );
  }
}

export default InputField;
