import React, { Component, Fragment } from 'react';

const {
  isSharedHub
} = __global__;

const validType = [ 'image', 'video' ];

const Image = ({ tile, ...rest }) => {

	if (tile.thumbImg) {
		return (
			<img src={`/thumbImg/${tile.thumbImg}`} {...rest}/>
		)
	}

	return (
		<img src={Routing.generate("api_save_thumb_image", { id: tile.tileId })} {...rest} />
	)
}

class TableDetailModal extends Component {

	constructor(props) {
		super(props);

		const { getTileById, tileId } = props;
		getTileById(tileId);
	}

	state = {
		isShowPopup: false
	}

	handleClose = () => {
		const { onResetTileDetailModal } = this.props;
		onResetTileDetailModal();
	}

	OnClickAddTileToTag = (e) => {
		e.stopPropagation();

		if(isSharedHub == 1) {
			return true;
		}

		const { handleOnClickAddTileToTag, tile } = this.props;
		handleOnClickAddTileToTag(tile.tileId);
	}

    render() {
		const { isShowPopup } = this.state;
		const { isOpen = false, tile = {}, tileId } = this.props;
		const { hubs = [] } = tile;
		const loadImage = tile.tileId && validType.includes(tile.fileType) ? <Image tile={tile} className="image-tiles" />: '';

		return (
				<Fragment>
				<div className='modal is-active modal-display tile-info'>
					<div className="modal-background" />
					<div className="modal-card">
						<header className="modal-card-head">
							<p className="modal-card-title">Tile details</p>
							<button className="delete" type="button" aria-label="close" onClick={this.handleClose}/>
						</header>
						<section className="modal-card-body">
							<div className="search-modal">
								<div className="tiles-image">
								{
									tile.source == 'URL' ?
									(tile.filePath) && <img src={tile.filePath} alt="" className="image-tiles" />
									: loadImage
								}
								</div>
								<ul>
									<li>Name: {tile.name}</li>
									{
										tile.source == 'URL' &&
										<li>URL:  <a href={tile.path} target="_blank"> {tile.path} </a></li>
									}
									<li>Added on: {tile.created}</li>
									<li>Author: {tile.owner}</li>
									{
										tile.fileSize &&
										<li>Size: {tile.fileSize}</li>
									}
									<li>Saved in:
									<span
										className="hubs-count"
									>
										{" "} {hubs.length} {" "}{hubs.length <= 1 ? "hub" : "hubs"}
									</span>
									</li>
									<li>
										<span className="tag-flip"><i className="fa fa-tag" /></span>
										<span className="tag-list-data">
											{
												tile.tags &&
												tile.tags.map(({name}, i) =>
													<span key={i}>{name}&nbsp;{i < (tile.tags.length-1) && "|"}&nbsp;</span>
												)
											}
										</span>
										{
											isSharedHub != 1 &&
											<span className="plus" onClick={this.OnClickAddTileToTag}><i className="fa fa-plus" /></span>
										}
									</li>
								</ul>
							</div>
						</section>
					</div>
				</div>
	        </Fragment>
		);
	}
}

class Wrapper extends Component {

	render() {

		const { isOpen = false, tileId } = this.props;

		if(!isOpen || !tileId) {
			return null;
		}


		return <TableDetailModal {...this.props} />
	}
}

export default Wrapper;
