/**
 *
 * @param {{}} state - Current State
 * @param {string} type - Type of action
 * @param {*} payload - Payload
 * @return {{}} - return state
 */

const tileDetailModal = {
  isOpen: false,
  tileId: null,
  tile: {},
  isLoading: false,
};

const listField = [
  {
    name: 'Author', mappedKey: 'owner', order: 1, isTabletMode: true,
  },
  { name: 'File upload date', mappedKey: 'created', order: 2 },
  { name: 'Size', mappedKey: 'fileSize', order: 3 },
  {
    name: 'Download', mappedKey: 'filePath', order: 4, isLink: true, className: 'table-action',
  },
];

const driveData = {
  limit: 100,
  offset: 0,
  pageToken: '',
};

const gNotify = {
  isOpen: false,
  message: 'This is text message.',
};

const hubDropdown = {
  isOpen: false,
  item: {},
};

const initialState = {
  data: {},
  total: 0,
  offset: 0,
  limit: 50,
  isLoading: false,
  isPagination: false,
  hubCount: {},
  tileDetailModal,
  listField,
  isOpenSidebar: false,
  firstTile: false,
  googleSync: false,
  isSelectMode: false,
  selectedTile: [],
  addTile: true,
  driveData,
  gNotify,
  isStartSync: false,
  isChangeView: false,
  tileLoaded: false,
  activeTabName: 'home',
};


const tile = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'INIT':
    case 'TILE_SET':
      return { ...state, ...payload };

    case 'RESET_TILE_MODAL':
      return { ...state, tileDetailModal };

    case 'SET_DRIVE_DATA':
      return {
        ...state,
        driveData: {
          ...state.driveData,
          ...payload,
        },
      };

    case 'SET_GOOGLE_NOTIFY':
      return {
        ...state,
        gNotify: {
          ...state.gNotify,
          ...payload,
        },
      };

    case 'SET_HUB_DROPDOWN':
      return {
        ...state,
        hubDropdown: {
          ...state.hubDropdown,
          ...payload,
        },
      };

    default:
      return state;
  }
};

export default tile;
