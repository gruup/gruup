/**
 *
 * @param {{}} state - Current State
 * @param {string} type - Type of action
 * @param {*} payload - Payload
 * @return {{}} - return state
 */

const initialize = {
  hubs: [],
  hubId: '',
  total: 0,
  offset: 0,
  limit: 50,
  isLoading: false,
  firstHub: false,
  hubUsers: [],
  isHubUserLoading: false,
};

const hub = (state = initialize, { type, payload }) => {
  switch (type) {
    case 'HUB_INIT':
    case 'HUB_SET':
      return {
        ...state,
        ...payload,
      };

    default:
      return state;
  }
};

export default hub;
