const app = (state = {}, { type, payload }) => {
  switch (type) {
    case 'APP_SET':
      return { ...state, ...payload };
    default:
      return state;
  }
};

export default app;
