/**
 *
 * @param {{}} state - Current State
 * @param {string} type - Type of action
 * @param {*} payload - Payload
 * @return {{}} - return state
 */

const {
  modifyCount = 0,
} = __global__;

const initialize = {
  data: {},
  searchData: [],
  changeClass: false,
  isLoading: false,
  isLoadingUser: false,
  isSearchView: false,
  searchText: '',
  modifyCount,
};

const account = (state = initialize, { type, payload }) => {
  switch (type) {
    case 'SEARCH_SET':
      return { ...state, ...payload };
    default:
      return state;
  }
};

export default account;
