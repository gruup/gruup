const hubModalData = {
  isOpen: false,
  tileId: null,
  onSubmit: val => val,
  isMultiSelect: false,
  params: {},
};

const tagModalData = {
  isOpen: false,
  tileId: null,
  onSubmit: val => val,
  isMultiSelect: false,
  params: {},
};

const deleteTileModalData = {
  tile: {},
  onDeleteTile: val => val,
  params: {},
  isMulti: false,
  isOpen: false,
};

const removeFromHubModalData = {
  tile: {},
  onRemove: val => val,
  params: {},
  isOpen: false,
  isMulti: false,
};

const initialState = {
  hubModalData,
  tagModalData,
  deleteTileModalData,
  removeFromHubModalData,
  isLoading: false,
};

const modals = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'SET_MODALS':
      return { ...state, ...payload };

    case 'SET_HUB_MODAL':
      return {
        ...state,
        hubModalData: { ...state.hubModalData, ...payload },
      };

    case 'RESET_HUB_MODAL':
      return { ...state, hubModalData };

    case 'SET_TAG_MODAL':
      return {
        ...state,
        tagModalData: { ...state.tagModalData, ...payload },
      };

    case 'RESET_TAG_MODAL':
      return { ...state, tagModalData };

    case 'SET_DELETE_TILE_MODAL':
      return {
        ...state,
        deleteTileModalData: { ...state.deleteTileModalData, ...payload },
      };

    case 'RESET_DELETE_TILE_MODAL':
      return { ...state, deleteTileModalData };

    case 'SET_REMOVE_FROM_HUB_MODAL':
      return {
        ...state,
        removeFromHubModalData: { ...state.removeFromHubModalData, ...payload },
      };

    case 'RESET_REMOVE_FROM_HUB_MODAL':
      return { ...state, removeFromHubModalData };


    default:
      return state;
  }
};

export default modals;
