const initialState = {
  email: '',
  id: '',
};

/**
 *
 * @param {{}} state - Current State
 * @param {string} type - Type of action
 * @param {*} payload - Payload
 * @return {{}} - return state
 */
const user = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'USER_INIT':
      return {
        ...state,
        ...payload,
      };
    case 'USER_SET': {
      return {
        ...state,
        ...payload,
      };
    }
    default:
      return state;
  }
};

export default user;
