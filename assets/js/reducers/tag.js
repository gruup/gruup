/**
 *
 * @param {{}} state - Current State
 * @param {string} type - Type of action
 * @param {*} payload - Payload
 * @return {{}} - return state
 */

const initialize = {
  tags: [],
  total: 0,
  offset: 0,
  limit: 15,
  isLoading: false,
};

const tag = (state = initialize, { type, payload }) => {
  switch (type) {
    case 'TAG_INIT':
      return {
        ...state,
        ...payload,
      };
    case 'TAG_SET': {
      return {
        ...state,
        ...payload,
      };
    }
    default:
      return state;
  }
};

export default tag;
