import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

import user from './user';
import tile from './tile';
import hub from './hub';
import tag from './tag';
import account from './account';
import modals from './modals';
import app from './app';

export default combineReducers({
  form,
  user,
  tile,
  hub,
  tag,
  account,
  modals,
  app,
});
