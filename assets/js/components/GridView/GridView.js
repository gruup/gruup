import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DisplayTiles from './DisplayTiles';

class GridView extends Component {
  render() {
    const {
      tile,
      tile: {
        isSelectMode,
        selectedTile = [],
        addTile = false,
      },
      handleOnClickAddTileToHub,
      handleOnClickAddTileToTag,
      getListType,
      onSetRemoveFromHubModal,
      onRemoveTileFromHub,
      onSetDeleteTileModal,
      onSelectedHubLength,
      toggleFlipTile,
      onSelectTile,
      onDeleteTile,
      handleAddTileClick
    } = this.props;

    return (
      <div className="grid-layout">
        { tile.firstTile && (
          <div className="first-tile" onClick={handleAddTileClick}>
            <div className="first-tile-txt">
              Click
              {' '}
              <span className="txt-bold">
                Add Tile
              </span>
              {' '}
                to create your first Tile
            </div>
          </div>)
        }
        { tile.googleSync && (
            <div className="first-tile">
              <a href={Routing.generate('connect_google')}>
                <div className="drive-tile-txt">
                  <div className="sync-drive-img">
                    <img src="/images/drive.png" className="google-drive-logo" />
                  </div>
                  <div className="sync-drive">
                    <span className="txt-bold">
                      Sync
                    </span>
                    {' '}
                      with Google Drive
                  </div>
                </div>
              </a>
            </div>
            )
        }
        {
          Object.values(tile.data).map((t, key) => {
            const selectTileId = t.tileId || t.hash || t.id;
            const isSelectedTile = selectedTile.some(i => i === selectTileId);
            return (
              <DisplayTiles
                tile={t}
                key={`${key}-${t.tileId}`}
                isSelectMode={isSelectMode}
                isSelectedTile={isSelectedTile}
                addTile={addTile}
                handleOnClickAddTileToHub={handleOnClickAddTileToHub}
                handleOnClickAddTileToTag={handleOnClickAddTileToTag}
                getListType={getListType}
                onSetRemoveFromHubModal={onSetRemoveFromHubModal}
                onRemoveTileFromHub={onRemoveTileFromHub}
                onSetDeleteTileModal={onSetDeleteTileModal}
                onSelectedHubLength={onSelectedHubLength}
                toggleFlipTile={toggleFlipTile}
                onSelectTile={onSelectTile}
                onDeleteTile={onDeleteTile}
              />
            );
          })
        }
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    );
  }
}

GridView.propTypes = {
  tile: PropTypes.object.isRequired,
  handleOnClickAddTileToHub: PropTypes.func.isRequired,
  handleOnClickAddTileToTag: PropTypes.func.isRequired,
  getListType: PropTypes.func.isRequired,
  onSetRemoveFromHubModal: PropTypes.func.isRequired,
  onRemoveTileFromHub: PropTypes.func.isRequired,
  onSetDeleteTileModal: PropTypes.func.isRequired,
  onSelectedHubLength: PropTypes.func.isRequired,
  toggleFlipTile: PropTypes.func.isRequired,
  onSelectTile: PropTypes.func.isRequired,
  onDeleteTile: PropTypes.func.isRequired,
  handleAddTileClick: PropTypes.func.isRequired,
};

export default GridView;
