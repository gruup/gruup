import React, { Component } from 'react';
import FrontTile from './FrontTile';
import BackTile from './BackTile';
import { fileTypes } from "./common";

class DisplayTiles extends Component {

    constructor(props) {
        super(props);
        this.handleOnDeleteTile = this.handleOnDeleteTile.bind(this);
        this.handleFlip = this.handleFlip.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    shouldComponentUpdate(nextProps) {
        return (
                nextProps.tile!==this.props.tile
                ||nextProps.isSelectMode!==this.props.isSelectMode
                ||nextProps.isSelectedTile!==this.props.isSelectedTile
                );
    }

    handleOnDeleteTile() {
        const {tile, onSetDeleteTileModal, onDeleteTile} = this.props;
        onSetDeleteTileModal({
            tile,
            isOpen: true,
            isMulti: false,
            onDeleteTile
        });
    }
    ;
            handleFlip(e) {
        const {onSelectedHubLength, toggleFlipTile, tile} = this.props;
        toggleFlipTile(tile.tileId);
        if (tile.isFlip) {
            onSelectedHubLength(tile.tileId);
        }
        this.forceUpdate();
    }
    ;
            handleSelect(e) {
        const {
            tile: {
                tileId = null,
                hash = null,
                id = null
            },
            onSelectTile
        } = this.props;

        const selectTileId = tileId||hash||id;
        onSelectTile(selectTileId);
    }
    ;
            render() {
        const {
            tile,
            tile: {
                fileType,
                isFlip = false
            },
            isSelectMode,
            handleOnClickAddTileToHub,
            handleOnClickAddTileToTag,
            getListType,
            addTile,
            onSetRemoveFromHubModal,
            onRemoveTileFromHub,
            isSelectedTile
        } = this.props;

        let mainClassName = 'grid-item flip-container';
        let colorClassName = 'tile-file-name bg-white';
        let icon = '/images/google-drive-icon.svg';
        let iconClass = ""
        const frontClass = fileTypes[fileType] || 'bg-other';

        if (fileType==='URL') {
            icon = tile.favicon;
        }
        if(icon && icon.includes('google-drive-icon')) {
            iconClass = "drive-icon-img";
        }

        if (fileType==='video') {
            mainClassName = "grid-item span-2 flip-container";
        } else if (fileType==='URL') {
            mainClassName = "grid-item flip-container";
            if (tile.filePath&&tile.description) {
                mainClassName = mainClassName+" span-2-vertical";
            }
        }

        const tileClass = !isFlip ? mainClassName : `${mainClassName} hover`;

        return(
                <div className={tileClass}>
                    {
                        isSelectMode&&
                                    <div
                                        className={`select-tile-mode ${isSelectedTile ? 'selected' : ''}`}
                                        onClick={this.handleSelect}
                                        >
                                        {
                                                isSelectedTile&&
                                                            <div className="icon-check">
                                                                <i className="fa fa-check" />
                                                            </div>
                                        }
                                    </div>
                    }

                    <div className="tile-flip flipper">
                        <FrontTile
                            colorClassName={colorClassName}
                            frontClass={frontClass}
                            icon={icon}
                            onclick={this.handleFlip}
                            tile={tile}
                            handleOnClickAddTileToHub={handleOnClickAddTileToHub}
                            iconClass={iconClass}
                            />
                        <BackTile
                            onclick={this.handleFlip}
                            deleteTile={this.handleOnDeleteTile}
                            tile={tile}
                            handleOnClickAddTileToTag={handleOnClickAddTileToTag}
                            handleOnClickAddTileToHub={handleOnClickAddTileToHub}
                            getListType={getListType}
                            addTile={addTile}
                            onSetRemoveFromHubModal={onSetRemoveFromHubModal}
                            onRemoveTileFromHub={onRemoveTileFromHub}
                            />
                    </div>
                </div>
                );
    }

}

export default DisplayTiles;

