import React, { Component, Fragment } from 'react';
import { urlTypes, imgTypes } from "./common";
import PlayIcon from './PlayIcon';
import UrlTileDetails from "./UrlTileDetails";

const {
    isSharedHub
} = __global__;

const ItemLink = ({ path, filePath, fileType }) => (
    <a href={fileType == "URL" ? path : filePath} target="_blank">
        <img src={filePath} alt="" className="tiles-image-bg" />
    </a>
);

class FrontTile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            background: null
        };

        this.handlePopup = this.handlePopup.bind(this);
        this.getImage = this.getImage.bind(this);
    }

    componentWillMount() {
        const {
            tile,
            tile: {
                filePath, path, name, fileType, favicon, fileExt, imgType, source
            },
            icon,
            colorClassName,
            frontClass,
            onclick
        } = this.props;
        this.getImage().then((image) => {
            if (image) {
                // if (fileType === 'jpg' || fileType==='jpg'||fileType==='jpeg'||fileType==='png'||fileType==='gif'||fileType==='tif'||fileType==='psd') {
                    const background = `url(${image}) center / cover no-repeat, url("/images/folder.png") no-repeat center 35% #f8f8f8`;
                    this.setState({ background });
                // }
            } else {
                this.setState({background: null});
            }
        });
    }

    handlePopup(e) {
        e.stopPropagation();
        if (isSharedHub===1) {
            return true;
        }
        const {
            handleOnClickAddTileToHub,
            tile: {
                tileId
            }
        } = this.props;
        handleOnClickAddTileToHub(tileId);
    }
    ;
    async getImage() {
        const {
            tile: {
                tileId,
                imgType: type,
                thumbImg,
                fileType,
                source
            }
        } = this.props;

        if (
            !(source === 'GoogleDrive' && fileType === 'video')
            && (type === 'other' || type === 'URL')
        ) {
            return null;
        } else if (imgTypes[type]) {
            return imgTypes[type];
        } else if (thumbImg) {
            if (thumbImg.indexOf("http:") === 0 || thumbImg.indexOf("https:") === 0) {
                if (type === "video") {
                    return thumbImg;
                }
                return null;
            }
            return `/thumbImg/${thumbImg}`;
        } else {
            return Routing.generate("api_save_thumb_image", { id: tileId });
        }
    };
    
    render() {

        const {
            tile,
            tile: {
                filePath, path, name, fileType, favicon, fileExt, imgType, source
            },
            icon,
            colorClassName,
            frontClass,
            onclick,
            iconClass,
            addTile
        } = this.props;
        const {background} = this.state;

        const tileInfo = (filePath) ? (<ItemLink fileType={fileType} path={path} filePath={filePath} />) : <UrlTileDetails tile={tile} />;
         
        const backgroundProps = (background) ? {background} : {};
        return(
                <div className={`front front-tile ${frontClass}`} style={backgroundProps}>
                    <div className="tiles-image">
                        {[...urlTypes, "image"].includes(fileType) ? tileInfo : <a href={filePath} target="_blank" className={`${tile.imgType==='other' ? 'bg' : ''}`} download={name}/>}
                        {frontClass==='bg-video'&&<PlayIcon tile={tile} />}
                        <div className="tiles-hover">
                            <span className="refresh" onClick={onclick}><i className="fa fa-info" /></span>
                            {isSharedHub!==1&&<span className="add" onClick={this.handlePopup}><img src="/images/plus.svg" alt=""/></span>}
                        </div>
                    </div>
                    <div className={colorClassName}>
                        {urlTypes.includes(fileType) ?
                            <div className="tile-name">
                                <a href={(source==="GoogleDrive") ? filePath : path} target="_blank">{(name&&fileType==="video") ? name : path.replace(/^(http|https):\/\//i, '')}</a>
                                <img src={favicon ? favicon : icon} alt="" className={`icon-tiles ${iconClass} ${favicon ? "favicon_icon" : ''}`}/>
                            </div>
                            :
                            <div className={`tile-name ${fileType}-is-left`}>
                                <a href={filePath} download target="_blank">{/^(https|http)/.test(name)? "" : name}</a>
                                <a href={filePath} download target="_blank">
                                    <img src={icon} alt="" className={`icon-tiles ${iconClass}`} />
                                </a>
                                {
                                    imgType==='other'&&fileExt&&<span className="tile-ext">{fileExt.replace(/\./g, "")}</span>
                                }
                            </div>
                        }
                    </div>
                    <div className="content">
                        <UrlTileDetails tile={tile} />
                    </div>
                </div>
                );
    }
}

export default FrontTile;
