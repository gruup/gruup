import {Fragment} from "react";
import {urlTypes} from "./common";
import truncate from "lodash/truncate";
import React from "react";

/**
 *
 * @param tile
 * @returns {*}
 * @constructor
 */
const UrlTileDetails = ({tile}) => {
  const {
    fileType,
    name,
    path,
    description,
    filePath,
    source
  } = tile;

  let href = path;

  if (fileType !== 'URL') {
    href = filePath;
  }
  if (source === 'GoogleDrive' && fileType === 'video') {
    href = filePath;
  }

  const desc = fileType === 'URL' ? truncate(description, {length: 125}) : '';
  return (
    <Fragment>
      {urlTypes.includes(fileType) && name && <p className="link-title"><a href={href} target="_blank">{name}</a></p>}
      <p className="is-size-7">
        <a href={href} target="_blank" className="link-description">
          {desc}
        </a>
      </p>
    </Fragment>
  );
};

export default UrlTileDetails;