import React, {Component} from 'react';

const {
  isSharedHub
} = __global__;

class BackTile extends Component {

  constructor(props) {
    super(props);
    this.onClickAddTileToTag = this.onClickAddTileToTag.bind(this);
    this.handlePopup = this.handlePopup.bind(this);
    this.handleRemoveFromHub = this.handleRemoveFromHub.bind(this);
  }

  onClickAddTileToTag(e) {
    e.stopPropagation();
    if (isSharedHub === 1) {
      return true;
    }
    const {handleOnClickAddTileToTag, tile} = this.props;
    handleOnClickAddTileToTag(tile.tileId);
  };

  handlePopup(e) {
    e.stopPropagation();
    if (isSharedHub === 1) {
      return true;
    }
    const {handleOnClickAddTileToHub, tile} = this.props;
    handleOnClickAddTileToHub(tile.tileId);
  }

  handleRemoveFromHub() {
    const {
      onSetRemoveFromHubModal,
      onRemoveTileFromHub,
      tile
    } = this.props;

    onSetRemoveFromHubModal({
      tile,
      onRemove: onRemoveTileFromHub,
      isOpen: true,
      isMulti: false
    });
  }

  render() {
    const {
      tile: {
        created,
        owner,
        fileSize,
        isDelete,
        tags,
        hubs = []
      },
      onclick,
      deleteTile,
      getListType,
      addTile
    } = this.props;
    const listType = getListType() || 'tile';

    return (
      <div className="back back-tile">
        <div className="top-section-flip">
          <span className="refresh" onClick={onclick}><img src="/images/update-arrows.png" alt=""/></span>
          {isDelete &&
          <span className="delete-tile" onClick={deleteTile}><img src="/images/trash.svg" alt=""/></span>
          }
          {
            isSharedHub !== 1 &&
            (listType !== 'hub' || !addTile) &&
            <span className="add" onClick={this.handlePopup}><img src="/images/plus.svg" alt=""/></span>
          }
          {
            listType === 'hub' && addTile &&
            <span className="add" onClick={this.handleRemoveFromHub}><img src="/images/minus.svg" alt=""/></span>
          }
        </div>
        <ul>
          <li><b>Added on:</b> {created}</li>
          <li><b>Author: </b>{owner}</li>
          {fileSize && <li>Size: {fileSize}</li>}
          <li><b>Saved in:</b>
            <span
              className="hubs-count"
            >{" "} {hubs.length} {" "}{hubs.length <= 1 ? "hub" : "hubs"}
            </span>
          </li>
        </ul>
        <div className="bottom-section-flip" onClick={this.onClickAddTileToTag}>
          <span className="tag-flip"><img src="/images/tag-white.png" alt=""/></span>
          <span className="tag-dot-content">{tags.map(({name}, i) => <span
            key={i}>{name}&nbsp;{i < (tags.length - 1) && "|"}&nbsp;</span>)}</span>
          {
            isSharedHub !== 1 &&
            <span className="plus"><i className="fa fa-plus"/></span>
          }
        </div>
      </div>
    );
  }
}

export default BackTile;
