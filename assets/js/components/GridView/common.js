export const urlTypes = ['URL', 'video'];
export const imgFileTypes = ['png', 'jpg', 'jpeg', 'gif', 'bmp'];

export const imgTypes = {
    spreadsheet: '/images/excel.jpg',
    document: '/images/bg-word.jpg',
    pdf: '/images/pdf.jpg',
    ppt: '/images/powerpoint.jpg',
};

export const fileTypes = {
    video: 'bg-video',
    image: 'bg-image',
    spreadsheet: 'bg-excel',
    document: 'bg-doc',
    URL: 'bg-url',
    pdf: 'bg-pdf',
    jpg: 'bg-image',
    jpeg: 'bg-image',
    svg: 'bg-image',
    gif: 'bg-image',
    tif: 'bg-image',
    psd: 'bg-image',
    png: 'bg-image',
    pptx: 'bg-ppt',
    ppt: 'bg-ppt',
    pps: 'bg-ppt',
    doc: 'bg-doc',
    docx: 'bg-doc',
    xls: 'bg-excel',
    xlsx: 'bg-excel'
};
