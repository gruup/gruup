import React from "react";
import { urlTypes } from './common';

/**
 *
 * @param tile
 * @returns {*}
 * @constructor
 */
const PlayIcon = ({ tile }) => {
  const {
    fileType,
    path,
    filePath
  } = tile;

  if (urlTypes.includes(fileType)) {
    return (
      <a href={path} target="_blank" className="playIcon" />
    )
  }

  return (
    <a href={filePath} download target="_blank" className="playIcon" />
  )
};

export default PlayIcon;