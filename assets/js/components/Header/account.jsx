import React, { Component } from 'react';
import { reduxForm } from 'redux-form';

const required = (value) => value ? undefined : "Name must be required!";

class Account extends Component {

    componentDidMount() {
        const {onGetUserDetails} = this.props;
        onGetUserDetails();
    }

    handleRoute = (e) => {
        e.preventDefault();
        const {history, handleOnClick} = this.props;
        handleOnClick();
        const url = Routing.generate('welcome_home');
        history.push(url);
    }
    ;
            handleGoogleLogin = (e) => {
        e.preventDefault();
        const {history, setSearch} = this.props;
        setSearch({isLoadingLog: true});
        window.location = Routing.generate('connect_google');
    }
    ;
            render() {
        const {
            handleSubmit,
            handleOnClick,
            error,
            isShowPopup,
            data: {
                email,
                googleLogin,
                googleEmail
            },
            isLoadingLog,
            isLoadingUser,
            totalTile,
        } = this.props;
        const loginWithGoogle = !isLoadingUser ? (<div>
            <a
                href={Routing.generate('connect_google')}
                onClick={this.handleGoogleLogin}
                className="logout__link"
                >
                Login with Google
            </a>
        </div>
                ) : '';

        const loginWithDropBox = !isLoadingUser ? (<div>
            <a
                href={'dropBox/auth'}
                className="logout__link"
                >
                Login with DropBox
            </a>
        </div>
                ) : '';


        return (
                <div className={isShowPopup ? 'modal is-active' : 'modal'}>
                    <form onSubmit={handleSubmit}>
                        <div className="modal-background" />
                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">Manage your account</p>
                                <button className="delete" type="button" aria-label="close" onClick={handleOnClick} />
                            </header>
                            <section className={`modal-card-body ${ isLoadingLog ? 'position-relative' : ''}`}>
                                {
                                    isLoadingLog&&
                                            (<div className="loading-account">
                                                <span>Loading...</span>
                                            </div>)
                                }
                                <div className="search-modal">
                                    <div>
                                        <span className="logout__link-caption"><strong>{isLoadingLog ? 'Email:' : ''}</strong> {email}</span>
                                        {googleLogin ?
                                    <div>
                                        <a href="" onClick={this.handleRoute} className="logout__link">
                                            Drive Details ({googleEmail})
                                        </a>
                                        <span>  | </span>
                                        <a href={Routing.generate('logout_google')} className="logout__link">
                                            Logout
                                        </a>
                                    </div>
                                                    :
                                                    loginWithGoogle
                                        }
                                    </div>
                                    {error&&<p className="help is-danger">{error}</p>}
                                    <br />
                                </div>
                            </section>
                            <footer className="modal-card-foot">
                                <button className="button search-btn-modal"
                                        type="button"
                                        onClick={handleOnClick}
                                        >
                                    Cancel
                                </button>
                            </footer>
                        </div>
                    </form>
                </div>
                );
    }
}

export default reduxForm({
    form: 'Account',
})(Account);
