import React, { Component } from 'react';


class AccountHeader extends Component {

            render() {
        const {
            handleSubmit,
            handleOnClick,
            isShowPopup,
           
        } = this.props;

        return (
                <div className={isShowPopup ? 'modal is-active' : 'modal'}>
                    <form onSubmit={handleSubmit}>
                        <div className="modal-background" />
                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">Manage your account</p>
                                <button className="delete" type="button" aria-label="close" onClick={handleOnClick} />
                            </header>
                           
                            <footer className="modal-card-foot">
                                <button className="button search-btn-modal"
                                        type="button"
                                        onClick={handleOnClick}
                                        >
                                    Cancel
                                </button>
                            </footer>
                        </div>
                    </form>
                </div>
                );
    }
}

export default AccountHeader;

