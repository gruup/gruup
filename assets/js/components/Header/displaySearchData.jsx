import React, { Component } from 'react';
import Highlighter from 'react-highlight-words';


class SearchItem extends Component {

  handleDetailClick = (e) => {
    e.preventDefault();
    const {
      item: {
        fileType = "",
        id,
        hash,
        tileId
      } = {},
      onSetTileDetailModal,
      handleClickOutside,
      history
    } = this.props;

    switch(fileType.toLowerCase()) {
      case 'hub':
        handleClickOutside();
        return history.push(`/welcome/view/hub/${hash}`);

      case 'tag':
        handleClickOutside();
        return history.push(`/welcome/view/tag/${id}`);

      default:
        return onSetTileDetailModal({ isOpen: true, tileId });
    }
  };

  render() {

    const { searchText, item } = this.props;

    return(
      <li className="searchItem">
        {/* <span onClick={this.handleDetailClick}> */}
          <span>
          <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={[searchText]}
            autoEscape
            textToHighlight={item.name}
          />
        </span>
      </li>
    );
  }
}

class DisplaySearchData extends Component {
  render() {
    const { data = [] } = this.props;

    return [
      ...data.map((item, key) =>
        <SearchItem
          key={key}
          item={item}
          {...this.props}
        />)
    ];
  }
}

export default DisplaySearchData;
