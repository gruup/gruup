import React, {Component, Fragment} from 'react';
import barIcon from "../../../../public/images/sidebar/bar.png";
import logoIcon from "../../../../public/images/gruup_logo.svg";
import Account from "./account";
import Search from "./search";
import menuIcon from "../../../../public/images/G_icon_Red.svg";
import moreIcon from "../../../../public/images/navbar/showMoreButton.svg";
// import {NotificationManager} from 'react-notifications';
import workerAction from '../../actions/worker';
import WebWorker from '../../common/webWorker';
import {getListType} from '../../actions/pagination';
import AccountHeader from '../../components/Header/accountHeader';
import userIcon from '../../../../public/images/navbar/userIcon.png'
const {
    isSharedHub,
    isConnectGoogle = "0"
} = __global__;

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isShowPopup: false,
            isShowProfile: false,
            isCloseMenu: false
        };

        this.setUpWorker = this.setUpWorker.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.handleAccount = this.handleAccount.bind(this);
        this.handleClass = this.handleClass.bind(this);
        this.handleGoogleSync = this.handleGoogleSync.bind(this);
        this.handleLink = this.handleLink.bind(this);
        this.handleLogo = this.handleLogo.bind(this);
        this.closeNotice = this.closeNotice.bind(this);
        this.setUpWorker()
    }
    
    setUpWorker() {
        this.worker = new WebWorker(workerAction);
        this.worker.addEventListener("message", ({data = {}}) => {
            const {result = "", message = "", pageToken = null, total = 0} = data;
            const {
                setDriveData,
                setGoogleNotify,
                getAllTiles,
                user: {
                    id,
                } = {},
                setTile,
                totalTile,
                driveData
            } = this.props;          
            let {offset, limit} = driveData;
            const listType = getListType();
            if (result==='success') {
                offset += limit;
                
                if (listType==='tile'&&totalTile<=0) {
                    getAllTiles({id});
                }
                
                if (pageToken) {
                    setDriveData({
                        pageToken,
                        offset
                    });
                    
                    const body = {
                        limit,
                        offset,
                        pageToken
                    };
                    const url = window.location.origin+Routing.generate('get_drive');
                    this.worker.postMessage({url, body});
                } else {
                    if (listType==='tile') {
                        getAllTiles({id});
                    }
                    const isCloseAfter = true;
                    setGoogleNotify({
                        isOpen: true,
                        message: `Successfully sync`
                    }, isCloseAfter);
                    setDriveData({
                        pageToken,
                        offset: 0
                    });
                    setTile({isStartSync: false});
                }
            } else {
                setGoogleNotify({
                    isOpen: false,
                    message: ''
                });
                setTile({isStartSync: false});
                // NotificationManager.error(message, 'Error', 3000);
            }
        });
        
        if (isConnectGoogle==="1") {
            this.handleGoogleSync();
        }
    }
    
    handleOnClick(e) {
        const { setTile } = this.props;
        setTile({isShowMobileSidebar : true});
    }
    ;
    handleAccount() {
        const {
                isShowPopup
            } = this.state;
            
            this.setState({
                    isShowPopup: !isShowPopup
                });
            }
            ;
            handleClass() {
                const {
                    onGetClassName,
                } = this.props;
                
                const {isCloseMenu} = this.state;
                this.setState({
                    isCloseMenu: !isCloseMenu
                });
                
                onGetClassName();
            }
            ;
            handleGoogleSync(event = {}) {
                if (typeof event.preventDefault==='function') {
                    this.closeNotice();
                    event.preventDefault();
                }
                
                const {driveData = {}, setTile, setGoogleNotify} = this.props;
                const {limit, offset} = driveData;
                const body = {
                    limit,
                    offset
                };
                setTile({isStartSync: true});
                setGoogleNotify({
                    isOpen: true,
                    message: `Do not close or refresh your browser whilst sync is in progress`
                });
                const url = window.location.origin+Routing.generate('get_drive');
                this.worker.postMessage({url, body});
            }
            ;
            handleLink(event) {
                const { isShowProfile } = this.state;
                this.setState({isShowProfile:!isShowProfile})
                event.preventDefault();
            }
            ;
            handleLogo(event) {
                event.preventDefault();
                const listType = getListType();
                const {
                    getAllTiles,
                    user: {
                        id,
                    } = {},
                    setTile,
                    history,
                    setSearch,
                    onResetOffset
                } = this.props;
                setSearch({isSearchView: false, searchText: ""});
                
                if (listType==='tile') {
                    const wrapper = document.querySelector(".content-wrap")||{};
                    wrapper.scrollTop = 0;
                    getAllTiles({id}).then(() => {
                        setTile({
                            tileLoaded: true
                        })
                    });
                }
                
                onResetOffset();
                const url = Routing.generate('welcome_home');
                history.push(url);
            }
            ;
            closeNotice() {
                const {setSearch} = this.props;
                setSearch({modifyCount: 0});
            }
            ;
            render() {
                const {
                    isShowPopup,
                    isShowProfile,
                    isCloseMenu

                } = this.state;
                
                const {
                    onGetUserDetails,
                    onSetTileDetailModal,
                    account: {
                        data,
                        changeClass,
                        isLoadingLog = false,
                        isLoadingUser = false,
                        modifyCount = 0
                    },
                    history,
                    account,
                    searchTile,
                    setSearch,    
                } = this.props;
        return (
            <Fragment>
                    {modifyCount>0&&
                                    <div className="suggest-sync">
                                        {modifyCount}&nbsp;Files on your Google Drive have changed. Resync them now.&nbsp;
                                        <a href="" onClick={this.handleGoogleSync}>Sync</a>.
                                        <span className="close" onClick={this.closeNotice}><i className="fa fa-times"/></span>
                                        <span className="clear-both"/>
                                    </div>
                    }
                    {isShowPopup&&
                                    <Account
                                        isShowPopup={isShowPopup}
                                        handleOnClick={this.handleAccount}
                                        data={data}
                                        onGetUserDetails={onGetUserDetails}
                                        history={history}
                                        isLoadingLog={isLoadingLog}
                                        isLoadingUser={isLoadingUser}
                                        setSearch={setSearch}
                                        />
                    }
                    <div className="navbar-wrapper">
                        <nav className="navbar is-transparent">
                            <div className="navbar-burger burger" onClick={this.handleLogo}>
                                <img src={ menuIcon }
                                     className="menu mobile_gruup_logo" />
                            </div>
                            {/* <div className="navbar-brand">
                                <a className="navbar-item" onClick={this.handleLogo}>
                                    <img src={logoIcon} className="Logo"/>
                                </a>
                            </div> */}

                            {isSharedHub!==1&&
                                    <div className={changeClass ? "field-search-menu" : "field"}>
                                        <Search
                                            {...account}
                                            searchTile={searchTile}
                                            history={history}
                                            onSetTileDetailModal={onSetTileDetailModal}
                                            />
                                    </div>
                            }

                            {isSharedHub!==1&&

                                    <div className="navbar-menu">
                                        <div className="navbar-end" onClick={this.handleOnClick}>
                                            <span className="navbar-item">
                                                <img src={barIcon} className="user-icon"/>
                                            </span>
                                            <div className="short__menu">
                                                {/* <div className="user__details">
                                                    <div className="user__name">Hi, Darryl</div>
                                                    <img src={userIcon} onClick={this.handleAccount} />
                                                </div> */}
                                                <img src={moreIcon} className="dot-icon" onClick={this.handleLink}/>
                                            </div>
                                        
                                           
                                            <AccountHeader 
                                                isShowPopup={isShowPopup}
                                                handleOnClick={this.handleAccount}
                                                data={data}
                                                onGetUserDetails={onGetUserDetails}
                                                history={history}
                                                isLoadingLog={isLoadingLog}
                                                isLoadingUser={isLoadingUser}
                                                setSearch={setSearch}
                                            />
                                            <div className="navbar-item has-dropdown">
                                                
                                                {/* <a className="navbar-link" href="#" >
                                                    Account
                                                </a> */}
                                                <div
                                                    className={isShowProfile ? "dropdown is-open hub-open-setting account__dropdown" : "dropdown hub-open-setting account__dropdown"}>
                                                        <ul>
                                                            {/* <li><span className="nav-item">My Profile</span> </li>
                                                            <li><span className="nav-item">Account Settings</span></li>
                                                            <li><span className="nav-item">What's New</span></li>
                                                            <li><span className="nav-item">Support</span></li> */}
                                                            <li><a href='/logout' className="nav-item-logout nav-item">Logout</a></li>
                                                            {/* <hr className="gray"/> */}
                                                            {/* <li className="delete__element button__upgrade">
                                                                <span className="nav-item">Upgrade</span>
                                                            </li> */}
                                                        </ul>


                                                    <span className="navbar-item account" onClick={this.handleAccount}>
                                                        Connections
                                                    </span>
                                                    <a className="navbar-item" href="/logout">
                                                        Logout
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            }
                            {isSharedHub===1&&<div className="navbar-menu">
                                <div className="navbar-end vertical-center">
                                    <a className="button large submit-btn-modal is-hidden-mobile" href={Routing.generate('user_registration')}>Join FREE today</a>
                                    <a className="button small submit-btn-modal is-hidden-tablet" href={Routing.generate('user_registration')}>Join FREE</a>
                                </div>
                            </div>}
                        </nav>
                    </div>
                </Fragment>
                );
    }

}

export default Header;
