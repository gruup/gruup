import React, { Component } from 'react'
import onClickOutside from "react-onclickoutside";
import DisplaySearchData from "./displaySearchData";
import searchBarIcon from "../../../../public/images/search.svg";

class Search extends Component {
    state = {
        isShow: false
    };

    handleClickOutside = () => {
        const { isShow } = this.state;
        if (isShow) {
            this.setState({
                isShow: !isShow
            });
        }
    }

    handleInputChange = (event) => {
        const { searchTile } = this.props;
        searchTile(this.search.value);
        const isShow = this.search.value != "" && this.search.value;
        this.setState({ isShow });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const { searchTile, history } = this.props;
        const isSetToTile = true;
        if(!this.search.value) {
            return true;
        }

        const wrapper = document.querySelector(".content-wrap") || {};
        wrapper.scrollTop = 0;
        history.push(`/welcome?q=${this.search.value}`);
        searchTile(this.search.value, isSetToTile);
        this.setState({ isShow: false });
    }

    render() {
        const { isShow } = this.state;
        const {
            searchData,
            isLoading,
            onSetTileDetailModal,
            history,
            handleClickOutside
        } = this.props;

        return (
            <form onSubmit={this.handleSubmit}>
                <p className="control has-icons-left">
                    <span className="select">

                    <input
                        name="searchText"
                        placeholder="Search"
                        ref={input => this.search = input}
                        onChange={this.handleInputChange}
                        className="input-search"
                        autoComplete="off"
                    />
                    </span>
                    <span className="icon is-small is-left">
                      <img src={ searchBarIcon } alt=""/>
                    </span>
                </p>
                <div className={isShow ? "search-dropdown is-open" : "search-dropdown"}>
                    {
                        (!Object.keys(searchData).length && !isLoading) &&
                        <div className="no-data-found">No data found</div>
                    }
                    {
                        isLoading &&
                        <div className="loading-block loading-ani">
                            Loading
                            <span>.</span>
                            <span>.</span>
                            <span>.</span>
                        </div>
                    }
                    {
                        searchData &&
                        Object.values(searchData).map((data, key) =>
                            (<div key={key} className="search-block">
                                <div className="cat-title">
                                <img src={'/images/' + data[0].fileType.toLowerCase() + '-icon.png'} alt="" />
                                {" "} {data[0].fileType}
                                <hr />
                                <ul className="cat-body">
                                    <DisplaySearchData
                                        data={data}
                                        key={key}
                                        searchText={this.search.value}
                                        onSetTileDetailModal={onSetTileDetailModal}
                                        history={history}
                                        handleClickOutside={this.handleClickOutside}
                                    />
                                </ul>
                                </div>
                            </div>)
                        )
                    }
                </div>
            </form>
        );
    }
}

export default onClickOutside(Search);