import React, { Component, Fragment } from 'react';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    };
  }

  componentDidCatch(e) {
    this.setState({ hasError: true });
  }

  render() {
    const {
      hasError,
    } = this.state;

    if (hasError) {
      return (
        <Fragment>
          <div className="exceptions">
            <div className="exceptions__margins">
              <h1 className="e_headers e_headers--style1">500</h1>
              <h2 className="e_headers e_headers--style2">Well this is a little unexpected</h2>
              <p className="exceptions__text">
                Something's not quite right here, we'll jump straight on it. If you want to give us a heads up,
                let us know what happened. <br/>
                <a href="mailto:hello@gruup.space" className="exceptions__a">hello@gruup.space</a>
              </p>
            </div>
          </div>
        </Fragment>);
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
