import React, {Component,Fragment} from 'react';

class Tag extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    const {
      onClick,
      tag: {
        id
      },
      handleCloseMobileSidebar
    } = this.props;
    handleCloseMobileSidebar();
    return onClick(id);
  };

  render() {
    const {
      tag: {
        name
      }
    } = this.props;
    return (
      
      <div className="tag_pill" onClick={this.handleClick}>#{name}
        </div>
    
    )
  }
}

export default Tag;
