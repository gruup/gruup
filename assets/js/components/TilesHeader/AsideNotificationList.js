import React, { Component, Fragment } from 'react';
import { reduxForm } from 'redux-form';
import drive from "../../../../public/images/sidebar/drive.png";
import dropbox from "../../../../public/images/sidebar/dropbox.png";
import box from "../../../../public/images/sidebar/box.png";
import mark from "../../../../public/images/sidebar/slack_mark.png";
import buffer from "../../../../public/images/sidebar/buffer.svg";
import closeIcon from '../../../../public/images/sidebar/red_close.svg';


const required = (value) => value ? undefined : "Name must be required!";

class AsideNotificationList extends Component {
    

     render() {
        const {
            error,
            isShowNotificationSidebar,
            isLoadingUser,
            handleOnClick,
        } = this.props;
        const mainClass = isShowNotificationSidebar ? 'display-connection' : 'Connection-sidebar';

        return (
            <div className={mainClass}>
                <div className="block__top__wrap">
                    <div className="header__top">
                        <span className="header__title">Notifications</span>
                        <img src={closeIcon} alt="" onClick={handleOnClick} />
                    </div>
                    <div className="section__body">
                        <div className="content__list_wrap notification__block ">
                            <div className="content__icon">
                                <img src={buffer} alt="" />
                            </div>
                            <div className="content__desc_wrap no__border">
                                <span className="date__notification">13 feb 2019</span>
                                <p className="content__desc">You were added to the <a>Marketing team </a>Hub
                                </p>
                               
                                {error&&<p className="help is-danger">{error}</p>}
                            </div>
                        </div>
                        
                    </div>
                </div>
               
            </div>
        );
    }
}


export default AsideNotificationList;
