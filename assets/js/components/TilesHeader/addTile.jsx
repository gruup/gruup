import React, { Component } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import Input from "../../common/inputField";
import ShowTileModal from "../../components/TilesHeader/showTileModal";
import addTileIcon from "../../../../public/images/addTile.svg";
import closeIcon from '../../../../public/images/sidebar/red_close.svg';

const required = (value) => value ? undefined : "Url must be required!";
const url = value =>
  value && !/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,6}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi.test(value) ?
  'Invalid link' : undefined;

class AddHub extends Component {

  handleOnSubmitAddTile = async (values) => {
    const {
      onAddTile,
      handleOnClick,
    } = this.props;

    await onAddTile(values
      ).then(() => {
        handleOnClick();
      }).catch(({ message: _error = "" }) => {
        throw new SubmissionError({ _error });
      });
  };

  render() {
    const {
      handleSubmit,
      handleOnClick,
      pristine,
      submitting,
      error,
      isVisible,
      Tile,
      onAddTile,
    } = this.props;

    return (
      <div className={isVisible ? 'modal is-active' : 'modal'}>
        <form onSubmit={handleSubmit(this.handleOnSubmitAddTile)}>
          <div className="modal-background" />
          <div className="modal-card">
              <div className="close-modal">
                <img src={closeIcon} onClick={handleOnClick} />
              </div>
              <section className="modal-card-body new-modal-body">
                <div className="modal-title">
                <img src={addTileIcon}/>
                  Add New Tile
                </div>
                <div className="search-modal modal-form">
                  {/* <span className="modal-lable"> Web address </span> */}
                  <Field
                    name="tile"
                    type="text"
                    component={Input}
                    className="modal-input"
                    validate={[required, url]}
                    placeholder="For example: https://drive.google.com/…"
                    isCheckUrl={true}
                    serverError={Boolean(error)}
                  />
                  {error && <p className="help is-danger">{error}</p>}
                  <br />
                </div>
                  {/*
                   <ShowTileModal
                   onAddTile={onAddTile}
                   handleOnClick={handleOnClick}
                   Tile={Tile}
                   submitting={submitting}
                   pristine={pristine}
                  /> */}
                <div className="new-modal-bottom">
                  <a href="javascript:;" className="cancel-lnk" onClick={handleOnClick}>Cancel</a>
                  <button
                    className={`button submit-btn-modal ${submitting ? 'is-loading' : ''}`}
                    type="submit" disabled={pristine || submitting}
                  >
                    Create Tile
                  </button>
                </div>
              </section>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'AddHub',
})(AddHub);
