import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
// import { NotificationManager } from 'react-notifications';
import isEmpty from 'lodash/isEmpty';
import Hubs from './Hub/Hubs';
import TagList from './TagList';
import AddHub from './Hub/addHub';

const {
  isSharedHub,
} = __global__;

class AsideMenuList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hubLoaded: false,
      tagLoaded: false,
      isVisible: false,
      sortName: 'all',
      currentHub: {},
    };
    this.handleOnClickAddHub = this.handleOnClickAddHub.bind(this);
    this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
  }

  componentDidMount() {
    const {
      onGetAllHubs,
      onGetAllTags,
      user: {
        id,
      },
    } = this.props;

    if (isSharedHub !== 1) {
      onGetAllHubs({ id }).then(() => {
        this.setState({
          hubLoaded: true,
        });
      });
      onGetAllTags({ id }).then(() => {
        this.setState({
          tagLoaded: true,
        });
      });
    } else {
      this.setState({
        hubLoaded: true,
      });
    }
  }

  notAllowedAddHubs() {
    // NotificationManager.warning('You’ve reached the maximum number of Hubs allowed for a free account. Upgrade your account for more.', '', 5000);
  }

  handleOnClickAddHub() {
    const { isVisible } = this.state;
    this.setState({
      isVisible: !isVisible,
      currentHub: {},
    });
  }

  handleToggleSidebar(e) {
    e.preventDefault();
    const { onSetTile, isOpenSidebar } = this.props;
    onSetTile({ isOpenSidebar: !isOpenSidebar });
  }

  render() {
    const {
      hub: { hubs = [], firstHub } = {},
      tag = {},
      onAddEditHub,
      account: {
        changeClass,
      },
      history,
      setTag,
      setHub,
      setHubDropdown,
      isOpenSidebar,
      listType,
      currentHubId,
      currentTagId,
      user,
      onToggleSelectMode,
    } = this.props;

    const {
      hubLoaded,
      isVisible,
      currentHub,
      sortName,
    } = this.state;

    const closeSidebar = isOpenSidebar ? 'is-close' : '';

    if (isEmpty(hubs) && hubLoaded === false) {
      return (
        <Fragment>
          <div className="sidebar-wrap">
            <div className="full-menu">
              <div className="sidebar-up-block">
                <div className="sidebar-toparea">
                  <p className="is-size-7 has-text-centered">Fetching your Hubs...</p>
                </div>
              </div>
            </div>
            <p className="has-text-centered">Loading</p>
          </div>
        </Fragment>
      );
    }

    const ownHubs = hubs.filter(({ email }) => email === user.email);
    const maxHubs = (ownHubs.length >= __global__.maxHub) && (user.accountType === 'FREE');

    return (
      <Fragment>
        {isVisible
          && (
          <AddHub
            isVisible={isVisible}
            handleOnClick={this.handleOnClickAddHub}
            onAddEditHub={onAddEditHub}
            currentHub={currentHub}
            isEdit={Boolean(currentHub.id)}
          />
          )
        }
        <div className={`${changeClass ? 'show-menu' : 'sidebar-wrap'} ${closeSidebar}`}>
          <div className="full-menu">
            <div className="sidebar-up-block">
              <div className="sidebar-toparea">
                {isSharedHub !== 1 && (
                  <div className="hub-select">
                    <div
                      className={`add-hub ${maxHubs ? 'inactive' : ''}`}
                      onClick={maxHubs ? this.notAllowedAddHubs : this.handleOnClickAddHub}
                    >
                      <div className="add-new-hub">
                          +
                      </div>
                      <label htmlFor="add-hub">
                        Add Hub
                      </label>
                    </div>
                  </div>)
                }
                {firstHub && (
                  <div className="first-hub" onClick={this.handleOnClickAddHub}>
                    <div className="first-hub-txt">
                      Click
                      {' '}
                      <span className="txt-bold">
                        Add Hub
                      </span>
                      {' '}
                      to create your first Hub
                    </div>
                  </div>)
                }
                {(!isEmpty(hubs) && hubLoaded === true) && (
                  <Hubs
                    hubs={hubs}
                    listType={listType}
                    currentHubId={currentHubId}
                    setHub={setHub}
                    setHubDropdown={setHubDropdown}
                    history={history}
                    sortName={sortName}
                    onToggleSelectMode={onToggleSelectMode}
                  />)
                }
              </div>
              <div className="hub-slider-minimise" onClick={this.handleToggleSidebar}>
                <a href="" className="toggel" />
              </div>
            </div>
            {isSharedHub !== 1 && (
              <TagList
                history={history}
                setTag={setTag}
                tags={tag.tags}
                currentTagId={currentTagId}
                onToggleSelectMode={onToggleSelectMode}
              />)
            }
          </div>
        </div>
        <div className="menu-short" onClick={this.handleToggleSidebar}>
          <div>
            <img src="/images/folder.png" alt="" />
            <img src="/images/tag@3x.png" width="26" height="28" alt="" />
          </div>
        </div>
      </Fragment>
    );
  }
}

AsideMenuList.propTypes = {
  onGetAllHubs: PropTypes.func.isRequired,
  onGetAllTags: PropTypes.func.isRequired,
  onAddEditHub: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
  onSetTile: PropTypes.func.isRequired,
  isOpenSidebar: PropTypes.bool.isRequired,
  hub: PropTypes.shape({
    hubs: PropTypes.array.isRequired,
    firstHub: PropTypes.bool.isRequired,
  }).isRequired,
  tag: PropTypes.object.isRequired,
  account: PropTypes.shape({
    changeClass: PropTypes.bool.isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,
  setTag: PropTypes.func.isRequired,
  setHub: PropTypes.func.isRequired,
  setHubDropdown: PropTypes.func.isRequired,
  listType: PropTypes.string,
  currentHubId: PropTypes.string,
};

export default AsideMenuList;
