import React, {Component, Fragment} from 'react';
import AddTile from "./addTile";
import {Link} from 'react-router-dom';
import plusSvg from '../../../../public/images/icon_plus_black.svg';
import folderIcon from '../../../../public/images/navbar/folder.png';
import homeIcon from '../../../../public/images/home_black.svg';
import gridView from '../../../../public/images/grid-view.svg';
import listView from '../../../../public/images/list-view.svg';
import closeIcon from '../../../../public/images/sidebar/red_close.svg';

const {
  isSharedHub
} = __global__;

class TileHeader extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    };
  }

  handleOnClickAddTile = (e) => {
    const {onSetUser} = this.props;
    onSetUser({ isTilePopupActive: false });
  };

  handleHubModal = (event) => {
    event.preventDefault();
    const {
      onSetHubModal,
      postSelectedTile,
      tile: {
        selectedTile = []
      } = {}
    } = this.props;

    if(selectedTile.length <= 0) {
      return true;
    }

    const params = {
      isOpen: true,
      isMultiSelect: true,
      onSubmit: postSelectedTile,
      params: {
        type: 'hubIds'
      }
    }
    onSetHubModal(params);
  }

  handleTagModal = (event) => {
    event.preventDefault();
    const {
      onSetTagModal,
      postSelectedTile,
      tile: {
        selectedTile = []
      } = {}
    } = this.props;

    if(selectedTile.length <= 0) {
      return true;
    }

    const params = {
      isOpen: true,
      isMultiSelect: true,
      onSubmit: postSelectedTile,
      params: {
        type: 'tags'
      }
    }
    onSetTagModal(params);
  }

  handleRemove = (event) => {
    event.preventDefault();
    const {
      onRemoveSelectedTile,
      onSetDeleteTileModal,
      tile: {
        selectedTile = []
      } = {}
    } = this.props;

    if(selectedTile.length <= 0) {
      return true;
    }

    onSetDeleteTileModal({
      isOpen: true,
      isMulti: true,
      onDeleteTile: onRemoveSelectedTile
    });
  }

  handleRemoveFromHub = (event) => {
    event.preventDefault();
    const {
      onSetRemoveFromHubModal,
      onRemoveTileFromHub,
      tile
    } = this.props;
    const { selectedTile = [] } = tile;

    if(selectedTile.length <= 0) {
      return true;
    }

    onSetRemoveFromHubModal({
      tile,
      onRemove: onRemoveTileFromHub,
      isOpen: true,
      isMulti: true
    });
  }

  resetSearch = () => {
    const {
      onSetSearch,
      onGetAllTiles,
      onResetOffset,
      user: { id },
      history: { location: { pathname = "" } } = {},
      setActiveTab,
    } = this.props;
    onResetOffset();
    onSetSearch({ isSearchView: false, searchText: null });
    if(pathname === "/welcome") {
      onGetAllTiles({ id });
    }
    setActiveTab('home');
  }

  render() {
    const {
      tile,
      tile: {
        total,
        name,
        isSelectMode = false,
        selectedTile = [],
        addTile,
        isStartSync = false,
        driveData: {
          offset: numberOfSyncFile = 0
        } = 0,
        isChangeView,
        activeTabName,
      },
      account: {
        isSearchView = false
      },
      onAddTile,
      onclick,
      onToggleSelectMode,
      getListType,
      isTilePopupActive,
      handleAddTileClick,
      setActiveTab,
    } = this.props;
    const listType = getListType();
    const nonSelect = (selectedTile.length <= 0)? 'none-select': '';

    const syncMessage = isStartSync ?(
      <span className="syncing"> - {numberOfSyncFile? numberOfSyncFile: ''} Syncing
        <span>.</span>
        <span>.</span>
        <span>.</span>
      </span>
    ): '';
  
  const headerLabel = !isSelectMode && <label>   <img src={homeIcon} className="icon__list"></img> Home | <span> &nbsp;  {total} items</span>{syncMessage}</label>
  const mobileHeaderLabel = !isSelectMode && <label>   <img src={homeIcon} className="icon__list"></img> All your content | <span> &nbsp;  {total} items</span>{syncMessage}</label>
  const header = window.innerWidth > 768 ? headerLabel : mobileHeaderLabel;

    return (
      <Fragment>
        {isTilePopupActive &&
          <AddTile
            isVisible={isTilePopupActive}
            handleOnClick={this.handleOnClickAddTile}
            onAddTile={onAddTile}
            Tile={tile}
            />
        }

        <div className="content-top-bar">
          <div className="tiles-count">
            {
              isSelectMode &&
              <label> Selected: <span> &nbsp;  {selectedTile.length} item{selectedTile.length > 1 && 's'}</span>
                {syncMessage}
              </label>
            }
            { (name || isSearchView) && !isSelectMode ?
                <span className='tiles-header-span'>
                    <label
                    className="header-label"
                    title={isSearchView? 'Search Result': name}
                    alt={isSearchView? 'Search Result': name}>
                    <span className="head-title">{isSearchView? 'Search Result': name} </span>
                     :
                    <span> &nbsp;  {total} items</span>
                    {syncMessage}
                  </label>
                  {
                    isSharedHub != 1 &&
                    <Link to={Routing.generate('welcome_home')} onClick={this.resetSearch} className="close-hub">Close </Link>
                  }
                </span>
              :
              header
              
            }
          </div>
          <div className="content-action navbar-end">
            {
              isSelectMode?
              <Fragment>
                <a href="#" className={`select-tile-action ${nonSelect}`} onClick={this.handleHubModal}>
                  <img src="/images/hub-icon.png" />
                  <span>Add to Hub</span>
                </a>
                {
                  (listType == "hub" && addTile) &&
                  <a href="#" className={`select-tile-action ${nonSelect}`} onClick={this.handleRemoveFromHub}>
                    <img src="/images/hub-icon.png" />
                    <span>Remove from Hub</span>
                  </a>
                }
                <a href="#" className={`select-tile-action ${nonSelect}`} onClick={this.handleTagModal}>
                  <img src="/images/tag.png" />
                  <span>Add Tag</span>
                </a>
                <a href="#" className={`select-tile-action ${nonSelect}`} onClick={this.handleRemove}>
                  <i className="fa fa-trash" />
                  <span>Delete</span>
                </a>
                <span className="close-selection" onClick={onToggleSelectMode}>
                  <img src={closeIcon} />
                </span>
              </Fragment>
              : <Fragment>
                  {
                    isSharedHub != 1 &&
                    <button className="select-multiple" onClick={onToggleSelectMode}>Bulk Select</button>
                  }
                  {   addTile &&
                      isSharedHub != 1 &&
                      <div className="button__add_hub btn__plus_dark" onClick={handleAddTileClick}>
                        <img src={ plusSvg } alt="" />
                        Add Tile
                      </div>
                  }
                  {/* <img src={folderIcon}
                       className="change-view"
                  /> */}
                  <img src={isChangeView ? gridView : listView }
                       className="change-view"
                       onClick={onclick}
                  />
              </Fragment>
            }
          </div>
        </div>
      </Fragment>
    );
  }
}

export default TileHeader;