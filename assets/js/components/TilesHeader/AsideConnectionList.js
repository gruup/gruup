import React, { Component, Fragment } from 'react';
import { reduxForm } from 'redux-form';
import drive from "../../../../public/images/sidebar/drive.png";
import dropbox from "../../../../public/images/sidebar/dropbox.png";
import box from "../../../../public/images/sidebar/box.png";
import mark from "../../../../public/images/sidebar/slack_mark.png";
import {getListType} from '../../actions/pagination';
import closeIcon from '../../../../public/images/sidebar/red_close.svg';
import workerAction from '../../actions/worker';
import WebWorker from '../../common/webWorker';
const required = (value) => value ? undefined : "Name must be required!";
const {
    isSharedHub,
    isConnectGoogle = "0"
} = __global__;

class AsideConnectionList extends Component {
    
    constructor(props){
        super(props);
        this.handleGoogleSync = this.handleGoogleSync.bind(this);
        this.setUpWorker = this.setUpWorker.bind(this);

        this.setUpWorker();        
    }
    setUpWorker() {
        this.worker = new WebWorker(workerAction);
        this.worker.addEventListener("message", ({data = {}}) => {
            const {result = "", message = "", pageToken = null} = data;
            const {
                setDriveData,
                onSetGoogleNotify,
                getAllTiles,
                user: {
                    id,
                } = {},
                setTile,
                tile:{
                    driveData,
                    total,
                },
                user,
                tile
            } = this.props;         
            let {offset, limit} = driveData;
            const listType = getListType();
            if (result==='success') {
                offset += limit;
                
                if (listType==='tile'&&total<=0) {
                    getAllTiles({id});
                }
                
                if (pageToken) {
                    setDriveData({
                        pageToken,
                        offset
                    });
                    
                    const body = {
                        limit,
                        offset,
                        pageToken
                    };
                    const url = window.location.origin+Routing.generate('get_drive');
                    this.worker.postMessage({url, body});
                } else {

                    if (listType==='tile') {
                        getAllTiles({id});
                    }
                    const isCloseAfter = true;
                    onSetGoogleNotify({
                        isOpen: true,
                        message: `Successfully sync`
                    }, isCloseAfter);
                    setDriveData({
                        pageToken,
                        offset: 0
                    });
                    setTile({isStartSync: false});
                }
            } else {
                onSetGoogleNotify({
                    isOpen: false,
                    message: ''
                });
                setTile({isStartSync: false});
                // NotificationManager.error(message, 'Error', 3000);
            }
        });
        
        if (isConnectGoogle==="1") {
            this.handleGoogleSync();
        }
    }
        closeNotice() {
            const {setSearch} = this.props;
            setSearch({modifyCount: 0});
        }
        ;
         handleGoogleSync(event = {}) {
        if (typeof event.preventDefault==='function') {
            this.closeNotice();
            event.preventDefault();
        }

        const {
         driveData = {},
         setTile,
         onSetGoogleNotify,
         handleHomeClick,
        } = this.props;
        const {limit, offset} = driveData;
        const body = {
            limit,
            offset
        };
        setTile({isStartSync: true});
        onSetGoogleNotify({
            isOpen: true,
            message: `Do not close or refresh your browser whilst sync is in progress`
        });
        const url = window.location.origin+Routing.generate('get_drive');
        this.worker.postMessage({url, body});
    }
    ;
    handleGoogleLogin = (e) => {
        e.preventDefault();
        const {history, setSearch} = this.props;
        setSearch({isLoadingLog: true});
        window.location = Routing.generate('connect_google');
    }
    ;

     render() {
        const {
            setSearch,
            setTile,
            data:{
                email,
                googleLogin,
                googleEmail,
            },
            error,
            isShowConnectionSidebar,
            isLoadingUser,
            handleOnClick, 
            setDriveData,
            tile,
        } = this.props;
        const mainClass = isShowConnectionSidebar ? 'display-connection' : 'Connection-sidebar';
       const loginWithGoogle = !isLoadingUser ? (<div>
        <a
            href={Routing.generate('connect_google')}
            onClick={this.handleGoogleLogin}
            className="logout__link"
            >
            Login with Google
        </a>
      </div>
            ) : '';
        return (
            <div className={mainClass}>
                <div className="block__top__wrap">
                    <div className="header__top">
                        <span className="header__title">Connections</span>
                        <img src={closeIcon} alt="" onClick={handleOnClick} />
                    </div>
                    <div className="section__body">
                        <div className="content__list_wrap">
                            <div className="content__icon">
                                <img src={drive} alt="" />
                            </div>
                            <div className="content__desc_wrap">
                                <p className="content__desc">{ googleLogin ? "Connected" : "Not Connected"} to Google Drive account
                                    <a> {googleLogin ? googleEmail : loginWithGoogle}</a>
                                </p>
                                {googleLogin ?
                                <div className="action__connection">
                                    <a onClick={this.handleGoogleSync}>Refresh</a>
                                        <span className="pipe__divider">|</span>
                                    <a href={Routing.generate('logout_google')}>Disconnect</a>
                                </div>
                                : 
                                ''
                                }
                                {error&&<p className="help is-danger">{error}</p>}
                            </div>
                        </div>
                    </div>
                </div>
                {/* <div className="section__footer">
                    <div className="add__connection_link"><a>Add a Connection</a></div>
                    <div className="quick__action">
                        <a href={Routing.generate('connect_google')}
                        onClick={this.handleGoogleLogin}
                        className="logout__link"><img src={drive} alt="" /></a>
                        <a><img src={dropbox} alt="" /></a>
                        <a><img src={box} alt="" /></a>
                        <a><img src={mark} alt="" /></a>
                    </div>
                </div> */}
            </div>
        );
    }
}


export default reduxForm({
    form: 'AsideConnectionList',
})(AsideConnectionList);
