import React, { Component } from 'react';
import { reduxForm, SubmissionError } from 'redux-form';
import { fileTypes } from '../../components/GridView/common';
import tilePreview from '../../../../public/images/tile-preview.png';
import editIcon from '../../../../public/images/edit.svg';
import editPhoto from '../../../../public/images/editphoto.svg';

class ShowTileModal extends Component {
    
    handleOnSubmitAddTile = async (values) => {
        const {
          onAddTile,
          handleOnClick,
        } = this.props;
    
        await onAddTile(values
          ).then(() => {
            handleOnClick();
          }).catch(({ message: _error = "" }) => {
            throw new SubmissionError({ _error });
          });
      };
    
    render() {
        const {
            Tile,
            onAddTile,
        } = this.props;
        
        return(
            <div className="tile__upload_preview">
                <div className="block__left">
                    <img src={tilePreview} alt=""/>
                    <div className="icon__bottom edit__photo"> <img src={editPhoto} alt=""/></div>
                </div>
                <div className="block__right">
                    <div className="tile__title">
                        Evernote Promise To Fix A Long List Of Problems...
                    </div>
                    <div className="tile_description">
                        Here are five things in technology that happened this past week and how they affect your business...
                    </div>
                    <div className="icon__bottom edit__icon"> <img src={editIcon} alt=""/></div>
                </div>
                <button onClick={this.handleOnSubmitAddTile}>Click</button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'ShowTileModal',
  })(ShowTileModal);
  