import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
// import { NotificationManager } from 'react-notifications';
import isEmpty from 'lodash/isEmpty';
import Hubs from './Hub/Hubs';
import TagList from './TagList';
import AddHub from './Hub/addHub';
import logoIcon from "../../../public/images/Gruup-Red.svg"
import homeIcon from "../../../public/images/sidebar/home.svg";
import notificationIcon from "../../../public/images/sidebar/notification.svg";
import connectionIcon from "../../../public/images/sidebar/cloud-computing.svg";
import rightIcon from "../../../public/images/sidebar/right-arrow.svg";
import iconbar from "../../../public/images/iconbar.png";
import AsideConnectionList from "./TilesHeader/AsideConnectionList";
import AsideNotificationList from "./TilesHeader/AsideNotificationList";
import bufferIcon from "../../../public/images/sidebar/buffer.svg";
import iconplus from "../../../public/images/sidebar/icon-plus.svg";
import tags from "../../../public/images/sidebar/tags.svg";
import iconMobile from "../../../public/images/G_icon_Red.svg";
import iconClose from "../../../public/images/sidebar/closeWhite.png";
import {getListType} from '../actions/pagination';

const {
    isSharedHub,
} = __global__;

class AsideMenuList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hubLoaded: false,
            tagLoaded: false,
            isVisible: false,
            sortName: 'all',
            currentHub: {},
            isShowConnectionSidebar:false,
            isShowNotificationSidebar:false,
        };
        this.handleOnClickAddHub = this.handleOnClickAddHub.bind(this);
        this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
        this.handleConnectionSidebar = this.handleConnectionSidebar.bind(this);
        this.handleCloseMobileSidebar = this.handleCloseMobileSidebar.bind(this);
        this.handleNotificationSidebar = this.handleNotificationSidebar.bind(this);
        this.handleHomeClick=this.handleHomeClick.bind(this);
        this.handleHubsIcon = this.handleHubsIcon.bind(this);
        this.handleConnectionIcon = this.handleConnectionIcon.bind(this);
        this.handleTagsClick = this.handleTagsClick.bind(this);
    }

    componentDidMount() {
        const {
            onGetAllHubs,
            onGetAllTags,
            user: {
                id,
            },
            onGetUserDetails,
            setTile,
            onsetGoogleNotify,
            tile,
        } = this.props;
        onGetUserDetails();

        if (isSharedHub !== 1) {
            onGetAllHubs({ id }).then(() => {
                this.setState({
                    hubLoaded: true,
                });
            });
            onGetAllTags({ id }).then(() => {
                this.setState({
                    tagLoaded: true,
                });
            });
        } else {
            this.setState({
                hubLoaded: true,
            });
        }
    }
    handleHubsIcon(e){
        e.preventDefault();
        const { onSetTile, isOpenSidebar } = this.props;
        onSetTile({ isOpenSidebar: false });

    }
    handleHomeClick(){
        event.preventDefault();
        const listType = getListType();
        const {
            getAllTiles,
            user: {
                id,
            } = {},
            onSetTile,
            history,
            setSearch,
            onResetOffset,
            isOpenSidebar
        } = this.props;
          
        onSetTile({ isOpenSidebar: false });
        this.handleActiveTab("home");
        setSearch({isSearchView: false, searchText: ""});
        if (listType==='tile') {
            const wrapper = document.querySelector(".content-wrap")||{};
            wrapper.scrollTop = 0;
            getAllTiles({id}).then(() => {
                onSetTile({
                    tileLoaded: true
                })
            });
        }
        
        onResetOffset();
        const url = Routing.generate('welcome_home');
        history.push(url);
        this.setState({isShowConnectionSidebar : false})

    }

    handleConnectionSidebar() {
        const {
            isShowConnectionSidebar
        } = this.state;
        this.setState({
            isShowConnectionSidebar: !isShowConnectionSidebar,
            isShowNotificationSidebar:false,
        });
        const { onSetTile, isOpenSidebar, activeTabName } = this.props;
        onSetTile({ isOpenSidebar: false });

        // if(activeTabName !== "connect" ) {
        //     this.activeTabName = activeTabName;
        // }

        // const tabName = !isShowConnectionSidebar? "connect": this.activeTabName;
        // this.handleActiveTab(tabName);
    };
    handleConnectionIcon(){
        const {
            isShowConnectionSidebar
        } = this.state;

        const { onSetTile, isOpenSidebar, activeTabName } = this.props;
        onSetTile({ isOpenSidebar: false });
        setTimeout(() => {
            this.setState({
                isShowConnectionSidebar: !isShowConnectionSidebar
            });
        },300);
        
    };
    handleNotificationSidebar() {
        const {
            isShowNotificationSidebar,
        } = this.state;
        this.setState({
            isShowConnectionSidebar:false,
            isShowNotificationSidebar: !isShowNotificationSidebar,
        });
        
        
        const { onSetTile, isOpenSidebar } = this.props;
        onSetTile({ isOpenSidebar: false });
        
        
    };
    
    notAllowedAddHubs() {
        // NotificationManager.warning('You’ve reached the maximum number of Hubs allowed for a free account. Upgrade your account for more.', '', 5000);
    }
    
    handleOnClickAddHub() {
        const { isVisible } = this.state;
        this.setState({
            isVisible: !isVisible,
            currentHub: {},
        });
    }
    
    handleToggleSidebar(e) {
        e.preventDefault();
        const { onSetTile, isOpenSidebar } = this.props;
        onSetTile({ isOpenSidebar: !isOpenSidebar });
        
        this.setState({
            isShowConnectionSidebar: false,
            isShowNotificationSidebar:false
        });
        
    }

    handleCloseMobileSidebar(){
        const { onSetTile } = this.props;
        onSetTile({ isShowMobileSidebar: false });
        this.setState({isShowConnectionSidebar : false});
        
    }

    handleActiveTab = (tabName) => {
        const { setActiveTab } = this.props;
        setActiveTab(tabName);
    }

    handleTagsClick(){
        const { onSetTile } = this.props;
        onSetTile({ isOpenSidebar: false });
    }
   
    render() {
        const {
            hub: { hubs = [], firstHub } = {},
            tag = {},
            onAddEditHub,
            account: {
                changeClass,
                data,
            },
            history,
            setTag,
            setHub,
            setHubDropdown,
            isOpenSidebar,
            listType,
            currentHubId,
            currentTagId,
            user,
            isActiveElement,
            onToggleSelectMode,
            onGetUserDetails,
            setSearch,
            onSetTile,
            onSetGoogleNotify,
            tile,
            getAllTiles,
            setDriveData,
            activeTabName,
        } = this.props;
        
        const {
            hubLoaded,
            isVisible,
            currentHub,
            sortName,
            isShowConnectionSidebar,
            isShowNotificationSidebar,
            onSetSearch,
        } = this.state;
    
        if (isEmpty(hubs) && hubLoaded === false) {
            return (
                <Fragment>
                    <div className="sidebar-wrap">
                        <div className="full-menu">
                            <div className="sidebar-up-block">
                                <div className="sidebar-toparea">
                                    <p className="is-size-7 has-text-centered">Fetching your Hubs...</p>
                                </div>
                            </div>
                        </div>
                        <p className="has-text-centered">Loading</p>
                    </div>
                </Fragment>
            );
        }
        
        const ownHubs = hubs.filter(({ email }) => email === user.email);
        const maxHubs = (ownHubs.length >= __global__.maxHub) && (user.accountType === 'FREE');
        const connectionClick = !isOpenSidebar ? this.handleConnectionSidebar : this.handleConnectionIcon;
        return (
            <Fragment>

                {isVisible
                    && (
                        <AddHub
                        isVisible={isVisible}
                            handleOnClick={this.handleOnClickAddHub}
                            onAddEditHub={onAddEditHub}
                            currentHub={currentHub}
                            isEdit={Boolean(currentHub.id)}
                        />
                    )
                }
                <div className='sidebar_block'>
                    <div className="brand_top web__view">
                        <div className="logo_wrap">
                            <a onClick={this.handleHomeClick}><img src={logoIcon} alt="" className='sidebar__logo' /></a>
                            <a><img src={iconMobile} alt="" className='sidebar-minimize__logo' onClick={this.handleToggleSidebar} /></a>

                        </div>
                        <div className="burger_icon" onClick={this.handleToggleSidebar}>
                            <img src={iconbar} alt="" />
                        </div>
                    </div>
                    <div className="brand_top mobile__view">
                        <div className="logo_wrap">
                            <a><img src={ iconMobile } alt="" className='mobile_gruup_logo'/></a>
                            <a  href="/logout">Logout</a>
                        </div>
                        <div className="burger_icon"  onClick={this.handleCloseMobileSidebar}>
                            <img src={iconClose} alt="" />
                        </div>
                    </div>
                    <div className="list__block">
                        <div className={`list_row ${activeTabName === "home" && isSharedHub!==1 ? 'active__element' : '' }`}>
                            <a className="list__grid" onClick={this.handleHomeClick}>
                                <span className="wrap_block"><img src={homeIcon} className="icon__list" />
                                <span className="list__caption">Home</span></span>
                            </a>
                        </div>
                        {/* <div className="list_row">
                            <a className="list__grid" onClick={this.handleNotificationSidebar}>
                                <span className="wrap_block"><img src={notificationIcon} className="icon__list"/>
                                <span className="list__caption">Notifications</span></span>
                                <span className="wrap_block"><img src={rightIcon} className="icon__list" /></span>
                            </a>
                          
                        </div> */}
                        <div className={`list_row ${isShowConnectionSidebar ? 'active__element' : ''}`}>
                            <a className="list__grid" onClick={connectionClick}>
                                <span className="wrap_block"><img src={connectionIcon} className="icon__list"/>
                                <span className="list__caption">Connections</span></span>
                                <span className="wrap_block arrow__connection"><img src={rightIcon} className="icon__list"/></span>
                            </a>
                        </div>
                           
                    </div>
                    <hr className="gray display__no_mobile" />

                    <div className="hub__section">
                        <div className="list__block">
                        
                            <div className={`list_row ${maxHubs ? 'inactive' : ''}`}>
                                <a className="list__grid">
                                    <span className="wrap_block">
                                        <img src={bufferIcon} className="icon__list" onClick={this.handleHubsIcon} />
                                        <span className="list__caption">HUBS</span>
                                    </span>
                                    <span className="wrap_block button__add_hub" onClick={maxHubs ? this.notAllowedAddHubs : this.handleOnClickAddHub}>
                                        <img src={iconplus} alt="" />
                                        Add Hub
                                    </span>
                                </a>
                            </div>
                           </div>
                           <div className="list__block">
                                        {(!isEmpty(hubs) && hubLoaded === true) && (
                                            <Hubs
                                                handleCloseMobileSidebar={this.handleCloseMobileSidebar}
                                                hubs={hubs}
                                                listType={listType}
                                                currentHubId={currentHubId}
                                                setHub={setHub}
                                                setHubDropdown={setHubDropdown}
                                                history={history}
                                                sortName={sortName}
                                                onToggleSelectMode={onToggleSelectMode}
                                                onActiveTab={this.handleActiveTab}
                                                activeTabName={activeTabName}
                                            />)
                                        }                                        
                        </div>
                    </div>

                    <div className="tag__section">
                        <hr className="gray" />
                        <div className="list__block">
                            <div className="list_row">
                                <a className="list__grid">
                                    <span className="wrap_block">
                                        <img src={tags} className="icon__list"  onClick={this.handleTagsClick}/>
                                        <span className="list__caption">TAGS</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div className="tag_pill__wrap">
                            
                                {isSharedHub !== 1 && (
                                    <TagList
                                        handleCloseMobileSidebar = {this.handleCloseMobileSidebar}
                                        history={history}
                                        setTag={setTag}
                                        tags={tag.tags}
                                        currentTagId={currentTagId}
                                        onToggleSelectMode={onToggleSelectMode}
                                    />
                                )
                                }
                            
                        </div>
                    </div>
                </div>
                {isShowConnectionSidebar &&
                    <AsideConnectionList
                        isShowConnectionSidebar={isShowConnectionSidebar}
                        handleOnClick={this.handleConnectionSidebar}
                        data={data}
                        setSearch={setSearch}
                        setTile={onSetTile}
                        tile={tile}
                        onSetGoogleNotify={onSetGoogleNotify}
                        setDriveData={setDriveData}
                        getAllTiles={getAllTiles}
                        handleHomeClick={this.handleHomeClick}
                        user={user}
                        />
                }

                {isShowNotificationSidebar &&
                    <AsideNotificationList
                        isShowNotificationSidebar={isShowNotificationSidebar}
                        handleOnClick={this.handleNotificationSidebar}
                    />
                }
             
            </Fragment>
        );
    }
}

AsideMenuList.propTypes = {
    onGetAllHubs: PropTypes.func.isRequired,
    onGetAllTags: PropTypes.func.isRequired,
    onAddEditHub: PropTypes.func.isRequired,
    user: PropTypes.shape({
        id: PropTypes.string.isRequired,
    }).isRequired,
    onSetTile: PropTypes.func.isRequired,
    isOpenSidebar: PropTypes.bool.isRequired,
    hub: PropTypes.shape({
        hubs: PropTypes.array.isRequired,
        firstHub: PropTypes.bool.isRequired,
    }).isRequired,
    tag: PropTypes.object.isRequired,
    account: PropTypes.shape({
        changeClass: PropTypes.bool.isRequired,
    }).isRequired,
    history: PropTypes.object.isRequired,
    setTag: PropTypes.func.isRequired,
    setHub: PropTypes.func.isRequired,
    setHubDropdown: PropTypes.func.isRequired,
    listType: PropTypes.string,
    currentHubId: PropTypes.string,
};

export default AsideMenuList;
