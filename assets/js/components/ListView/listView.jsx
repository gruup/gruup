import React, { PureComponent } from 'react';

import TableHeader from './listTable/tableHeader';
import TableRow from './listTable/tableRow';

class ListView extends PureComponent {
  render() {
    const {
      tile: {
        data = {},
        listField = [],
        isSelectMode,
        selectedTile = [],
      } = {},
      ...ownProps
    } = this.props;

    return (
      <div className="list-layout">
        <div className="panel-block">
          <table className="table  pricing__table">

            <TableHeader {...this.props} />
            <tbody>
              {Object.values(data).map((t, key) => (
                <TableRow
                  key={key}
                  tile={t}
                  listField={listField}
                  isSelectMode={isSelectMode}
                  selectedTile={selectedTile}
                  {...ownProps}
                />
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ListView;
