import React, { Component, Fragment } from 'react';
import onClickOutside from "react-onclickoutside";
import truncate from 'lodash/truncate';
import moreIcon from '../../../../../public/images/navbar/showMoreButton.svg';
class RowCell extends Component {

  render() {

    const { field, row } = this.props;
    let view = row[field.mappedKey];

    if(field.isLink) {
      view = <a download target="_blank" href={row[field.mappedKey]}><i className="fa fa-download" /></a>
    }

    return(
      <td className={field.className ? field.className: '' }>{view}</td>
    );
  }
}

class TableRow extends Component {
    state = {
      isShowPopup: false,
      isShowDetails: false
    };

    handleClickOutside = () => {
        const { isShowPopup } = this.state;
        if(isShowPopup) {
          this.setState({
            isShowPopup: !isShowPopup
          });
        }
    }

    showDetails = () => {
      const { isShowDetails } = this.state;
      const { onSelectedHubLength, tile, onSetTileDetailModal } = this.props;
      onSelectedHubLength(tile.tileId);
      onSetTileDetailModal({ isOpen: true, tileId: tile.tileId });
    }

    handleOnShowDropDown = () => {
      const { isShowPopup } = this.state;
      this.setState({
        isShowPopup: !isShowPopup
      });
    }

    handleSelect = (event) => {
      const {
        tile: {
          tileId = null,
          hash = null,
          id = null
        },
        onSelectTile
      } = this.props;

      const selectTileId = tileId || hash || id;
      onSelectTile(selectTileId);
    }

    render() {

      const { isShowDetails, isShowPopup } = this.state;
      const {
        tile,
        tile: {
          tileId = null,
          hash = null,
          id = null
        },
        handleOnClickAddTileToTag,
        handleOnClickAddTileToHub,
        hubCount,
        listField = [],
        isSelectMode,
        selectedTile
      } = this.props;
      const selectTileId = tileId || hash || id;
      const isSelectedTile = selectedTile.some((i) => i === selectTileId);

      let icon = '/images/google-drive-icon.svg';
      let iconClass = 'favicon';
      if(tile.source === 'URL'){
        iconClass = 'icon-tableRow';
        icon = tile.favicon;
      }
      if(tile.fileType === 'spreadsheet' || tile.fileExt === 'xlsx' || tile.fileExt === '.xlsx')
      {
        icon = "/images/xlsx-icon.png";
      }
      else if(tile.fileType === 'pdf' || tile.fileExt === '.pdf'){
        icon = "/images/pdf-icon.png";
      }

      return (
        <Fragment>
          <tr>
            {
              isSelectMode &&
              <td>
                <input
                  type="checkbox"
                  value={isSelectedTile}
                  onChange={this.handleSelect}
                />
              </td>
            }
            <td><img src={tile.favicon ? tile.favicon : icon} className={iconClass} alt=""/></td>
            <td className="tile-name-ellip">
              {tile.source == 'URL' ?
                <span className="with-link"><a href={tile.path} target="_blank">
                  {tile.name}
                </a></span>
              :
                <span className="without-link">{tile.name}</span>
              }
                <img src={moreIcon} alt="" className="list-mobile-more  navbar-end" onClick={this.handleOnShowDropDown}/>
                <div className={isShowPopup ? "box dropdown dropdown-author-mobile is-open" : "box dropdown dropdown-author-mobile"}>
                  <ul>
                    <li className="active"><span className="nav-item">Author: {tile.owner}</span></li>
                    <li><span className="nav-item">File Upload date: {tile.created}</span></li>
                    <li><span className="nav-item">Size: {tile.fileSize}</span></li>
                    <li>
                      <span className="nav-item" onClick={this.showDetails}>
                        <i className="fa fa-eye" />
                      </span>
                      <span className="nav-item">
                      {
                        tile.fileType === 'URL' ?
                        <a href={tile.path} target="_blank">
                        <i className="fa fa-link" />
                        </a>
                        :
                        <a href={tile.filePath} download target="_blank">
                          <i className="fa fa-download" />
                        </a>
                      }
                      </span>
                    </li>
                  </ul>
                </div>
            </td>
            {
              listField.map((field, key) =>
                <RowCell
                  key={key}
                  row={tile}
                  field={field}
                />
              )
            }
            <td className="table-action" onClick={this.showDetails}><i className="fa fa-eye" /></td>
          </tr>
        </Fragment>
      );
    }
}

export default onClickOutside(TableRow);
