import React, { Component } from 'react';
import HeaderCell from './headerCell';

class TableHeader extends Component {
  state = {
    isOpen: false
  };

  handleOnClick = () => {
    const { isOpen } = this.state;

    let screenWidth = window.innerWidth;

    if(screenWidth >= 768 && screenWidth <= 1024) {
      this.setState({
        isOpen: !isOpen
      })
    }
    else {
      return null;
    }
  }
  render() {
    const { isOpen } = this.state;
    const {
      tile: {
        listField = [],
        remainField = [],
        isSelectMode = false
      } = {},
      onSetColumn
    } = this.props;

    return (
      <thead>
        <tr>
          {
            isSelectMode &&
              <th>#</th>
          }
          <th>File</th>
          <th>Name</th>
          {
            listField.map((field, key) =>
              <HeaderCell
                key={`${field.name}-${key}`}
                field={field}
                remainField={listField}
                isOpen={isOpen}
                onSwitchColumn={this.handleOnClick}
                onSetColumn={onSetColumn}
              />
            )
          }
          <th>Details</th>
        </tr>
      </thead>
    );
  }
}

export default TableHeader;
