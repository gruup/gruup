import React, { PureComponent } from 'react';

const RemainField = ({ field = {}, index, onSelect }) => {

  const handleClick = (e) => {
    e.preventDefault();
    if(typeof onSelect === "function") {
      onSelect(field, index);
    }
  }

  if(field.isTabletMode) {
    return null;
  }

  return (
    <li onClick={handleClick}>
      <span className="nav-item">{field.name}</span>
    </li>
  );
}

class HeaderCell extends PureComponent {

  handleSelectCell = (data, index) => {
    const { onSetColumn, onSwitchColumn } = this.props;
    onSetColumn(data, index);
    onSwitchColumn();
  }

  render() {
    const {
      onSwitchColumn,
      isOpen = false,
      field = {},
      remainField = [],
    } = this.props;

    if(!field.isTabletMode) {
      return <th>{field.name}</th>
    }

    return (
      <th>
        <div className="control">
          <div className="button-author" onClick={onSwitchColumn}>{field.name}</div>
          <div className={isOpen ? "box dropdown dropdown-author is-open" : "box dropdown dropdown-author"}>
            <ul>
              {
                remainField.map((cell, key) =>
                  <RemainField
                    key={key}
                    field={cell}
                    index={key}
                    onSelect={this.handleSelectCell}
                  />
                )
              }
            </ul>
          </div>
        </div>
      </th>
    )
  }
}

export default HeaderCell;
