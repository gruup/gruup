
import React, {Component, Fragment} from 'react';
import AddTile from "./TilesHeader/addTile";
import addTileIcon from "../../../public/images/sidebar/addTileMobile.svg";
class AddTileMobile extends Component {

    constructor(props) {
    super(props);
    this.state = {
        isVisible: false,
    };
    }

    handleOnClickAddTile = (e) => {
    const {onSetUser} = this.props;
    onSetUser({ isTilePopupActive: false });
    };

    render() {
    const {
        onAddTile,
        isTilePopupActive,
        handleAddTileClick,
        tile:{
            addTile
        }
    } = this.props;
    return (
    <Fragment>
   {
    addTile
     ?           
    <div className="add_tile_icon__global" onClick={handleAddTileClick}>
    <a><img src={addTileIcon} alt=""/></a>
    </div>
    :
    ''
   }
        {isTilePopupActive &&
            <AddTile
            isVisible={isTilePopupActive}
            handleOnClick={this.handleOnClickAddTile}
            onAddTile={onAddTile}
            />
        }
        </Fragment>
    );
    }
}

export default AddTileMobile;