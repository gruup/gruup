import React, { Component, Fragment } from 'react';
import isEmpty from 'lodash/isEmpty';
import TilesHeader from "./TilesHeader";
import GridView from "./GridView";
import ListView from "./ListView";
import Infinite from "react-infinite-scroller";

import TileDetailModal from '../containers/tileDetailModal';
import Modals from '../containers/modals';
import { getListType } from '../actions/pagination';
import HubDropDown from './HubDropDown';
import AddTileMobile from './AddTileMobile';

const {
  isSharedHub,
  hash,
  email
} = __global__;

class SectionMainDashboard extends Component {

  constructor(props){
    super(props);
    const {
      onToggleSelectMode,
    } = props;

    const isReset = true;
    onToggleSelectMode(isReset);

    this.handleGoogleSync = this.handleGoogleSync.bind(this);
    this.handleOnClickAddTileToHub = this.handleOnClickAddTileToHub.bind(this);
    this.handleOnClickAddTileToTag = this.handleOnClickAddTileToTag.bind(this);
    this.handleOnSubmitAddTileToTag = this.handleOnSubmitAddTileToTag.bind(this);
    this.changeView = this.changeView.bind(this);
    this.handleAddTileClick = this.handleAddTileClick.bind(this);
    this.handleAddTileMobileClick = this.handleAddTileMobileClick.bind(this);
  }

  componentDidMount() {
    const {
      onGetAllTiles,
      user: {
        id
      },
      match: {
        params: {
          hubId: hubId = '',
          tagId: tagId = ''
        } = {}
      } = {},
      location = {},
      getHubByHash,
      getQueryParam,
      getParameterByName,
      setTile
    } = this.props;


    const search = getParameterByName('q', location.search);

    if(isSharedHub === 1 && hash &&  email) {
      getHubByHash(hash, email);
    } else if(search) {
      getQueryParam(search);
    } else {
      onGetAllTiles({ id, hubId, tagId, isPagination: false }).then(() => {
        setTile({
          tileLoaded: true
        })
      });
    }
  }

  componentDidUpdate({match: {
        params: {
          hubId: prevHubId = '',
          tagId: prevTagId = ''
        } = {}
      } = {}}
  ) {
    const {
      onGetAllTiles,
      user: {
        id,
      },
      match: {
        params: {
          hubId = '',
          tagId = ''
        } = {}
      } = {},
      setTile
    } = this.props;
    if (prevHubId !== hubId || prevTagId !== tagId) {
      setTile({tileLoaded: false});
        onGetAllTiles({ id, hubId, tagId, isPagination: false }).then(() => {
        setTile({
          tileLoaded: true
        })
      });
    }
  }

  handleGoogleSync() {
    const body = {
      limit: 500
    };
    const url = window.location.origin + Routing.generate('get_drive');
    this.worker.postMessage({ url, body });
  }

  handleOnClickAddTileToHub(tileId) {
    const {
      onSetHubModal,
      onResetHubModal,
      postSelectedTile
    } = this.props;

    if(tileId) {
      const params = {
        isOpen: true,
        isMultiSelect: false,
        onSubmit: postSelectedTile,
        tileId,
        params: {
          type: 'hubIds',
          selectedTileId: tileId
        }
      };
      onSetHubModal(params);
    } else {
      onResetHubModal();
    }
  };

  handleOnClickAddTileToTag(tileId) {
    const {
      onSetTagModal,
      onResetTagModal
    } = this.props;

    if(tileId) {
      const params = {
        isOpen: true,
        isMultiSelect: false,
        onSubmit: this.handleOnSubmitAddTileToTag,
        tileId
      };
      onSetTagModal(params);
    } else {
      onResetTagModal();
    }
  };

  async handleOnSubmitAddTileToTag(values, { tileId }) {
    const {
      user: {
        id
      },
      onAddTileToTag,
      match: { params: { tagId } = {} } = {},
      onResetTagModal,
      history
    } = this.props;

    await onAddTileToTag({
        id,
        tileId,
        tags: { tiles: values },
        tagId
      }, history).then(() => {
        onResetTagModal();
      });
  };

  changeView() {
    const { tile: { isChangeView }, setTile } = this.props;
    setTile({ isChangeView: !isChangeView })
  }

  handleAddTileClick() {
    const { onSetUser } = this.props;
    onSetUser({ isTilePopupActive: true });
  }
  handleAddTileMobileClick(){
    const { onSetUser } = this.props;
    onSetUser({ isTilePopupActive: true });
  }

  render() {
    const {
      tile,
      onGetSelectedHub,
      onAddNewTag,
      loadMoreOnScroll,
      onSelectedHubLength,
      onSetTileDetailModal,
      onDeleteTile,
      user: {
        isTilePopupActive=false
      },
      tile: {
        hubCount,
        gNotify,
        hubDropdown = {},
        isChangeView = false,
        tileLoaded = false
      },
      onSetSearch,
    
    } = this.props;
    const view = this.props[getListType()];
    const hasMore = ((view.total + view.limit) > view.offset) && !tile.isLoading && !tile.isPagination;

    if (isEmpty(tile) && tileLoaded === false) {
      return (
        <Fragment>
          <p className="is-size-7">Loading</p>
        </Fragment>
      );
    }

    if (isEmpty(tile) && tileLoaded === true) {
      return (
        <section className="hero is-info welcome is-small">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">
                Hello
              </h1>
              <h2 className="subtitle is-size-7">
                Looks like you haven't added any tiles yet. Get started <a href={Routing.generate('tile_new')} className="is-link">here</a>.
              </h2>
            </div>
          </div>
        </section>
      );
    }
    return (
      <Fragment>

        <div className="content-wrap">
          <Infinite
            loadMore={loadMoreOnScroll}
            hasMore={hasMore}
            pageStart={0}
            useWindow={false}
          >
            <div className="tiles-wrap">

              <TilesHeader
                {...this.props}
                isTilePopupActive={isTilePopupActive}
                handleAddTileClick={this.handleAddTileClick}  
                onclick={this.changeView}
                getListType={getListType}
              />
              {isChangeView ?
                <ListView
                  {...this.props}
                  handleOnClickAddTileToTag={this.handleOnClickAddTileToTag}
                  handleOnClickAddTileToHub={this.handleOnClickAddTileToHub}
                  hubCount={hubCount}
                  onSelectedHubLength={onSelectedHubLength}
                  onSetTileDetailModal={onSetTileDetailModal}
                  getListType={getListType}
                />
                :
                <GridView
                  {...this.props}
                  handleAddTileClick={this.handleAddTileClick}                  
                  handleOnClickAddTileToTag={this.handleOnClickAddTileToTag}
                  handleOnClickAddTileToHub={this.handleOnClickAddTileToHub}
                  onGetSelectedHub={onGetSelectedHub}
                  onDeleteTile={onDeleteTile}
                  hubCount={hubCount}
                  onSelectedHubLength={onSelectedHubLength}
                  onSetTileDetailModal={onSetTileDetailModal}
                  getListType={getListType}
                />
              }
              {
                tile.isLoading &&
                <div className="loading-block loading-ani">
                    Loading
                    <span>.</span>
                    <span>.</span>
                    <span>.</span>
                </div>
              }
            </div>
          </Infinite>

        </div>
        <TileDetailModal
          handleOnClickAddTileToTag={this.handleOnClickAddTileToTag}
          handleOnClickAddTileToHub={this.handleOnClickAddTileToHub}
          hubCount={hubCount}
        />
        <Modals
          onAddNewTag={onAddNewTag}
        />

        {
          hubDropdown.isOpen &&
          <HubDropDown
            {...this.props}
            {...hubDropdown}
          />
        }
        {
          gNotify.isOpen &&
          <div className="g-notify">
            {gNotify.message}
          </div>
        }
        <AddTileMobile 
         {...this.props}
           isTilePopupActive={isTilePopupActive}
           handleAddTileClick={this.handleAddTileMobileClick} 
        />
      </Fragment>
    )
  }
}

export default SectionMainDashboard;
