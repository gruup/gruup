import React, {Component, Fragment} from 'react';
import onClickOutside from "react-onclickoutside";
import DeleteHub from "./Hub/deleteHub";
import ShareHub from "./Hub/shareHub";
import CreateCopy from "./Hub/createCopy";
import AddTag from "./Hub/addTag";
import AddHub from "./Hub/addHub";
import iconClose from "../../../public/images/sidebar/red_close.svg";

class HubDropDown extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isDelete: false,
      isCreateCopy: false,
      isAddTag: false,
      isVisible: false,
      isManageHubUser: false
    };

    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.handleAddTag = this.handleAddTag.bind(this);
    this.handleOnDeleteHub = this.handleOnDeleteHub.bind(this);
    this.handleCreateCopy = this.handleCreateCopy.bind(this);
    this.handleEditHub = this.handleEditHub.bind(this);
    this.handleMangeHubUser = this.handleMangeHubUser.bind(this);
  };

  handleClickOutside() {
    const { setHubDropdown } = this.props;
    setHubDropdown({ isOpen: false, item: {} });
  };

  handleAddTag() {
    const {isAddTag} = this.state;
    this.setState({
      isAddTag: !isAddTag
    });
    if(isAddTag) {
      this.handleClickOutside();
    }
  };

  handleOnDeleteHub() {
    const {isDelete} = this.state;
    this.setState({
      isDelete: !isDelete
    });
    if(isDelete) {
      this.handleClickOutside();
    }
  };

  handleCreateCopy() {
    const {isCreateCopy} = this.state;
    this.setState({
      isCreateCopy: !isCreateCopy
    });
    if(isCreateCopy) {
      this.handleClickOutside();
    }
  };

  handleEditHub() {
    const {isVisible} = this.state;
    this.setState({
      isVisible: !isVisible
    });
    if(isVisible) {
      this.handleClickOutside();
    }
  }

  handleMangeHubUser() {
    const { isManageHubUser } = this.state;
    const { onSetHubState } = this.props;
    this.setState({
      isManageHubUser: !isManageHubUser
    });
    if(isManageHubUser) {
      this.handleClickOutside();
      onSetHubState({ hubUsers: [] });
    }
  }

  render() {
    const {
      isDelete,
      isCreateCopy,
      isAddTag,
      isVisible,
      isManageHubUser
    } = this.state;

    const {
      hub: {
        hubUsers = [],
        isHubUserLoading,
        hubs
      } = {},
      item,
      onDeleteHub,
      onShareHub,
      onCreateCopy,
      onAddTag,
      tag,
      getHubSelectedTag,
      onAddNewTag,
      onAddEditHub,
      top = -100,
      left = -100,
      onRemoveUserAtSharedHub,
      onGetHubUsers,
      user,
    } = this.props;

    return (
      <div>

        {isVisible &&
          <AddHub
            isVisible={isVisible}
            handleOnClick={this.handleEditHub}
            onAddEditHub={onAddEditHub}
            currentHub={item}
            isEdit={Boolean(item.id)}
          />
        }

        {
          isAddTag &&
          <AddTag
            hubId={item.id}
            handleOnClick={this.handleAddTag}
            isAddTag={isAddTag}
            data={tag.tags}
            onAddTag={onAddTag}
            getHubSelectedTag={getHubSelectedTag}
            onAddNewTag={onAddNewTag}
          />
        }
        {
          isDelete &&
          <DeleteHub
            item={item}
            handleOnClick={this.handleOnDeleteHub}
            isDelete={isDelete}
            onDeleteHub={onDeleteHub}
          />
        }
        {
          isManageHubUser &&
          <ShareHub
            item={item}
            handleOnClick={this.handleMangeHubUser}
            isOpen={isManageHubUser}
            onShareHub={onShareHub}
            isManageUser={true}
            list={hubUsers}
            onRemoveUserAtSharedHub={onRemoveUserAtSharedHub}
            onGetHubUsers={onGetHubUsers}
            isHubUserLoading={isHubUserLoading}
          />
        }

        {
          isCreateCopy &&
          <CreateCopy
            user={user}
            item={item}
            handleOnClick={this.handleCreateCopy}
            isCreateCopy={isCreateCopy}
            onCreateCopy={onCreateCopy}
            hubs={hubs}
          />
        }
        <div className="dropdown is-open hub-open-setting" style={{ top, left }}>
          <ul>
            <li>
              <span className="nav-item" onClick={this.handleAddTag}>Add Tag </span>
            </li>
            <li><span className="nav-item" onClick={this.handleCreateCopy}>Duplicate Hub</span></li>
           {
              item.hubType !== 'private' && item.isDelete &&
              <li><span className="nav-item" onClick={this.handleMangeHubUser}>Hub users</span></li>
            }
            { item.isDelete &&
              <Fragment>
                <li><span className="nav-item" onClick={this.handleEditHub}>Information</span></li>
              </Fragment>
            }
            <hr className="gray"/>
            <li className="delete__element">
              <img src={ iconClose } alt="" />
              <span className="nav-item" onClick={this.handleOnDeleteHub}>Delete a Hub</span></li>
           
          </ul>
        </div>
      </div>
    );
  }
}

export default onClickOutside(HubDropDown);
