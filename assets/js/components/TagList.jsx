import React, {Component, Fragment} from 'react';
import TagIcon from "../../../public/images/tag@3x.png";
import Tag from "./Tag";

class TagList extends Component {

  constructor(props) {
    super(props);
    this.handleTag = this.handleTag.bind(this);
  }

  handleTag(tagId) {
    const {
      history,
      setTag,
      onToggleSelectMode,
      currentTagId
    } = this.props;
    if(tagId === currentTagId) {
      return true
    }

    onToggleSelectMode(true);
    setTag({offset: 0});
    const url = Routing.generate('view_tag_details', {tagId});
    history.push(url);
  };

  render() {
    const {tags = [],
      handleCloseMobileSidebar
    } = this.props;
    return (
      
      <Fragment>
            {tags.map((tag, key) =>
              <Tag
                handleCloseMobileSidebar={handleCloseMobileSidebar}
                key={`${key}-${tag.id}`}
                tag={tag}
                onClick={this.handleTag}
              />
            )}
      
        </Fragment>
    );
  };
}

export default TagList;
