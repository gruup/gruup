import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Select from 'react-select';

const renderMultiselect = ({ input, options= [] }) => {

    const handleChange = (value) => {
      input.onChange(value);
    }

    return (
      <Select.Creatable
        onBlur={() => input.onBlur()}
        autoFocus={true}
        multi={true}
        value={input.value || []}
        valueArray={input.value || []}
        onChange={handleChange}
        isSearchable={true}
        name={input.name}
        options={options}
        showNewOptionAtTop={true}
        placeholder={'Select tag'}
      />
    )
};

class TagModal extends Component {

  constructor(props) {
    super(props);
    const {
      data = []
    } = props;
    this.options = data.map(({ id, name }) => ({ value: id, label: name })) || [];
    this.getSelectedTag();
    this.state = {
      containerClassName: '',
      searchTerm: ''
    }
  }

  getSelectedTag = async () => {
    const {
      actions: { onGetSelectedTag },
      initialize,
      data = {},
      tagModalData: {
        tileId = null,
        isMultiSelect = false
      }
    } = this.props;

    let filteredData = [];
    if(!isMultiSelect && tileId) {
      const { data: selectedTags = 0 } = await onGetSelectedTag(tileId) || { data: [] };

      if(selectedTags.length > 0) {
        data.map((tag) => {
          const isSelected = selectedTags.some((tagId) => tagId == tag.id);
          if(isSelected) {
            filteredData.push(tag);
          }
        })
      }
    }
    const options = filteredData.map(({ id, name }) => ({ value: id, label: name }));
    initialize({ tiles: options });
  }

  handleSubmit = ({ tiles = []}) => {
    const {
      tagModalData: {
        tileId,
        onSubmit,
        params = {}
      },
      data
    } = this.props;
    tiles = tiles.map(({ value: id, label: name, className }) => {
      if(className) {
        return { name };
      }

      return {
        id,
        name
      }
    });

    onSubmit(tiles, {...params, tileId});
  }

  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      error,
      tagModalData: {
        isOpen,
        isMultiSelect = false
      },
      data: filteredData,
      actions: {
        resetTagModal
      },
      isLoading
    } = this.props;

    // const options = filteredData.map(({ id, name }) => ({ value: id, label: name }));

    return (

      <div className={isOpen ? 'modal is-active' : 'modal'}>
        <form onSubmit={handleSubmit(this.handleSubmit)}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Add Tag</p>
              <button className="delete" type="button" aria-label="close" onClick={resetTagModal} />
            </header>
            <section className="modal-card-body add-tag-modal">
              <div>
                <Field
                  name="tiles"
                  component={renderMultiselect}
                  options={this.options}
                  isMultiSelect={isMultiSelect}
                />

                {error && <p className="help is-danger">{error}</p>}
                <br />
              </div>
            </section>
            <footer className="modal-card-foot">
              <button className={`button search-btn-modal ${submitting || isLoading ? 'is-loading' : ''}`} type="submit" disabled={pristine || submitting}>Save changes</button>
              <button className="button search-btn-modal" type="button" onClick={resetTagModal}>Cancel</button>
            </footer>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'TagModal',
})(TagModal);
