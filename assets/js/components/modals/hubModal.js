import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { DropdownList } from 'react-widgets';
import bufferIcon from '../../../../public/images/sidebar/buffer.svg';
import closeIcon from '../../../../public/images/sidebar/red_close.svg';

const listIcon = {
  private: '/images/lock.svg',
  public: '/images/group.svg',
  shared: '/images/link.svg',
  open: '/images/group.svg'
};

class HubModal extends Component {

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.listItem = this.listItem.bind(this);
  }

  handleSubmit({ tiles }) {
    const {
      hubModalData: {
        onSubmit,
        tileId,
        params = {}
      }
    } = this.props;
    tiles = [tiles];

    onSubmit(tiles, {...params, tileId});
  };

  listItem({ item }) {
    const { hubType, name } = item;
    return (<span><img src={listIcon[hubType]} height="16" width="18"/> {name}</span>)
  };

  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      error,
      data = [],
      hubModalData: {
        isOpen,
      },
      actions: {
        resetHubModal
      },
      isLoading
    } = this.props;

    const filteredData = data.filter(({ addToHub = false }) => addToHub);


    const renderMultiSelect = ({ input, data, listItem }) => (
      <DropdownList
        {...input}
        onBlur={() => input.onBlur()}
        value={input.value || ''} // requires value to be an string
        data={data}
        itemComponent={listItem}
        valueComponent={listItem}
        valueField="value"
        textField="name"
        autoFocus={true}
      />
    );

    return (
      <div className={isOpen ? 'modal is-active' : 'modal'}>
        <form onSubmit={handleSubmit(this.handleSubmit)}>
          <div className="modal-background" />
          <div className="modal-card select-hub-main">
            <div className="close-modal">
                <img src={closeIcon} onClick={resetHubModal} />
            </div>
            <section className="modal-card-body new-modal-body select-hub">
              <div className="modal-title"><img src={bufferIcon} alt=""/>Add Tile to Hub</div>
              <div className="search-modal modal-form">
                <Field
                  name="tiles"
                  component={renderMultiSelect}
                  data={filteredData}
                  listItem={this.listItem}
                />

                {error && <p className="help is-danger">{error}</p>}
                <br />
              </div>
              <div className="new-modal-bottom">
                <a href="javascript:;" className="cancel-lnk" onClick={resetHubModal}>Cancel</a>
                <button
                  className={`button submit-btn-modal ${submitting || isLoading ? 'is-loading' : ''}`}
                  type="submit" disabled={pristine || submitting}
                >
                  Add to Hub
                </button>
              </div>
            </section>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'hub-modal',
})(HubModal);
