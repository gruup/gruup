import React, { PureComponent, Fragment } from 'react';

import HubModal from './hubModal';
import TagModal from './tagModal';
import DeleteTileModal from './deleteTileModal';
import RemoveFromHub from './removeFromHub';

class Modals extends PureComponent {
  render() {
    const {
      hubModalData = {},
      tagModalData = {},
      deleteTileModalData = {},
      removeFromHubModalData = {},
      hub,
      tag,
    } = this.props;

    return (
      <Fragment>
        {
                    hubModalData.isOpen
                    && <HubModal {...this.props} data={hub.hubs} />
                }

        {
                    tagModalData.isOpen
                    && <TagModal {...this.props} data={tag.tags} />
                }

        {
                    deleteTileModalData.isOpen
                    && <DeleteTileModal {...this.props} />
                }

        {
                    removeFromHubModalData.isOpen
			        && <RemoveFromHub {...this.props} />
                }
      </Fragment>
    );
  }
}

export default Modals;
