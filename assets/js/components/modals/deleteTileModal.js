import React, { Component } from 'react';


class DeleteTileModal extends Component {
  deleteTile = () => {
    const { 
      deleteTileModalData: {
        tile = {},
        onDeleteTile,
        params,
        isMulti = false
      },
    } = this.props;
    if(isMulti) {
      onDeleteTile(params);
    } else {
      onDeleteTile(tile.tileId);
    }
  }

  render() {
    const {
      deleteTileModalData: {
        tile = {},
        isOpen,
        isMulti = false
      },
      actions: {
        resetDeleteTileModal
      },
      isLoading
    } = this.props;

    return (
      <div className={isOpen ? 'modal is-active' : 'modal'}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Delete Tile{isMulti && 's'}</p>
              <button className="delete" type="button" aria-label="close" onClick={resetDeleteTileModal} />
            </header>
            <section className="modal-card-body">
              <div className="search-modal">
              {
                tile.source == 'URL' ?
                  'Deleting this Tile will remove it from your Wall and all Hubs it may appear in.'
                :
                  'Deleting this Tile will remove it from your Wall and all Hubs it may appear in but will NOT delete the file from your cloud storage.'
              }
              </div>
            </section>
            <footer className="modal-card-foot">
              <button className={`button search-btn-modal ${isLoading ? 'is-loading' : ''}`} onClick={this.deleteTile}>
                Delete
              </button>
              <button className="button search-btn-modal"
                type="button"
                onClick={resetDeleteTileModal}
              >
                Cancel
              </button>
            </footer>
          </div>
      </div>
    );
  }
}

export default DeleteTileModal;
