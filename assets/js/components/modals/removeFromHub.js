import React, { Component } from 'react';


class RemoveFromHub extends Component {

    handleRemove = () => {
        const {
            tile: {
                id: hubId,
                selectedTile = []
            },
            removeFromHubModalData: {
                tile: { tileId } = {},
                onRemove,
                isMulti
            }
        } = this.props;

        let tileIds = selectedTile;

        if(!isMulti) {
            tileIds = [tileId];
        }

        onRemove(tileIds, hubId, isMulti);
    }
  
  render() {
    const {
        removeFromHubModalData: {
            isOpen,
            isMulti
        },
        isLoading,
        actions: {
            resetRemoveFromHubModal
        }
    } = this.props;

    return (
      <div className={isOpen ? 'modal is-active' : 'modal'}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Remove from Hub</p>
              <button className="delete" type="button" aria-label="close" onClick={resetRemoveFromHubModal} />
            </header>
            <section className="modal-card-body">
              <div className="search-modal">
                This will remove {isMulti? 'Tiles ': 'the Tile '} from this Hub but will not remove it from your Wall.
              </div>
            </section>
            <footer className="modal-card-foot">
              <button className={`button search-btn-modal ${isLoading ? 'is-loading' : ''}`} onClick={this.handleRemove}>
                Remove
              </button>
              <button className="button search-btn-modal"
                type="button"
                onClick={resetRemoveFromHubModal}
              >
                Cancel
              </button>
            </footer>
          </div>
      </div>
    );
  }
}

export default RemoveFromHub;
