import React, { Component } from "react";

const ListItem = ({ item = {}, onRemoveUser }) => {
    const handleClick = () => {
        onRemoveUser(item);
    }

    return (
        <div className="control">
            <div className="tags has-addons">
            <a className="tag is-link" title={item.email}>{item.email}</a>
            <a className="tag is-delete" onClick={handleClick}></a>
            </div>
        </div>
    );
};

class SharedHubUsers extends Component {

    handleRemoveUser = (userDetail) => {
        const {
            onRemoveUserAtSharedHub,
            hubId
        } = this.props;
        onRemoveUserAtSharedHub(userDetail, hubId);
    }

    render() {
        const { list, isHubUserLoading } = this.props;
        const loaderSpace = isHubUserLoading && list.length < 1 ? "loader-space" : "";

        return (
            <div className={`shared-hub-users`}>
                {
                    isHubUserLoading &&
                    <div className="loader-block"><span className="is-loading" /></div>
                }
                <div className={`field is-grouped is-grouped-multiline ${loaderSpace}`}>
                    {
                        list.map((item, key) =>
                            <ListItem
                                key={`shared-hub-user-${key}`}
                                item={item}
                                onRemoveUser={this.handleRemoveUser}
                            />
                        )
                    }
                </div>
            </div>
        );
    }
}

export default SharedHubUsers;
