import React, { Component, Fragment } from "react";
import moreIcon from "../../../../public/images/sidebar/more.svg";
import privateIcon from "../../../../public/images/lock.svg";
import publicIcon from "../../../../public/images/group.svg";
import sharedIcon from "../../../../public/images/link.svg";
import userIcon from "../../../../public/images/sidebar/user.png";
const {
  isSharedHub
} = __global__;

const icons = {
  privateIcon,
  publicIcon,
  sharedIcon,
};

class Hub extends Component {

  constructor(props) {
    super(props);
    this.state = {
        isOpenHubDrop : true
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleAction = this.handleAction.bind(this);

    if(props.currentHubId === props.item.hash) {
      props.onActiveTab("hub");  
    }
  }

  handleClick(e) {
    const {
      onClick,
      item: {
        hash
      },
      onActiveTab
    } = this.props;
    e.preventDefault();
    onClick(hash);
    onActiveTab("hub");
  };

  handleAction(e) {
    e.preventDefault();
    const {
      setHubDropdown,
      item
    } = this.props;
    
    let top = e.clientY - 10;
    let left = e.clientX + 10;

    if (window.innerWidth < 768) {
      top += window.pageYOffset || 0; // for mobile device when scroll
      left -= 150;
    }
    const { isOpenHubDrop } = this.state;
   
    setHubDropdown({
      top,
      left,
      item,
      isOpen: isOpenHubDrop
    });
  };

  render() {
    const {
      listType,
      currentHubId,
      item,
      handleCloseMobileSidebar,  
      activeTabName,
    } = this.props;
    const activeHubClass = currentHubId === item.hash ? 'hub-active' : '';
    const hubClass = listType === 'hub' && !activeHubClass ? 'hub-blur' : '';
    const activeElement = isSharedHub===1 || currentHubId === item.hash  ? 'active__element' : '';
    return (
    <div className={`list_row ${activeElement}`}>
      <div className="list__grid">
          <span className={`wrap_block list__caption ${hubClass} ${activeHubClass}`} onClick={this.handleClick}>
            <a onClick={handleCloseMobileSidebar}>{item.name}</a>
            {/* <span className="notification_list">
              <ul>
                  <li>YJ</li>
                  <li><img src={userIcon} alt="" /></li> 
                  <li><img src={userIcon} alt="" /></li> 
                  <li className="count_no">+4</li>
              </ul>
              <span className="total_no">12</span>
              </span> */}
          </span>
          {isSharedHub !== 1 &&
            <span className="wrap_block">
              <img src={moreIcon} className="icon__list icon__more" onClick={this.handleAction}/>
            </span>
          }
      </div>
    </div>
    )
  }

}

export default Hub;