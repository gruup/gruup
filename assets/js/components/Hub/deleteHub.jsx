import React, { Component } from 'react';


class DeleteHub extends Component {
  deleteHub = () => {
    const { item, onDeleteHub, handleOnClick } = this.props;
    onDeleteHub(item.id).then((res) => {
      handleOnClick();
    });
  }

  render() {
    const {  handleOnClick, isDelete } = this.props;

    return (
      <div className={isDelete ? 'modal is-active' : 'modal'}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title is-size-6">Delete Hub</p>
              <button className="delete" type="button" aria-label="close" onClick={handleOnClick} />
            </header>
            <section className="modal-card-body">
              <div className="search-modal">
                This will delete the Hub and the access to it for you and others you may
                have shared it with. It will NOT delete the Tiles within the Hub.
              </div>
            </section>
            <footer className="modal-card-foot">
              <button className="button search-btn-modal" onClick={this.deleteHub}>
                Delete
              </button>
              <button className="button search-btn-modal"
                type="button"
                onClick={handleOnClick}
              >
                Cancel
              </button>
            </footer>
          </div>
      </div>
    );
  }
}

export default DeleteHub;
