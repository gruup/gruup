import React, { Component } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
// import { NotificationManager } from "react-notifications";
import Input from "../../common/inputField";

const required = (value) => value ? undefined : "Name must be required!";

class CreateCopy extends Component {

  notAllowedAddHubs() {
    // NotificationManager.warning('You’ve reached the maximum number of Hubs allowed for a free account. Upgrade your account for more.', '', 5000);
}
  handleOnSubmitCreateCopy = async (values) => {
    const {
      onCreateCopy,
      handleOnClick,
      hubs,
      user,
      item: { id }
    } = this.props;
    
    
    const ownHubs = hubs.filter(({ email }) => email === user.email);
    const maxHubs = (ownHubs.length >= __global__.maxHub) && (user.accountType === 'FREE');

    if(maxHubs) {
      this.notAllowedAddHubs();
      handleOnClick();
      return;
    }

    await onCreateCopy(values, id
      ).then(() => {
        handleOnClick()
      }).catch(({ message: _error = "" }) => {
        throw new SubmissionError({ _error });
      });
  };

  render() {
    const {
      user,
      hubs,
      handleSubmit,
      handleOnClick,
      pristine,
      submitting,
      error,
      isCreateCopy,
    } = this.props;
  
    return (
      <div className={isCreateCopy ? 'modal is-active' : 'modal'}>
        <form onSubmit={handleSubmit(this.handleOnSubmitCreateCopy)}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Create hub copy</p>
              <button className="delete" type="button" aria-label="close"   onClick={handleOnClick} />
            </header>
            <section className="modal-card-body">
              <div className="search-modal">
                <div className="input-field">
                  <span className="field-name"> Name your new Hub </span>
                  <Field
                    name="name"
                    type="text"
                    component={Input}
                    className="modal-input"
                    validate={[ required ]}
                  />
                </div>
                <br />
              </div>
            </section>
            <footer className="modal-card-foot">
              <button
                className={`button search-btn-modal ${submitting ? 'is-loading' : ''}`}
                type="submit" disabled={pristine || submitting}
              >
                Save changes
              </button>
              <button className="button search-btn-modal"
                type="button"
                onClick={handleOnClick}
              >
                Cancel
              </button>
            </footer>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'CreateCopy',
})(CreateCopy);
