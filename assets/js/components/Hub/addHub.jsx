import React, { Component } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import Input from "../../common/inputField";
import bufferIcon from '../../../../public/images/sidebar/buffer.svg';
import closeIcon from '../../../../public/images/sidebar/red_close.svg';
const required = (value) => value ? undefined : "Name must be required!";
const {
  isSharedHub,
} = __global__;

class AddHub extends Component {

  constructor(props) {
    super(props);
    const {
      isEdit,
      currentHub: {
        name: hub,
        hubType,
        id
      } = {},
      initialize
    } = props;

    if(isEdit) {
      initialize({ hub, hubType, id });
    }
  }

  handleOnSubmitAddHub = async (values) => {

    if(!values.hubType || values.hubType == "") {
      throw new SubmissionError({ _error: 'Hub type is required' });
    }

    const {
      onAddEditHub,
      handleOnClick
    } = this.props;

    await onAddEditHub(values
      ).then(() => {
        handleOnClick()
      }).catch(({ message: _error = "" }) => {
        throw new SubmissionError({ _error });
      });
  };

  render() {
    const {
      handleSubmit,
      handleOnClick,
      pristine,
      submitting,
      error,
      isVisible,
      isEdit = false,
    } = this.props;
    const errorMessage = isSharedHub ? "To create a Hub please register for your free account" : error;
    return (
      <div className={isVisible ? 'modal is-active' : 'modal'}>
        <form onSubmit={handleSubmit(this.handleOnSubmitAddHub)}>
          <div className="modal-background" />
          <div className="modal-card">
            <div className="close-modal">
                <img src={closeIcon} onClick={handleOnClick} />
            </div>
            <section className="modal-card-body new-modal-body">
              <div className="modal-title">
              <img src={bufferIcon} alt=""/>
                {isEdit? 'Edit': 'Add New'} Hub
              </div>
              <div className="search-modal modal-form">
                <div className="input-field">
                  <span className="field-name"> Name your{isEdit? '': ' new'} Hub </span>
                  <Field
                    name="hub"
                    type="text"
                    component={Input}
                    className="modal-input"
                    validate={[ required ]}
                  />
                </div>
                <div className="checkbox-inline">
                  <Field
                    name="hubType"
                    type="radio"
                    component="input"
                    value="private"
                    id="private"
                  /> <label htmlFor="private">Private Hub</label>
                  <span className="field-checkbox">Only you can see what's in this Hub</span>
                </div>
                <div className="checkbox-inline">
                  <Field
                    name="hubType"
                    type="radio"
                    component="input"
                    value="shared"
                    id="shared"
                  /> <label htmlFor="shared">Shared Hub</label>
                  <span className="field-checkbox">You can share what's in this Hub with anyone</span>
                </div>
                <div className="checkbox-inline">
                  <Field
                    name="hubType"
                    type="radio"
                    component="input"
                    value="open"
                    id="open"
                  /> <label htmlFor="open">Open Hub</label>
                  <span className="field-checkbox">A collaborative Hub where everyone can add Tiles</span>
                </div>
                {error && <p className="help is-danger">{errorMessage}</p>}
                <br />
              </div>
              <div className="new-modal-bottom">
                <a href="javascript:;" className="cancel-lnk" onClick={handleOnClick}>Cancel</a>
                <button
                  className={`button submit-btn-modal ${submitting ? 'is-loading' : ''}`}
                  type="submit" disabled={pristine || submitting}
                >
                  {isEdit? 'Update':'Create'} Hub
                </button>
              </div>
            </section>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'AddHub',
})(AddHub);
