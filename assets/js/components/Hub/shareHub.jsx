import React, { Component, Fragment } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import Input from "../../common/inputField";
import SharedHubUsers from "./sharedHubUsers";

const required = (value) => value ? undefined : "Email must be required!";
const email = value => value && !/^(\s?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+)$/i.test(value) ?
  'Invalid email address' : undefined;

class AddHub extends Component {

  async componentDidMount() {
    const {
      isManageUser,
      onGetHubUsers,
      item: { id }
    } = this.props;
    if(isManageUser) {
      await onGetHubUsers(id);
    }
  }

  handleOnSubmitShareHub = async (values) => {
    const {
      onShareHub,
      handleOnClick,
      item: { id },
      isManageUser,
      onGetHubUsers,
      reset
    } = this.props;

    await onShareHub(values, id
      ).then(async () => {
        if(isManageUser) {
          reset();
          await onGetHubUsers(id);
        } else {
          handleOnClick();
        }
      }).catch(({ message: _error = "" }) => {
        throw new SubmissionError({ _error });
      });
  };

  render() {
    const {
      handleSubmit,
      handleOnClick,
      pristine,
      submitting,
      error,
      isOpen,
      isManageUser,
      list = [],
      onRemoveUserAtSharedHub,
      item,
      isHubUserLoading
    } = this.props;

    return (
      <div className={isOpen ? 'modal is-active' : 'modal'}>
        <form onSubmit={handleSubmit(this.handleOnSubmitShareHub)}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">{isManageUser ? "Manage Hub User"  : "Share Hub"}</p>
              <button className="delete" type="button" aria-label="close" onClick={handleOnClick} />
            </header>
            <section className={`modal-card-body ${isManageUser? 'mange-user': ''}`}>
              <div className="search-modal">
                <div className="input-field">
                  <span className="field-name"> Email </span>
                  <Field
                    name="email"
                    type="text"
                    component={Input}
                    className="modal-input"
                    placeholder="Email address"
                    validate={[ required, email ]}
                  />
                  <span className="field-note"> To send to more than one email address, separate them with a comma. </span>
                </div>
                {error && <p className="help is-danger">{error}</p>}
              </div>
              {
                isManageUser &&
                <Fragment>
                  <div className="action">
                    <button
                      className={`button search-btn-modal ${submitting ? 'is-loading' : ''}`}
                      type="submit" disabled={pristine || submitting}
                    >
                      Share
                    </button>
                  </div>
                  <SharedHubUsers
                    list={list}
                    onRemoveUserAtSharedHub={onRemoveUserAtSharedHub}
                    hubId={item.id}
                    isHubUserLoading={isHubUserLoading}
                  />
                </Fragment>
              }
            </section>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'AddHub',
})(AddHub);
