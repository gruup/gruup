import React, { Component } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import Select from 'react-select';

const renderMultiselect = ({ input, options= [] }) => {

    const handleChange = (value) => {
      input.onChange(value);
    };

    return (
      <Select.Creatable
        onBlur={() => input.onBlur()}
        autoFocus={true}
        multi={true}
        value={input.value || []}
        valueArray={input.value || []}
        onChange={handleChange}
        isSearchable={true}
        name={input.name}
        options={options}
        showNewOptionAtTop={true}
        placeholder={'Select tag'}
      />
    )
};

class AddTag extends Component {

  constructor(props) {
    super(props);
    this.getSelectedTag();
    const {
      data = []
    } = props;
    this.options = data.map(({ id, name }) => ({ value: id, label: name })) || [];
  }

  getSelectedTag = async () => {
    const { hubId, getHubSelectedTag, initialize, data } = this.props;

    const { data: selectedTags = 0 } = await getHubSelectedTag(hubId) || { data: [] };

    let filteredData = [];
    if(selectedTags.length > 0) {
      data.map((tag) => {
        const isSelected = selectedTags.some((tagId) => tagId == tag.id);
        if(isSelected) {
          filteredData.push(tag);
        }
      })
    }
    const options = filteredData.map(({ id, name }) => ({ value: id, label: name }));
    initialize({ hubs: options });
  };

  handleOnSubmitAddTag = ({ hubs } = {}) => {
    const {
      onAddTag,
      handleOnClick,
      hubId
    } = this.props;

    hubs = hubs.map(({ value: id = null, label: name, className }) => {
      if(className) {
        return { name };
      }

      return {
        id,
        name
      }
    });

    onAddTag({
        hubId,
        tags: { hubs }
      }).then(() => {
        handleOnClick()
      }).catch(({ message: _error = "" }) => {
        throw new SubmissionError({ _error });
      });
  };

  render() {
    const {
      handleOnClick,
      pristine,
      submitting,
      error,
      isAddTag,
      data: filteredData,
      handleSubmit
    } = this.props;

    return (
      <div className={isAddTag ? 'modal is-active' : 'modal'}>
        <form onSubmit={handleSubmit(this.handleOnSubmitAddTag)}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Add Tag</p>
              <button className="delete" type="button" aria-label="close" onClick={handleOnClick} />
            </header>
            <section className="modal-card-body add-tag-modal">
              <div>
                <Field
                  name="hubs"
                  component={renderMultiselect}
                  options={this.options}
                />

                {error && <p className="help is-danger">{error}</p>}
                <br />
              </div>
            </section>
            <footer className="modal-card-foot">
              <button className={`button search-btn-modal ${submitting ? 'is-loading' : ''}`} type="submit" disabled={pristine || submitting}>Save changes</button>
              <button className="button search-btn-modal" type="button" onClick={handleOnClick}>Cancel</button>
            </footer>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'AddTag',
})(AddTag);
