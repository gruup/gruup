import React, { Component, Fragment} from 'react';
import Hub from "./Hub";

const {
  isSharedHub
} = __global__;

class Hubs extends Component {

  constructor(props) {
    super(props);
    this.handleHub = this.handleHub.bind(this);
  }

  handleHub(hash) {
    if (isSharedHub === 1) {
      return true;
    }
    const {
      history,
      setHub,
      onToggleSelectMode,
      currentHubId,
      
    } = this.props;

    if(hash === currentHubId) {
      return true;
    }

    const url = Routing.generate('view_hub_details', {hash});
    onToggleSelectMode(true);
    setHub({offset: 0});
    history.push(url);
  };

  render() {
    const {
      hubs = [],
      handleCloseMobileSidebar,
      ...ownProps
    } = this.props;
    return (
      <Fragment>
          {
            hubs.map((item) =>
              <Hub
              handleCloseMobileSidebar={handleCloseMobileSidebar}
                key={item.hash}
                item={item}
                hubs={hubs}
                onClick={this.handleHub}
                {...ownProps}
              />
            )
          }
      </Fragment>
    );
  }
}

export default Hubs;
