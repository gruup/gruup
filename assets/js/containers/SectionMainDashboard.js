import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import SectionMainDashboard from '../components/SectionMainDashboard';
import {
  getAllTiles as onGetAllTiles,
  addTileToTag as onAddTileToTag,
  getSelectedTag as onGetSelectedTag,
  getSelectedHub as onGetSelectedHub,
  addNewTag as onAddNewTag,
  addTile as onAddTile,
  loadMoreOnScroll,
  deleteTile as onDeleteTile,
  getSelectedHubLength as onSelectedHubLength,
  onSetTileDetailModal,
  setColumn as onSetColumn,
  toggleSelectMode as onToggleSelectMode,
  selectTile as onSelectTile,
  removeSelectedTile as onRemoveSelectedTile,
  postSelectedTile,
  getHubByHash,
  getQueryParam,
  getParameterByName,
  removeTileFromHub as onRemoveTileFromHub,
  setGoogleNotify,
  setHubDropdown,
  setTile,
  toggleFlipTile,
  resetOffset,
  setActiveTab,
} from '../actions/section-main-dashboard';

import {
  setHubModal as onSetHubModal,
  setTagModal as onSetTagModal,
  setDeleteTileModal as onSetDeleteTileModal,
  resetDeleteTileModal as onResetDeleteTileModal,
  resetHubModal as onResetHubModal,
  resetTagModal as onResetTagModal,
  setRemoveFromHubModal as onSetRemoveFromHubModal,
} from '../actions/modals';

import {
  setSearch as onSetSearch,
} from '../actions/account';

import {
  setUser as onSetUser,
} from '../actions/user';

import {
  addEditHub as onAddEditHub,
  deleteHub as onDeleteHub,
  shareHub as onShareHub,
  createCopy as onCreateCopy,
  addTag as onAddTag,
  getHubSelectedTag,
  removeUserAtSharedHub as onRemoveUserAtSharedHub,
  getHubUsers as onGetHubUsers,
  setHubState as onSetHubState,
} from '../actions/AsideMenuList';


const mapStateToProps = state => state;

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    onGetAllTiles,
    onAddTileToTag,
    onGetSelectedTag,
    onGetSelectedHub,
    onAddNewTag,
    onAddTile,
    loadMoreOnScroll,
    onDeleteTile,
    onSelectedHubLength,
    onSetTileDetailModal,
    onSetColumn,
    onToggleSelectMode,
    onSelectTile,
    onSetHubModal,
    postSelectedTile,
    onSetTagModal,
    onRemoveSelectedTile,
    onResetHubModal,
    onResetTagModal,
    onSetDeleteTileModal,
    onResetDeleteTileModal,
    onSetSearch,
    getHubByHash,
    getQueryParam,
    getParameterByName,
    onSetRemoveFromHubModal,
    onRemoveTileFromHub,
    setGoogleNotify,
    setHubDropdown,
    setTile,
    toggleFlipTile,
    onSetUser,
    onResetOffset: resetOffset,

    /* hub dropdown action */
    onDeleteHub,
    onShareHub,
    onCreateCopy,
    onAddTag,
    onAddEditHub,
    getHubSelectedTag,
    onRemoveUserAtSharedHub,
    onGetHubUsers,
    onSetHubState,
    setActiveTab,
  },
  dispatch,
);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SectionMainDashboard));
