import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TileDetailModal from '../common/tileDetailModal';
import {
  onSetTileDetailModal,
  onResetTileDetailModal,
  getTileById,
  setActiveTab,
} from '../actions/section-main-dashboard';

const mapStateToProps = ({ tile: { tileDetailModal } }) => ({ ...tileDetailModal });

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    setActiveTab,
    onSetTileDetailModal,
    onResetTileDetailModal,
    getTileById,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(TileDetailModal);
