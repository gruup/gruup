import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Account from '../components/Header/header';

import {
  getUserDetails as onGetUserDetails,
  searchTile,
  getClassName as onGetClassName,
  setSearch,
} from '../actions/account';
import {
  onSetTileDetailModal,
  onResetTileDetailModal,
  setDriveData,
  setGoogleNotify,
  getAllTiles,
  resetOffset as onResetOffset,
} from '../actions/section-main-dashboard';
import {
  setTile,
} from '../actions/pagination';

const mapStateToProps = ({
  modals,
  tile: {
    driveData,
    data = {},
  },
  ...ownProps
}) => ({
  ...ownProps,
  driveData,
  totalTile: Object.keys(data).length,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    onGetUserDetails,
    searchTile,
    onGetClassName,
    onSetTileDetailModal,
    onResetTileDetailModal,
    setDriveData,
    setGoogleNotify,
    getAllTiles,
    setSearch,
    setTile,
    onResetOffset,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(Account);
