import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import Modals from '../components/modals';

import * as modalsActions from '../actions/modals';
import {
  getSelectedTag as onGetSelectedTag,
  getSelectedHub as onGetSelectedHub,
} from '../actions/section-main-dashboard';

const selector = formValueSelector('TagModal');
const mapStateToProps = (state) => {
  const {
    modals, hub, tile, tag,
  } = state;
  const selectTagItems = selector(state, 'tiles');
  return {
    ...modals,
    hub,
    tile,
    tag,
    selectTagItems,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      ...modalsActions,
      onGetSelectedHub,
      onGetSelectedTag,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Modals);
