import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AsideMenuList from '../components/AsideMenuList';

import {
  getAllHubs as onGetAllHubs,
  getAllTags as onGetAllTags,
  addEditHub as onAddEditHub,
  setTag,
  setHub,
  getHubSelectedTag,
} from '../actions/AsideMenuList';

import {
  setTile as onSetTile,
  setHubDropdown,
  toggleSelectMode as onToggleSelectMode,
  getAllTiles,
  resetOffset as onResetOffset,
  setGoogleNotify as onSetGoogleNotify,
  setDriveData,
  setActiveTab,
} from '../actions/section-main-dashboard';
import { getUserDetails as onGetUserDetails, setSearch } from '../actions/account';

const mapStateToProps = ({
  tile: {
    isOpenSidebar,
    listType,
    currentHubId,
    currentTagId,
    isShowMobileSidebar,
    activeTabName,
  },
  tile,
  modals,
  ...ownProps
}) => ({
  ...ownProps,
  tile,
  isOpenSidebar,
  listType,
  currentHubId,
  currentTagId,
  isShowMobileSidebar,
  activeTabName,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    setDriveData,
    onGetAllHubs,
    onGetAllTags,
    onAddEditHub,
    setTag,
    setHub,
    getHubSelectedTag,
    onSetTile,
    setHubDropdown,
    onToggleSelectMode,
    onGetUserDetails,
    getAllTiles,
    onResetOffset,
    setSearch,
    onSetGoogleNotify,
    setActiveTab,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(AsideMenuList);
