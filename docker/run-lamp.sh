#!/bin/bash

function exportBoolean {
    if [ "${!1}" = "**Boolean**" ]; then
            export ${1}=''
    else
            export ${1}='Yes.'
    fi
}

exportBoolean LOG_STDOUT
exportBoolean LOG_STDERR

if [ $environment ]; then
  echo "enviroment is "${environment} > /var/log/ENVIRONMENT
  cp /app/mysql_config/env.${environment} /app/.env
  cp /app/docker/site.conf.${environment} /etc/nginx/conf.d/${environment}.conf
fi


&>/dev/null /etc/init.d/nginx start
&>/dev/null /usr/local/sbin/php-fpm
