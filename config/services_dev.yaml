# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: 'en'
    router.request_context.host: '%env(DOMAIN_HOST)%'
    router.request_context.scheme: '%env(DOMAIN_SCHEME)%'
    router.request_context.base_url: ''

services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        public: false       # Allows optimizing the container by removing unused services; this also means
                            # fetching services directly from the container via $container->get() won't work.
                            # The best practice is to be explicit about your dependencies anyway.

    # makes classes in src/ available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    App\:
        resource: '../src/*'
        exclude: '../src/{Entity,Migrations,Tests,Kernel.php}'

    # controllers are imported separately to make sure services can be injected
    # as action arguments even if you don't extend any base controller class
    App\Controller\:
        resource: '../src/Controller'
        tags: ['controller.service_arguments']

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones


    setting.manager:
        class: App\Service\SettingManager
        public: true

    tile.manager:
        class: App\Service\TileManager
        public: true

    hub.manager:
        class: App\Service\HubManager
        public: true

    tag.manager:
        class: App\Service\TagManager
        public: true

    search.manager:
        class: App\Service\SearchManager
        public: true

    email.send:
        class: App\Service\EmailSend
        public: true

    user.manager:
        class: App\Service\UserManager
        public: true

    google.oauth.manager:
        class: App\Service\GoogleOauthManager
        public: true
        arguments:
            - '@request_stack'
            - '@google_client'
            - '@setting.manager'
            - '@logger'

    fileListsManager:
        class: App\Service\FileListsManager
        public: true
        arguments:
            - '@google_service_drive'
            - '@setting.manager'
            - '@logger'

    openDownloadFilesManager:
        class: App\Service\OpenDownloadFilesManager
        public: true
        arguments:
            - '@google_service_drive'
            - '@google_client'
            - '@setting.manager'

    google.drive.client.manager:
        class: App\Service\GoogleDriveClientManager
        public: true
        arguments:
            - '@request_stack'
            - '@google_client'
            - '@google_service_oauth2'
            - '@setting.manager'
            - '@logger'

    google_client:
        class: Google_Client
        public: true
        calls:
            - [setAuthConfig, ['%env(GOOGLE_CREDENTIAL)%']]
            - [setApplicationName, ['Gruup']]
            - [setScopes, [["https://www.googleapis.com/auth/userinfo.email",
                "https://www.googleapis.com/auth/plus.me",
                "https://www.googleapis.com/auth/userinfo.profile",
                "https://www.googleapis.com/auth/drive"]]]
            - [setAccessType, ['offline']]
            - [setApprovalPrompt, ['force']]

    google_service_oauth2:
        class: Google_Service_Oauth2
        public: true
        arguments:
            - '@google_client'

    google_service_drive:
        class: Google_Service_Drive
        public: true
        arguments:
            - '@google_client'

    mime_type_extension:
        class: Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser
        public: true